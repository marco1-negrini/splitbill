
import 'package:flutter/material.dart';
import 'package:project/values/values.dart';


class Calendar4CopyWidget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      body: Opacity(
        opacity: 1,
        child: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            color: Color.fromARGB(217, 0, 0, 0),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Align(
                alignment: Alignment.topRight,
                child: Container(
                  width: 50,
                  height: 49,
                  margin: EdgeInsets.only(top: 50, right: 25),
                  child: Opacity(
                    opacity: 1,
                    child: Image.asset(
                      "assets/images/picture2-9.png",
                      fit: BoxFit.none,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 184,
                  height: 71,
                  margin: EdgeInsets.only(left: 50, top: 1),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Positioned(
                        left: 0,
                        top: 0,
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              left: 0,
                              top: 37,
                              child: Opacity(
                                opacity: 1,
                                child: Text(
                                  "07:45",
                                  // alignment: TextAlign.left,
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    fontFamily: "Lato",
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 0,
                              top: 0,
                              child: Opacity(
                                opacity: 0.95622,
                                child: Text(
                                  "Monday, 11 November",
                                  // alignment: TextAlign.left,
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    fontFamily: "Lato",
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Positioned(
                        left: 75,
                        top: 49,
                        child: Opacity(
                          opacity: 1,
                          child: Text(
                            "n participants ",
                            // alignment: TextAlign.left,
                            style: TextStyle(
                              color: Color.fromARGB(255, 208, 222, 163),
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w300,
                              // style: FontStyle.italic,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Spacer(),
              Container(
                height: 25,
                margin: EdgeInsets.only(left: 139, right: 169, bottom: 64),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                        width: 17,
                        height: 25,
                        child: Opacity(
                          opacity: 1,
                          child: Image.asset(
                            "assets/images/icon-arrow-left-4-2.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                        width: 17,
                        height: 25,
                        child: Opacity(
                          opacity: 1,
                          child: Image.asset(
                            "assets/images/icon-arrow-left-4-2.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}