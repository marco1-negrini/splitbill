import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:project/SizeConfig.dart';
import 'package:project/utilities.dart';
import 'package:project/values/values.dart';
import 'dart:collection';


class PartecipantsWidget extends StatelessWidget {
  int partecipantPerRow = 4;
  List<HashMap> participantPreviews = get_participantsPreviewa();


  display_row_elements(index, context)

  {
    List <Widget> children = new List<Widget>();
    int j = 0;
    print("index: " + index.toString());
    while ((index   < participantPreviews.length - j ) && (j < partecipantPerRow))
    {
      print(index+j);
      int gap = j;
      Container c = Container(
        height: SizeConfig.screenHeight*0.065,
        margin: EdgeInsets.only(
            top: SizeConfig.screenHeight*0.03,
        ),

        child: ButtonTheme(
          child: FlatButton(
            child: Opacity(
              opacity: 1,
              child: Image.asset(
                participantPreviews[index+j]["Image"],
                fit: BoxFit.fill,
              ),
            ),
            onPressed: () {
              print(gap);
              print(index+gap);
              print(participantPreviews[index+gap]["ID"] + " pressed");
              Navigator.pushNamed(context, '/Partecipants2', arguments: participantPreviews[index+gap]["ID"]);
            },
          ),
        ),
      );
      children.add(c);
      j++;
    }
    return children;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Opacity(
        opacity: 1,

        // container page
        child: Container(
          color: Color.fromARGB(217, 0, 0, 0),
        child: Column(
          children: <Widget>[
            // SEARCH BAR AND HOME BUTTON CONTAINER

            Container(
              height: SizeConfig.screenHeight*0.1,
              margin: EdgeInsets.only(
                  left: SizeConfig.screenWidth*0.07,
                  top: SizeConfig.screenHeight*0.070,
                  right: SizeConfig.screenWidth*0.07
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [

                  // SEARCH IMAGE

                  Align(
                    alignment: Alignment.topLeft,
                    child: Opacity (
                      opacity: 1,
                      // container magnifier
                      child: Container (
                        width: SizeConfig.screenWidth*0.07,  //150
                        height: SizeConfig.screenHeight*0.05,

                        child: Image.asset(
                          "assets/images/glyphs-search.png",
                          fit: BoxFit.none,
                        ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    // container search bar + text
                    child: Container(
                      width: SizeConfig.screenWidth*0.5,
                      height: SizeConfig.screenHeight*0.05,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight*0.002,
                        left: SizeConfig.screenWidth*0.02,
                      ),
                      child:
                      TextField (
                        decoration: InputDecoration(
                          filled: true,
                          hintText:'People',
                          fillColor: Colors.white,
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(25.7),
                          ),
                        ),
                        style: TextStyle(
                          //color: AppColors.secondaryText,
                          fontFamily: "Lato",
                          fontWeight: FontWeight.w400,
                          fontSize: 20,
                        ),
                      ),
                    ),
                  ),
                  Spacer(),

                  //HOME BUTTON

                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      width: SizeConfig.screenWidth*0.2,
                      height: SizeConfig.screenWidth*0.12,
                     // margin: EdgeInsets.only(bottom: SizeConfig.screenHeight*0.002),
                      child: ButtonTheme(
                            child: FlatButton(
                              child: Opacity(
                               opacity: 1,
                                child: Image.asset(
                                "assets/images/picture2-2.png",
                                  fit: BoxFit.none,
                             ),
                            ),
                            onPressed: () {
                             Navigator.pushNamed(context, '/HomePage');
                             print("Pressing Back Home");
                              // Navigator.pushNamed(context, '/AddEvent1Widget');
                             },
                          ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            Align(
              alignment: Alignment.topLeft,
              // container V0 text
              child: Container(
                color: Color.fromARGB(217, 0, 0, 0),
                height: SizeConfig.screenHeight*0.04,
                margin: EdgeInsets.only(
                    //top: SizeConfig.screenHeight*0.0005,
                    left: SizeConfig.screenWidth*0.04
                ),
                child: Opacity(
                  opacity: 0.95622,
                  child: Text(
                    "V0",
                    // alignment: TextAlign.left,
                    style: TextStyle(
                      color: AppColors.secondaryText,
                      fontFamily: "Lato",
                      fontWeight: FontWeight.w700,
                      fontSize: 24,
                    ),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Opacity(
                opacity: 1,
                // container row under V0
                child: Container(
                  width: SizeConfig.screenWidth*0.6,
                  height: SizeConfig.screenHeight*0.001,
                  margin: EdgeInsets.only(
                      left: SizeConfig.screenWidth*0.04
                  ),
                  decoration: BoxDecoration(
                    color: AppColors.primaryElement,
                    border: Border.fromBorderSide(Borders.primaryBorder),
                  ),
                  child: Container(),
                ),
              ),
            ),

            //Container for V0 ListView
            Expanded(
              child: ListView.separated(
                padding: EdgeInsets.only(
                  left: SizeConfig.screenWidth*0.04,
                  right: SizeConfig.screenWidth*0.04,
                ),
                itemCount: (participantPreviews.length/partecipantPerRow).ceil(),
                //itemCount: 9,
                itemBuilder: (context, int index) {
                  print("index:");
                  print(index);

                  return Container(

                    //height: 50,
                    child:
                    Row(
                      //child: Text('Entry ${participantPreviews[index]}')
                        children: display_row_elements(index*partecipantPerRow,context)
                    ),
                  );
                },
                separatorBuilder: (context, int index) => const Divider(),

              ),
            ),

            Align(
                alignment: Alignment.topLeft,
                // container V0 text
                child: Container(
                    color: Colors.black38,
                    height: SizeConfig.screenHeight*0.04,
                    margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight*0.06,
                        left: SizeConfig.screenWidth*0.04
                    ),
                    child: Opacity(
                      opacity: 0.95622,
                      child: Text(
                        "V1",
                        // alignment: TextAlign.left,
                        style: TextStyle(
                          color: AppColors.secondaryText,
                          fontFamily: "Lato",
                          fontWeight: FontWeight.w700,
                          fontSize: 24,
                        ),
                      ),
                    ),
                  ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Opacity(
                opacity: 1,
                // container row under V0
                child: Container(
                  width: SizeConfig.screenWidth*0.6,
                  height: SizeConfig.screenHeight*0.001,
                  margin: EdgeInsets.only(
                      left: SizeConfig.screenWidth*0.04
                  ),
                  decoration: BoxDecoration(
                    color: AppColors.primaryElement,
                    border: Border.fromBorderSide(Borders.primaryBorder),
                  ),
                  child: Container(),
                ),
              ),
            ),
            Expanded(
              child: ListView.separated(
                padding: EdgeInsets.only(
                  left: SizeConfig.screenWidth*0.04,
                  right: SizeConfig.screenWidth*0.04,
                ),
                itemCount: (participantPreviews.length/partecipantPerRow).ceil(),
                //itemCount: 9,
                itemBuilder: (context, int index) {
                  print("index:");
                  print(index);

                  return Container(

                    //height: 50,
                    child:
                    Row(
                      //child: Text('Entry ${participantPreviews[index]}')
                        children: display_row_elements(index*partecipantPerRow,context)
                    ),
                  );
                },
                separatorBuilder: (context, int index) => const Divider(),

              ),
            ),

          ]
        ),

        )
        ),

    );
  }
}


