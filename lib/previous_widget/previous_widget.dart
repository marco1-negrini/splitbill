
import 'package:flutter/material.dart';
import 'package:project/values/values.dart';


class PreviousWidget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      body: Opacity(
        opacity: 1,
        child: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            color: Color.fromARGB(217, 0, 0, 0),
          ),
          child: Stack(
            alignment: Alignment.topCenter,
            children: [
              Positioned(
                top: 149,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        width: 127,
                        child: Opacity(
                          opacity: 1,
                          child: Text(
                            "m/n participants ",
                            // alignment: TextAlign.left,
                            style: TextStyle(
                              color: Color.fromARGB(255, 208, 222, 163),
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w300,
                              // style: FontStyle.italic,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        width: 127,
                        margin: EdgeInsets.only(top: 6),
                        child: Opacity(
                          opacity: 1,
                          child: Text(
                            "m/n participants ",
                            // alignment: TextAlign.left,
                            style: TextStyle(
                              color: Color.fromARGB(255, 214, 226, 170),
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w300,
                              // style: FontStyle.italic,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        width: 127,
                        margin: EdgeInsets.only(top: 7),
                        child: Opacity(
                          opacity: 1,
                          child: Text(
                            "m/n participants ",
                           // alignment: TextAlign.left,
                            style: TextStyle(
                              color: Color.fromARGB(255, 219, 230, 177),
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w300,
                              // style: FontStyle.italic,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        width: 127,
                        margin: EdgeInsets.only(top: 7),
                        child: Opacity(
                          opacity: 1,
                          child: Text(
                            "m/n participants ",
                            // alignment: TextAlign.left,
                            style: TextStyle(
                              color: Color.fromARGB(255, 214, 226, 170),
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w300,
                              // style: FontStyle.italic,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        width: 127,
                        margin: EdgeInsets.only(top: 8),
                        child: Opacity(
                          opacity: 1,
                          child: Text(
                            "m/n participants ",
                            // alignment: TextAlign.left,
                            style: TextStyle(
                              color: Color.fromARGB(255, 214, 226, 170),
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w300,
                             // style: FontStyle.italic,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                left: 31,
                top: 50,
                right: 25,
                bottom: -1002,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        width: 50,
                        height: 49,
                        child: Opacity(
                          opacity: 1,
                          child: Image.asset(
                            "assets/images/picture2.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: 174,
                        height: 59,
                        margin: EdgeInsets.only(left: 19, top: 1),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              left: 0,
                              top: 37,
                              child: Opacity(
                                opacity: 1,
                                child: Text(
                                  "07:00",
                                 //  alignment: TextAlign.left,
                                  style: TextStyle(
                                    color: AppColors.secondaryText,
                                    fontFamily: "Lato",
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 0,
                              top: 0,
                              child: Opacity(
                                opacity: 0.95622,
                                child: Text(
                                  "Thursday, 7 November",
                                  // alignment: TextAlign.left,
                                  style: TextStyle(
                                    color: AppColors.secondaryText,
                                    fontFamily: "Lato",
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(left: 19, top: 7),
                        child: Opacity(
                          opacity: 1,
                          child: Text(
                            "07:45",
                            // alignment: TextAlign.left,
                            style: TextStyle(
                              color: AppColors.secondaryText,
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w300,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(left: 19, top: 7),
                        child: Opacity(
                          opacity: 1,
                          child: Text(
                            "12:30",
                           //  alignment: TextAlign.left,
                            style: TextStyle(
                              color: AppColors.secondaryText,
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w300,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(left: 19, top: 6),
                        child: Opacity(
                          opacity: 1,
                          child: Text(
                            "13:15",
                           //  alignment: TextAlign.left,
                            style: TextStyle(
                              color: AppColors.secondaryText,
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w300,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(left: 19, top: 7),
                        child: Opacity(
                          opacity: 1,
                          child: Text(
                            "18:30",
                           //  alignment: TextAlign.left,
                            style: TextStyle(
                              color: AppColors.secondaryText,
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w300,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        width: 300,
                        height: 490,
                        margin: EdgeInsets.only(bottom: 192),
                        child: Opacity(
                          opacity: 1,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Align(
                                alignment: Alignment.topLeft,
                                child: Opacity(
                                  opacity: 0.95622,
                                  child: Text(
                                    "WOD name: ",
                                  //   alignment: TextAlign.left,
                                    style: TextStyle(
                                      color: AppColors.secondaryText,
                                      fontFamily: "Lato",
                                      fontWeight: FontWeight.w400,
                                      fontSize: 24,
                                    ),
                                  ),
                                ),
                              ),
                              Opacity(
                                opacity: 1,
                                child: Container(
                                  height: 1,
                                  margin: EdgeInsets.only(top: 1),
                                  decoration: BoxDecoration(
                                    color: AppColors.primaryElement,
                                    border: Border.fromBorderSide(Borders.primaryBorder),
                                  ),
                                  child: Container(),
                                ),
                              ),
                              Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                  margin: EdgeInsets.only(top: 19),
                                  child: Opacity(
                                    opacity: 1,
                                    child: Text(
                                      "Insert WOD name",
                                     // alignment: TextAlign.left,
                                      style: TextStyle(
                                        color: Color.fromARGB(255, 166, 187, 193),
                                        fontFamily: "Lato",
                                        fontWeight: FontWeight.w400,
                                        fontSize: 18,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                  margin: EdgeInsets.only(top: 33),
                                  child: Opacity(
                                    opacity: 0.95622,
                                    child: Text(
                                      "WOD type: ",
                                     // alignment: TextAlign.left,
                                      style: TextStyle(
                                        color: AppColors.secondaryText,
                                        fontFamily: "Lato",
                                        fontWeight: FontWeight.w400,
                                        fontSize: 24,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Opacity(
                                opacity: 1,
                                child: Container(
                                  height: 1,
                                  margin: EdgeInsets.only(top: 1),
                                  decoration: BoxDecoration(
                                    color: AppColors.primaryElement,
                                    border: Border.fromBorderSide(Borders.primaryBorder),
                                  ),
                                  child: Container(),
                                ),
                              ),
                              Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                  margin: EdgeInsets.only(top: 19),
                                  child: Opacity(
                                    opacity: 1,
                                    child: Text(
                                      "Insert WOD type",
                                      // alignment: TextAlign.left,
                                      style: TextStyle(
                                        color: Color.fromARGB(255, 175, 195, 200),
                                        fontFamily: "Lato",
                                        fontWeight: FontWeight.w400,
                                        fontSize: 18,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Opacity(
                                opacity: 0.15,
                                child: Container(
                                  height: 1,
                                  margin: EdgeInsets.only(top: 58),
                                  decoration: BoxDecoration(
                                    color: AppColors.primaryElement,
                                    border: Border.fromBorderSide(Borders.primaryBorder),
                                  ),
                                  child: Container(),
                                ),
                              ),
                              Opacity(
                                opacity: 0.15,
                                child: Container(
                                  height: 1,
                                  margin: EdgeInsets.only(top: 49),
                                  decoration: BoxDecoration(
                                    color: AppColors.primaryElement,
                                    border: Border.fromBorderSide(Borders.primaryBorder),
                                  ),
                                  child: Container(),
                                ),
                              ),
                              Opacity(
                                opacity: 0.15,
                                child: Container(
                                  height: 1,
                                  margin: EdgeInsets.only(top: 49),
                                  decoration: BoxDecoration(
                                    color: AppColors.primaryElement,
                                    border: Border.fromBorderSide(Borders.primaryBorder),
                                  ),
                                  child: Container(),
                                ),
                              ),
                              Spacer(),
                              Opacity(
                                opacity: 0.15,
                                child: Container(
                                  height: 1,
                                  margin: EdgeInsets.only(bottom: 24),
                                  decoration: BoxDecoration(
                                    color: AppColors.primaryElement,
                                    border: Border.fromBorderSide(Borders.primaryBorder),
                                  ),
                                  child: Container(),
                                ),
                              ),
                              Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 1),
                                  child: Opacity(
                                    opacity: 0.95622,
                                    child: Text(
                                      "WOD score: ",
                                      // alignment: TextAlign.left,
                                      style: TextStyle(
                                        color: AppColors.secondaryText,
                                        fontFamily: "Lato",
                                        fontWeight: FontWeight.w400,
                                        fontSize: 24,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Opacity(
                                opacity: 1,
                                child: Container(
                                  height: 1,
                                  margin: EdgeInsets.only(bottom: 19),
                                  decoration: BoxDecoration(
                                    color: AppColors.primaryElement,
                                    border: Border.fromBorderSide(Borders.primaryBorder),
                                  ),
                                  child: Container(),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 80, bottom: 8),
                                child: Opacity(
                                  opacity: 1,
                                  child: Text(
                                    "Generated score description",
                                   //  alignment: TextAlign.left,
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 198, 215, 219),
                                      fontFamily: "Lato",
                                      fontWeight: FontWeight.w300,
                                      fontSize: 18,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        width: 285,
                        height: 285,
                        margin: EdgeInsets.only(bottom: 90),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              bottom: 60,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Align(
                                    alignment: Alignment.topCenter,
                                    child: Container(
                                      width: 119,
                                      height: 121,
                                      margin: EdgeInsets.only(bottom: 11),
                                      child: Opacity(
                                        opacity: 1,
                                        child: Image.asset(
                                          "assets/images/movie-symbol-of-video-camera.png",
                                          fit: BoxFit.none,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topCenter,
                                    child: Opacity(
                                      opacity: 1,
                                      child: Text(
                                        "Add video",
                                        // alignment: TextAlign.center,
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 191, 209, 213),
                                          fontFamily: "Lato",
                                          fontWeight: FontWeight.w400,
                                          fontSize: 24,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Positioned(
                              bottom: 0,
                              child: Opacity(
                                opacity: 1,
                                child: Container(
                                  width: 285,
                                  height: 285,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 0.5,
                                      color: Color.fromARGB(255, 187, 206, 138),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color.fromARGB(26, 0, 0, 0),
                                        offset: Offset(0, 15),
                                        blurRadius: 24,
                                      ),
                                    ],
                                    borderRadius: BorderRadius.all(Radius.circular(18)),
                                  ),
                                  child: Container(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: 253,
                        margin: EdgeInsets.only(left: 23, bottom: 5),
                        child: Opacity(
                          opacity: 0.95622,
                          child: Text(
                            "Partecipation per class in this day",
                            // alignment: TextAlign.left,
                            style: TextStyle(
                              color: AppColors.secondaryText,
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w300,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: 299,
                        height: 145,
                        margin: EdgeInsets.only(bottom: 43),
                        child: Opacity(
                          opacity: 1,
                          child: Image.asset(
                            "assets/images/istogramma-singolo-allenamento-b.png",
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 25,
                      margin: EdgeInsets.only(left: 108, right: 144),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.bottomLeft,
                            child: Container(
                              width: 17,
                              height: 25,
                              child: Opacity(
                                opacity: 1,
                                child: Image.asset(
                                  "assets/images/icon-arrow-left-4-2.png",
                                  fit: BoxFit.none,
                                ),
                              ),
                            ),
                          ),
                          Spacer(),
                          Align(
                            alignment: Alignment.bottomLeft,
                            child: Container(
                              width: 17,
                              height: 25,
                              child: Opacity(
                                opacity: 1,
                                child: Image.asset(
                                  "assets/images/icon-arrow-left-4-2.png",
                                  fit: BoxFit.none,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}