
//import 'dart:html';

import 'dart:collection';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:project/calendar1_widget/calendar1_widget.dart';
import 'package:project/data.dart';
import 'package:project/homepage_widget/homepage_widget.dart';
import 'package:project/utilities.dart';
import 'package:project/utilities.dart';
import 'package:project/values/values.dart';
import 'package:project/SizeConfig.dart';
//import 'package:image_picker/image_picker.dart';
//import 'package:video_player/video_player.dart';
import 'package:project/frequency_series.dart';
import 'package:syncfusion_flutter_charts/charts.dart';



class Calendar4Widget extends StatelessWidget {

    int a;
    int b;

    //String participationpertimeslot = get_participationpertimeslot(a,b);

    //File _video;



    /*
    _pickVideo() async {
    File video = await ImagePicker.pickVideo(source: ImageSource.gallery);
    _video = video;
    _videoPlayerController = VideoPlayerController.file(_video)..initialize().then((_) {
      setState(() {

      });
      _videoPlayerController.play();
    });
  }

  */
  
  @override
  Widget build(BuildContext context) {
    print('Loading wod:');
    print(selected_wod);
    HashMap <String,Object> wod_info = wods[selected_wod];
    TextEditingController description_controller = new TextEditingController(text:wod_info['description']);
    String ex1 = wod_info['ex1'] !=null? wod_info['ex1'] :"none";
    ex1 = ex1.substring(0, min(ex1.length, 23));
    String ex2 = wod_info['ex2'] !=null? wod_info['ex2'] :"none";
    ex2 = ex2.substring(0, min(ex2.length, 23));
    String ex3 = wod_info['ex3'] !=null? wod_info['ex3'] :"none";
    ex3 = ex3.substring(0, min(ex3.length, 23));
    String ex4 = wod_info['ex4'] !=null? wod_info['ex4'] :"none";
    ex4 = ex4.substring(0, min(ex4.length, 23));
    return Scaffold(
      body: Opacity(
        opacity: 1,
        child: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            color: Color.fromARGB(217, 0, 0, 0),
          ),

          child: ListView(
            padding: EdgeInsets.only(
              left: SizeConfig.screenWidth*0.04,
              right: SizeConfig.screenWidth*0.04,
            ),

            children: <Widget>[


              Container (
                height: SizeConfig.screenHeight*0.03,
                width: SizeConfig.screenWidth*0.9,
              ),

              Container(
                height: SizeConfig.screenHeight*0.12,
                child: Row(
                  children: <Widget>[

                    Container(

                      width: SizeConfig.screenWidth*0.13,
                      child: ButtonTheme(
                        child: FlatButton(
                          child: Opacity(
                            opacity: 1,
                            child: Image.asset(
                              "assets/images/icon-arrow-left-4.png",
                              fit: BoxFit.fill,
                            ),
                          ),
                          onPressed: () {
                            Navigator.pushNamed(context, '/HomePage');
                            print("Pressing Back Button");
                            // Navigator.pushNamed(context, '/AddEvent1Widget');
                          },
                        ),
                      ),
                    ),


                    Container(

                      width: SizeConfig.screenWidth*0.5,
                      margin: EdgeInsets.only(
                        //left: SizeConfig.screenWidth*0.24,
                        top: SizeConfig.screenHeight*0.01,
                        bottom: SizeConfig.screenHeight*0.01,

                      ),
                      child: Opacity(
                        opacity: 0.95622,
                        child: Text(
                          'WOD ' + wod_info['ID'],
                          // alignment: TextAlign.left,
                          style: TextStyle(
                            color: AppColors.secondaryText,
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),

                    Container(

                        width: SizeConfig.screenWidth*0.25,
                        //margin: EdgeInsets.only(
                          //  top: SizeConfig.screenHeight*0.03),
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: ButtonTheme(
                            child: FlatButton(
                              child: Opacity(
                                opacity: 1,
                                child: Image.asset(
                                  "assets/images/picture2-2.png",
                                  fit: BoxFit.fill,
                                ),
                              ),
                              onPressed: () {
                                Navigator.pushNamed(context, '/HomePage');
                                print("Pressing Back Home");
                                // Navigator.pushNamed(context, '/AddEvent1Widget');
                              },
                            ),
                          ),
                        )
                    ),

                  ],
                ),

              ),



              Container( // bordo bianco
                height: SizeConfig.screenHeight*0.05,
                margin: EdgeInsets.only(
                    left: SizeConfig.screenWidth*0.03,
                    right: SizeConfig.screenWidth*0.03, top: SizeConfig.screenHeight*0.03), //70
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5, // 0.5
                    color: Color.fromARGB(255, 187, 206, 138),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(7)), //2
                ),

                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      child: Container(
                        margin:EdgeInsets.only(
                          //right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth*0.04,
                          //top: SizeConfig.screenHeight*0.075,
                        ),
                        child: Text(
                          "name: ",
                          //alignment: TextAlign.left,
                          style: TextStyle(
                            color: Color.fromARGB(255, 156, 178, 184),
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 22,
                          ),
                        ),
                      ),
                    ),
                    // Spacer(),
                    Align(
                      child: Container(
                        margin:EdgeInsets.only(
                          //right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth*0.01,
                          // top: SizeConfig.screenHeight*0.075,
                        ),
                        child: Text(
                          wod_info['name'],
                          style: TextStyle(
                            color: AppColors.accentText,
                            decorationColor: Color.fromARGB(150, 156, 178, 184),
                            decorationThickness: 0.0,
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              Container( // bordo bianco
                width: SizeConfig.screenWidth*0.95,
                height: SizeConfig.screenHeight*0.05,
                margin: EdgeInsets.only(
                  top: SizeConfig.screenHeight*0.01,
                  left: SizeConfig.screenWidth*0.03,
                  right: SizeConfig.screenWidth*0.03,), //70
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5, // 0.5
                    color: Color.fromARGB(255, 187, 206, 138),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(7)), //2
                ),

                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      child: Container(
                        margin:EdgeInsets.only(
                          //right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth*0.02,
                          //top: SizeConfig.screenHeight*0.075,
                        ),
                        child: Text(
                          "type: ",
                          //alignment: TextAlign.left,
                          style: TextStyle(
                            color: Color.fromARGB(255, 156, 178, 184),
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 22,
                          ),
                        ),
                      ),
                    ),
                    // Spacer(),
                    Align(
                      child: Container(
                        width: SizeConfig.screenWidth*0.65,
                        margin:EdgeInsets.only(
                          //right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth*0.01,
                          // top: SizeConfig.screenHeight*0.075,
                        ),
                        child: Text(
                          wod_info['type'],
                          style: TextStyle(
                            color: AppColors.accentText,
                            decorationColor: Color.fromARGB(150, 156, 178, 184),
                            decorationThickness: 0.0,
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),



              Container(
                height: SizeConfig.screenHeight*0.22,
                margin: EdgeInsets.only(
                  top: SizeConfig.screenHeight*0.01,
                  left: SizeConfig.screenWidth*0.03,
                  right: SizeConfig.screenWidth*0.03,), //70
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5, // 0.5
                    color: Color.fromARGB(255, 187, 206, 138),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(7)), //2
                ),

                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [

                    Container(
                      height: SizeConfig.screenHeight*0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight*0.005,
                        left: SizeConfig.screenWidth*0.02,
                        right: SizeConfig.screenWidth*0.02,
                        bottom: SizeConfig.screenHeight*0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.15,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.04,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "Ex 1: ",
                                //alignment: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 156, 178, 184),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.6,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.01,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                ex1,
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      height: SizeConfig.screenHeight*0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight*0.005,
                        left: SizeConfig.screenWidth*0.02,
                        right: SizeConfig.screenWidth*0.02,
                        bottom: SizeConfig.screenHeight*0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.15,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.04,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "Ex 2: ",
                                //alignment: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 156, 178, 184),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.6,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.01,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                ex2,
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      height: SizeConfig.screenHeight*0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight*0.005,
                        left: SizeConfig.screenWidth*0.02,
                        right: SizeConfig.screenWidth*0.02,
                        bottom: SizeConfig.screenHeight*0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.15,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.04,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "Ex 3: ",
                                //alignment: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 156, 178, 184),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.6,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.01,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                ex3,
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      height: SizeConfig.screenHeight*0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight*0.005,
                        left: SizeConfig.screenWidth*0.02,
                        right: SizeConfig.screenWidth*0.02,
                        bottom: SizeConfig.screenHeight*0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.15,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.04,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "Ex 4: ",
                                //alignment: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 156, 178, 184),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.6,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.01,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                ex4,
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),

              Container( // bordo bianco
                height: SizeConfig.screenHeight*0.2,
                margin: EdgeInsets.only(
                    left: SizeConfig.screenWidth*0.03,
                    right: SizeConfig.screenWidth*0.03,
                    top: SizeConfig.screenHeight*0.01
                ), //70
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5, // 0.5
                    color: Color.fromARGB(255, 187, 206, 138),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(7)), //2
                ),

                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      child: Container(
                        width:SizeConfig.screenWidth*0.8,
                        margin:EdgeInsets.only(
                          //right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth*0.01,
                          // top: SizeConfig.screenHeight*0.075,
                        ),
                        child: TextField(
                          maxLines: 13,
                          //text:wod_info['description'] !=null? wod_info['description'] :"none",
                          controller: description_controller,
                          style: TextStyle(
                            color: AppColors.accentText,
                            decorationColor: Color.fromARGB(150, 156, 178, 184),
                            decorationThickness: 0.0,
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              Container(
                height: SizeConfig.screenHeight*0.03,

              ),



              Container(
                height: SizeConfig.screenHeight*0.3,
                margin: EdgeInsets.only(
                  top: SizeConfig.screenHeight*0.01,
                  left: SizeConfig.screenWidth*0.03,
                  right: SizeConfig.screenWidth*0.03,), //70
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(7)), //2
                ),

                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [

                    Container( // bordo bianco
                      height: SizeConfig.screenHeight*0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight*0.005,
                        bottom: SizeConfig.screenHeight*0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.2,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                get_participationpertimeslot(0, 0),
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.15,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.05,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "m/n",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 187, 206, 138),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.3,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,

                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "participants",
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      height: SizeConfig.screenHeight*0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight*0.005,
                        bottom: SizeConfig.screenHeight*0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.2,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                get_participationpertimeslot(1, 0),
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.15,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.05,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "m/n",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 187, 206, 138),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.3,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,

                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "participants",
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      height: SizeConfig.screenHeight*0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight*0.005,
                        bottom: SizeConfig.screenHeight*0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.2,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                get_participationpertimeslot(2, 0),
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.15,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.05,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "m/n",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 187, 206, 138),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.3,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,

                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                  "participants",
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      height: SizeConfig.screenHeight*0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight*0.005,
                        bottom: SizeConfig.screenHeight*0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.2,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                get_participationpertimeslot(3, 0),
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.15,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.05,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "m/n",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 187, 206, 138),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.3,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,

                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "participants",
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      height: SizeConfig.screenHeight*0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight*0.005,
                        bottom: SizeConfig.screenHeight*0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.2,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                get_participationpertimeslot(4, 0),
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.15,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.05,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "m/n",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 187, 206, 138),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.3,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,

                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "participants",
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),


                    Container( // bordo bianco
                      height: SizeConfig.screenHeight*0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight*0.005,
                        bottom: SizeConfig.screenHeight*0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.2,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                get_participationpertimeslot(5, 0),
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.15,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.05,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "m/n",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 187, 206, 138),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.3,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,

                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "participants",
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                  ],
                ),
              ),


              Container (
                margin: EdgeInsets.only(
                    top: SizeConfig.screenHeight*0.06,
                    bottom: SizeConfig.screenHeight*0.01
                ),
                child: Opacity(
                  opacity: 0.95622,
                  child: Text(
                    "Participation per class in this day",
                    // alignment: TextAlign.left,
                    style: TextStyle(
                      color: AppColors.secondaryText,
                      fontFamily: "Lato",
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),

              Container(
                height: SizeConfig.screenWidth*0.005,
                margin: EdgeInsets.only(
                  right: SizeConfig.screenWidth*0.1

                ),
                decoration: BoxDecoration(
                  color: AppColors.primaryElement,
                  border: Border.fromBorderSide(Borders.primaryBorder),
                ),

              ),





              Container( // bordo bianco
                height: SizeConfig.screenHeight*0.4,
                width: SizeConfig.screenWidth*0.5,//70
                margin: EdgeInsets.only(
                    bottom: SizeConfig.screenHeight*0.03,
                    top: SizeConfig.screenHeight*0.05,
                ),

                child: SfCartesianChart(
                  primaryXAxis: CategoryAxis(),
                  //initialize line series
                  series: <ColumnSeries<Participationxclass, String>>[
                    ColumnSeries <Participationxclass,String>(
                      dataSource: [
                        Participationxclass(get_participationpertimeslot(0, 0), int.parse(get_participationpertimeslot(0, 2))),
                        Participationxclass(get_participationpertimeslot(1, 0), int.parse(get_participationpertimeslot(1, 2))),
                        Participationxclass(get_participationpertimeslot(2, 0), int.parse(get_participationpertimeslot(2, 2))),
                        Participationxclass(get_participationpertimeslot(3, 0), int.parse(get_participationpertimeslot(3, 2))),
                        Participationxclass(get_participationpertimeslot(4, 0), int.parse(get_participationpertimeslot(4, 2))),
                        Participationxclass(get_participationpertimeslot(5, 0), int.parse(get_participationpertimeslot(5, 2))),
                      ],
                      xValueMapper: (Participationxclass sales, _) => sales.Hour,
                      yValueMapper: (Participationxclass sales, _) => sales.participants,
                      color: Color.fromARGB(255, 187, 206, 138),

                    ),
                  ],
                ),
              ),










            ],

          ),
        ),
      ),
    );

              /*
              Positioned(
                top: 149,
                child: Opacity(
                  opacity: 1,
                  child: Text(
                    "m/n participants ",
                    // alignment: TextAlign.left,
                    style: TextStyle(
                      color: Color.fromARGB(255, 208, 222, 163),
                      fontFamily: "Lato",
                      fontWeight: FontWeight.w300,
                     // style: FontStyle.italic,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),*/



                            /*
                            Positioned(
                              left: 0,
                              top: 37,
                              child: Opacity(
                                opacity: 1,
                                child: Text(
                                  "07:00",
                                  // alignment: TextAlign.left,
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    fontFamily: "Lato",
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),*/
                            /*
                            Positioned(
                              left: 0,
                              top: 0,
                              child: Opacity(
                                opacity: 0.95622,
                                child: Text(
                                  "Thursday, 7 November",
                                  // alignment: TextAlign.left,
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    fontFamily: "Lato",
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
          */

  }
}