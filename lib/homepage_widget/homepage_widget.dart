import 'package:flutter/material.dart';
import 'package:project/values/values.dart';
import '../SizeConfig.dart';
import '../utilities.dart';
String profilePicture;

class HomepageWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    if(profilePicture == null){
      profilePicture = get_imageProfile(USER);}

    //String profilePicture = get_imageProfile(context,USER);

    return Scaffold(
      body: Opacity(
        opacity: 1,
        child: Container(
          height: SizeConfig.screenHeight,
          width: SizeConfig.screenWidth,
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            color: Color.fromARGB(217, 0, 0, 0),
          ),
          child: Stack(
            //alignment: Alignment.center,
            children: [
              // container coach and settings
              Positioned(
                left: SizeConfig.screenWidth*0.05,
                top: SizeConfig.screenHeight*0.05,
                right: SizeConfig.screenWidth*0.05,
                bottom: SizeConfig.screenHeight*0.05,
                child: Column(
                  // camadonna
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    // container profile image and icons
                    Container(
                      height: SizeConfig.screenHeight*0.16,
                      margin: EdgeInsets.only(top: SizeConfig.screenWidth*0.075, left: SizeConfig.screenWidth*0.05, right: SizeConfig.screenWidth*0.05),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Opacity(
                              opacity: 1,
                              child: Container(
                                width: SizeConfig.screenWidth*0.25,
                                height: SizeConfig.screenHeight*0.15,
                                decoration: BoxDecoration(
                                  color: AppColors.secondaryBackground,
                                  border: Border.all(
                                    color: Color.fromARGB(255, 187, 206, 138),
                                  ),
                                  borderRadius: BorderRadius.all(Radius.circular(50)),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    // container profile image
                                    Container(
                                      height: SizeConfig.screenHeight*0.09,
                                      margin: EdgeInsets.symmetric(horizontal: SizeConfig.screenWidth*0.035),
                                      child: Opacity(
                                        opacity: 1,
                                        child: Image.asset(
                                          profilePicture,
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Spacer(),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Opacity(
                              opacity: 1,
                              // container settings icon
                              child: Container(
                                width: SizeConfig.screenWidth*0.2,
                                height: SizeConfig.screenHeight*0.1,
                                margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.01, left: SizeConfig.screenWidth*0.1),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(25)),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    // container settings icon box
                                    /*Container(
                                      height: SizeConfig.screenHeight*0.1,
                                      child: Opacity(
                                        opacity: 1,
                                        child: Image.asset(
                                          "assets/images/picture1.png",
                                          fit: BoxFit.none,
                                        ),
                                      ),
                                    ),*/
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            // container logout icon box
                            child: Container(
                              width: SizeConfig.screenWidth*0.23,
                              height: SizeConfig.screenHeight*0.1,
                              margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.01, left: SizeConfig.screenWidth*0.01),
                                child: ButtonTheme(
                                  child: FlatButton(
                                    child: Opacity(
                                      opacity: 1,
                                      child: Image.asset(
                                        "assets/images/logout-2.png",
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                    onPressed: () {
                                      logout(context);
                                      // Navigator.pushNamed(context, '/HomePage');
                                    },
                                  ),
                                ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      // container user name
                      child: Container(
                        margin: EdgeInsets.only(left: SizeConfig.screenWidth*0.025),
                        child: Opacity(
                          opacity: 0.95622,
                          child: Text(
                            "Coach: " + USER,
                            //alignment: TextAlign.center,
                            style: TextStyle(
                              color: AppColors.secondaryText,
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w400,
                              fontSize: 24,
                            ),
                          ),
                        ),
                      ),
                    ),


                    // Container first row buttons
                    Container(
                      //height: 100,
                      height: SizeConfig.screenHeight*0.135,
                      margin: EdgeInsets.only(
                          left: SizeConfig.screenWidth*0.05,
                          top: SizeConfig.screenHeight*0.05,
                          right: SizeConfig.screenWidth*0.05,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Opacity(
                              opacity: 1,
                              // container add event
                              child: Container(
                                width: SizeConfig.screenWidth*0.25,
                                height: SizeConfig.screenHeight*0.5,
                                decoration: BoxDecoration(
                                  color: AppColors.primaryBackground,
                                  borderRadius: BorderRadius.all(Radius.circular(50)),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    // container add event image
                                    Container(
                                      height: SizeConfig.screenHeight*0.135,
                                      child: ButtonTheme(
                                        child: FlatButton(
                                          child: Opacity(
                                            opacity: 1,
                                            child: Image.asset(
                                              "assets/images/add-2.png",
                                              fit: BoxFit.fill,
                                            ),
                                      ),
                                      onPressed: () {
                                            print("add event pressed");
                                            clean_add_event_data();
                                            Navigator.pushNamed(context, '/Addevent1');
                                      },
                                    ),
                                  ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Spacer(),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Opacity(
                              opacity: 1,
                              // container reports
                              child: Container(
                                width: SizeConfig.screenWidth*0.25,
                                height: SizeConfig.screenHeight*0.5,
                                decoration: BoxDecoration(
                                  color: AppColors.primaryBackground,
                                  borderRadius: BorderRadius.all(Radius.circular(50)),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    // container reports image
                                    Container(
                                      height: SizeConfig.screenHeight*0.135,
                                      child: ButtonTheme(
                                        child: FlatButton(
                                          child: Opacity(
                                            opacity: 1,
                                            child: Image.asset(
                                              "assets/images/analytics.png",
                                              fit: BoxFit.fill,
                                            ),
                                          ),
                                          onPressed: () {
                                            print("reports pressed");
                                            Navigator.pushNamed(context, '/Reports2', arguments:12);
                                            // Navigator.pushNamed(context, '/AddEvent1Widget');
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    // container first row texts
                    Container(
                      height: SizeConfig.screenHeight*0.05,
                      margin: EdgeInsets.only(
                          right: SizeConfig.screenWidth*0.075,
                          left: SizeConfig.screenWidth*0.035,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Opacity(
                              opacity: 0.95622,
                              child: Text(
                                "Add Event",
                                //alignment: TextAlign.center,
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 24,
                                ),
                              ),
                            ),
                          ),
                          Spacer(),
                          Align(
                            alignment: Alignment.topRight,
                            child: Opacity(
                              opacity: 0.95622,
                              child: Text(
                                "Reports",
                                //alignment: TextAlign.center,
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 24,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                //Container second row buttons
                    Container(
                      height: SizeConfig.screenHeight*0.135,
                      margin: EdgeInsets.only(
                        left: SizeConfig.screenWidth*0.05,
                        top: SizeConfig.screenHeight*0.02,
                        right: SizeConfig.screenWidth*0.05,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Opacity(
                              opacity: 1,
                              // container participants button
                              child: Container(
                                width: SizeConfig.screenWidth*0.25,
                                height: SizeConfig.screenHeight*0.5,
                                decoration: BoxDecoration(
                                  color: AppColors.primaryBackground,
                                  borderRadius: BorderRadius.all(Radius.circular(50)),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    // container participants image
                                    Container(
                                      height: SizeConfig.screenHeight*0.135,
                                      child: ButtonTheme(
                                        child: FlatButton(
                                          child: Opacity(
                                            opacity: 1,
                                            child: Image.asset(
                                              "assets/images/magnifier-with-small-handle-1.png",
                                              fit: BoxFit.fill,
                                            ),
                                          ),
                                          onPressed: () {
                                            //print("participant button pressed");
                                            Navigator.pushNamed(context, '/Partecipants');
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Spacer(),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Opacity(
                              opacity: 1,
                              // container calendar button
                              child: Container(
                                width: SizeConfig.screenWidth*0.25,
                                height: SizeConfig.screenHeight*0.5,
                                decoration: BoxDecoration(
                                  color: AppColors.primaryBackground,
                                  borderRadius: BorderRadius.all(Radius.circular(50)),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    // container calendar image
                                    Container(
                                      height: SizeConfig.screenHeight*0.135,
                                      child: ButtonTheme(
                                        child: FlatButton(
                                          child: Opacity(
                                            opacity: 1,
                                            child: Image.asset(
                                              "assets/images/immagine1-2.png",
                                              fit: BoxFit.fill,
                                            ),
                                          ),
                                          onPressed: () {
                                            print("calendar button pressed");
                                            Navigator.pushNamed(context, '/Calendar1');
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),


                    // container second row text
                    Container(
                      height: SizeConfig.screenHeight*0.05,
                      margin: EdgeInsets.only(
                          right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth*0.01,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Opacity(
                              opacity: 0.95622,
                              child: Text(
                                "Participants",
                                //alignment: TextAlign.center,
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 24,
                                ),
                              ),
                            ),
                          ),
                          Spacer(),
                          Align(
                            alignment: Alignment.topRight,
                            child: Opacity(
                              opacity: 0.95622,
                              child: Text(
                                "Calendar",
                                //alignment: TextAlign.center,
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 24,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    //Spacer(),

                    // container third row buttons
                    Container(
                      height: SizeConfig.screenHeight*0.135,
                      margin: EdgeInsets.only(
                        left: SizeConfig.screenWidth*0.05,
                        top: SizeConfig.screenHeight*0.02,
                        right: SizeConfig.screenWidth*0.05,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.bottomLeft,
                            child: Opacity(
                              opacity: 1,
                              // container last WOD button
                              child: Container(
                                width: SizeConfig.screenWidth*0.25,
                                height: SizeConfig.screenHeight*0.5,
                                decoration: BoxDecoration(
                                  color: AppColors.primaryBackground,
                                  borderRadius: BorderRadius.all(Radius.circular(50)),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    // container last WOD image
                                    Container(
                                      height: SizeConfig.screenHeight*0.135,
                                      child: ButtonTheme(
                                        child: FlatButton(
                                          child: Opacity(
                                            opacity: 1,
                                            child: Image.asset(
                                              "assets/images/previous-2.png",
                                              fit: BoxFit.fill,
                                            ),
                                          ),
                                          onPressed: () {
                                            print("last WOD button pressed");
                                            Navigator.pushNamed(context, '/Calendar4');
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Spacer(),
                          Align(
                            alignment: Alignment.bottomLeft,
                            child: Opacity(
                              opacity: 1,
                              // container next WOD button
                              child: Container(
                                width: SizeConfig.screenWidth*0.25,
                                height: SizeConfig.screenHeight*0.5,
                                decoration: BoxDecoration(
                                  color: AppColors.primaryBackground,
                                  borderRadius: BorderRadius.all(Radius.circular(50)),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    // container next WOD image
                                    Container(
                                      height: SizeConfig.screenHeight*0.135,
                                      child: ButtonTheme(
                                        child: FlatButton(
                                          child: Opacity(
                                            opacity: 1,
                                            child: Image.asset(
                                              "assets/images/previous.png",
                                              fit: BoxFit.fill,
                                            ),
                                          ),
                                          onPressed: () {
                                            print("next WOD button pressed");
                                            Navigator.pushNamed(context, '/Calendar4');
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    //container last WOD and next WOD text
                    Container(
                      height: SizeConfig.screenHeight*0.05,
                      margin: EdgeInsets.only(
                        right: SizeConfig.screenWidth*0.07,
                        left: SizeConfig.screenWidth*0.04,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.bottomLeft,
                            child: Opacity(
                              opacity: 0.95622,
                              child: Text(
                                "Last WOD",
                                //alignment: TextAlign.center,
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 24,
                                ),
                              ),
                            ),
                          ),
                          Spacer(),
                          Align(
                            alignment: Alignment.bottomLeft,
                            child: Opacity(
                              opacity: 0.95622,
                              child: Text(
                                "Necst WOD",
                                //alignment: TextAlign.center,
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 24,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}