import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:month_picker_strip/month_picker_strip.dart';


import '../SizeConfig.dart';
import 'package:project/values/values.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../utilities.dart';



String month;

class Reports2 extends StatefulWidget {
  @override
  _Reports2State createState() => new _Reports2State();
}

class _Reports2State extends State<Reports2> {
  final DateFormat dateFormat = new DateFormat('MMMM yyyy');
  DateTime selectedMonth;

  @override
  void initState() {
    super.initState();
    selectedMonth = new DateTime(2019, 12);
  }

  HashMap reportsdata = getreportsdata();

  @override
  Widget build(BuildContext context) {

    month = dateFormat.format(selectedMonth);


    List<List> presencesdata = reportsdata[month][0]; //  to plot presences
    List<conspresences> consegutivepreseces = new List<conspresences>();
    for (int i=0; i<5; i++){ // presencesdata.length
      consegutivepreseces.add(conspresences(presencesdata[i][0], int.parse(presencesdata[i][1])));
    }

    List<List> participationperdaydata = reportsdata[month][1]; // to plot participation per day
    List<participants> participantperday = new List<participants>();
    for (int i=0; i<4; i++){ // participationperdaydata.length
      participantperday.add(participants(participationperdaydata[i][0],int.parse(participationperdaydata[i][1])));
    }


    List<List> participationpertimeslotdata = reportsdata[month][2]; // to plot participation per time slot
    List<participantsslot> participantpertimeslot = new List<participantsslot>();
    for (int i =0; i<4; i++){ //participationpertimeslotdata .length
      participantpertimeslot.add(participantsslot(participationpertimeslotdata[i][0],int.parse(participationpertimeslotdata[i][1])));
    }




    return new Scaffold(
      body: new SafeArea(

        child: Container(
          color: Color.fromARGB(217, 0, 0, 0),
          child: new Column(
          children: <Widget>[

            // FIRST ROW: REPORTS + home button

            Row(
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(
                    left: SizeConfig.screenWidth*0.33
                  ),

                  child: Text("REPORTS",
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: "Lato",
                      fontWeight: FontWeight.w400,
                      fontSize: 30,
                    ),
                  ),

                ),
                Spacer(),
                Container(
                  height: SizeConfig.screenHeight*0.05,
                  width: SizeConfig.screenHeight*0.1,
                  child: ButtonTheme(
                    child: FlatButton(
                      child: Opacity(
                        opacity: 1,
                        child: Image.asset(
                          "assets/images/picture2-2.png",
                          fit: BoxFit.fill,
                        ),
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, '/HomePage');
                        print("Pressing Back Home");
                      },
                    ),
                  ),
                ),
              ],
            ),

            // SECOND ROW: Calendar

            new Divider(
              height: 1.0,
              color: Colors.white,
            ),
            new MonthStrip(
              format: 'MMM yyyy',
              from: new DateTime(2019, 1),
              to: new DateTime(2020, 12),
              initialMonth: selectedMonth,
              height: 48.0,
              viewportFraction: 0.25, // 0.25
              onMonthChanged: (v) {
                setState(() {
                  selectedMonth = v;
                });
              },

            ),
            new Divider(
              height: 1.0,
                color: Colors.white,
            ),


            // LIST VIEW

            Expanded(
              child: Container(// contiene tutto
                // height: SizeConfig.screenHeight,
                width: SizeConfig.screenWidth,

                decoration: BoxDecoration( // decorazione dis sfondo
                  color: Color.fromARGB(217, 0, 0, 0),
                ),

                child: ListView(
                  padding: EdgeInsets.only(
                      right: SizeConfig.screenWidth*0.05,
                      left: SizeConfig.screenWidth*0.05
                  ),
                  children: <Widget>[

                    // SECONDA ROW: streak ranking e linea sotto


                    Container(
                      height: SizeConfig.screenHeight*0.05,
                      // width: SizeConfig.screenWidth*0.95,
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight*0.03
                      ),

                      // margin: EdgeInsets.only(bottom: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              margin: EdgeInsets.only(), //left: 50, top: 40
                              child: Opacity(
                                opacity: 0.95622,
                                child: Text(
                                  "Streak ranking",
                                  // alignment: TextAlign.left,
                                  style: TextStyle(
                                    color: AppColors.secondaryText,
                                    fontFamily: "Lato",
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Align( // linea sotto streak ranking
                            alignment: Alignment.centerLeft,
                            child: Opacity(
                              opacity: 1,
                              child: Container(
                                width: 260,
                                height: 1, //10
                                margin: EdgeInsets.only(), //left: 50, top: 7
                                decoration: BoxDecoration(
                                  color: AppColors.primaryElement,
                                  border: Border.fromBorderSide(Borders.primaryBorder),
                                ),
                                child: Container(),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),




                    // TERZA ROW-Colonna: istogramma e podio

                    Container(
                      height: SizeConfig.screenHeight*0.44,
                      width: SizeConfig.screenWidth*0.95,


                      alignment: Alignment.center,
                      child: Column(
                        // alignment: Alignment.center,
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            child: SfCartesianChart(
                              // Initialize category axis
                              primaryXAxis: CategoryAxis(),
                              series: <ColumnSeries<conspresences, String>>[
                                ColumnSeries<conspresences, String>(
                                  // Bind data source
                                  dataSource:  consegutivepreseces, // plot of the five participant with most presences
                                  xValueMapper: (conspresences sales, _) => sales.persone,
                                  yValueMapper: (conspresences sales, _) => sales.presenze,
                                  color: Color.fromARGB(255, 187, 206, 138), // specifica il colore delle colonne dell'isto
                                ),
                              ],
                            ),
                          ),

                        ],
                      ),
                    ),



// QUARTA ROW: Partecipation per day e linea sotto


                    Container(
                      height: SizeConfig.screenHeight*0.05,
                      width: SizeConfig.screenWidth*0.95,
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              margin: EdgeInsets.only(), //left: 50, top: 40
                              child: Opacity(
                                opacity: 0.95622,
                                child: Text(
                                  "Participation per day",
                                  // alignment: TextAlign.left,
                                  style: TextStyle(
                                    color: AppColors.secondaryText,
                                    fontFamily: "Lato",
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Align( // linea sotto streak ranking
                            alignment: Alignment.centerLeft,
                            child: Opacity(
                              opacity: 1,
                              child: Container(
                                width: 260,
                                height: 1, //10
                                margin: EdgeInsets.only(), //left: 50, top: 7
                                decoration: BoxDecoration(
                                  color: AppColors.primaryElement,
                                  border: Border.fromBorderSide(Borders.primaryBorder),
                                ),
                                child: Container(),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),


// QUINTA ROW: contiene il grafico Partecipanti/giorno

                    Container(
                      margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.01),
                      height: SizeConfig.screenHeight*0.37,
                      // width: SizeConfig.screenWidth*0.95,
                      // margin: EdgeInsets.only(bottom: 49),

                      /*child: Opacity(
                    opacity: 1,
                    child: Image.asset(
                      "assets/images/immagine2.png",
                    ),
                  ),*/

                      child: SfCartesianChart(
                        // Initialize category axis
                        primaryXAxis: CategoryAxis(),
                        series: <ColumnSeries<participants, String>>[
                          ColumnSeries<participants, String>(
                            // Bind data source
                            dataSource:  participantperday,
                            xValueMapper: (participants sales, _) => sales.giorno,
                            yValueMapper: (participants sales, _) => sales.partecipanti,
                            color: Color.fromARGB(255, 187, 206, 138), // specifica il colore delle colonne dell'isto
                          ),
                        ],
                      ),




                    ),




                    // SESTA ROW: Partecipation per time slot e line sotto

                    Container(
                      height: SizeConfig.screenHeight*0.05,
                      width: SizeConfig.screenWidth*0.95,
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.07),
                      // margin: EdgeInsets.only(bottom: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              margin: EdgeInsets.only(), //left: 50, top: 40
                              child: Opacity(
                                opacity: 0.95622,
                                child: Text(
                                  "Participation per time slot",
                                  // alignment: TextAlign.left,
                                  style: TextStyle(
                                    color: AppColors.secondaryText,
                                    fontFamily: "Lato",
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Align( // linea sotto streak ranking
                            alignment: Alignment.centerLeft,
                            child: Opacity(
                              opacity: 1,
                              child: Container(
                                width: 260,
                                height: 1, //10
                                margin: EdgeInsets.only(), //left: 50, top: 7
                                decoration: BoxDecoration(
                                  color: AppColors.primaryElement,
                                  border: Border.fromBorderSide(Borders.primaryBorder),
                                ),
                                child: Container(),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),


                    // SETTIMA ROW: contiene grafico partecipanti/fascia oraria

                    Container(
                      margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.01),
                      height: SizeConfig.screenHeight*0.3,
                      width: SizeConfig.screenWidth*0.95,
                      // margin: EdgeInsets.only(bottom: 51),

                      /* child: Opacity(
                    opacity: 1,
                    child: Image.asset(
                      "assets/images/immagine1-5.png",
                    ),
                  ),*/

                      child: SfCartesianChart(
                        // Initialize category axis
                        primaryXAxis: CategoryAxis(),
                        series: <ColumnSeries<participantsslot, String>>[
                          ColumnSeries<participantsslot, String>(
                            // Bind data source
                            dataSource:  participantpertimeslot,
                            xValueMapper: (participantsslot sales, _) => sales.fasciaoraria,
                            yValueMapper: (participantsslot sales, _) => sales.partecipanti,
                            color: Color.fromARGB(255, 187, 206, 138), // specifica il colore delle colonne dell'isto
                          ),
                        ],
                      ),






                    ),


                    // Ottava ROW: download


                    Container(
                      height: SizeConfig.screenHeight*0.05,
                      // width: SizeConfig.screenWidth*0.1,
                      decoration: BoxDecoration(
                        color: Color.fromARGB(255, 187, 206, 138),
                        border: Border.fromBorderSide(Borders.primaryBorder),
                        borderRadius: BorderRadius.all(Radius.circular(2)),
                      ),
                      margin:  EdgeInsets.only(
                        left: SizeConfig.screenWidth*0.2,
                        right: SizeConfig.screenWidth*0.2,
                        top: SizeConfig.screenHeight*0.05,
                        bottom: SizeConfig.screenHeight*0.05,
                      ),
                      alignment: Alignment.center,
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin:  EdgeInsets.only(left: SizeConfig.screenWidth*0.02, ),
                            child:Opacity( // simbolo download
                              opacity: 1,
                              child: Image.asset(
                                "assets/images/download-arrow-2.png",
                                fit: BoxFit.none,
                              ),
                            ),
                          ),
                          Container(
                            margin:  EdgeInsets.only(left: SizeConfig.screenWidth*0.02),
                            child: Opacity( // testo
                              opacity: 0.95622,
                              child: FlatButton(
                                onPressed: () {
                                  print('Pressing download csv');
                                },
                                child: Text(
                                  "DOWNLOAD csv",
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 64, 80, 85),
                                    fontFamily: "Lato",
                                    fontWeight: FontWeight.w700,
                                    fontSize: 11,
                                  ),
                                ),

                              ),
                            ),
                          ),
                        ],
                      ),


                    ),





                  ],
                ),



              ),

            ),
          ],
        ),
      ),
      ),
    );
  }
}

class conspresences {
  conspresences(this.persone, this.presenze);
  final String persone;
  final int presenze;

}

class participants {
  participants(this.giorno, this. partecipanti);
  final String giorno;
  final int partecipanti;

}

class participantsslot {
  participantsslot(this.fasciaoraria, this. partecipanti);
  final String fasciaoraria;
  final int partecipanti;
}