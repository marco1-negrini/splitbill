
import 'package:flutter/material.dart';
import 'package:project/values/values.dart';


class Calendar5Widget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      body: Opacity(
        opacity: 1,
        child: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            color: Color.fromARGB(217, 0, 0, 0),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Align(
                alignment: Alignment.topRight,
                child: Container(
                  width: 50,
                  height: 49,
                  margin: EdgeInsets.only(top: 50, right: 25),
                  child: Opacity(
                    opacity: 1,
                    child: Image.asset(
                      "assets/images/picture2-7.png",
                      fit: BoxFit.none,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 184,
                  height: 217,
                  margin: EdgeInsets.only(left: 50, top: 1),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Positioned(
                        left: 0,
                        top: 0,
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              left: 0,
                              top: 0,
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  Positioned(
                                    left: 0,
                                    top: 0,
                                    child: Stack(
                                      alignment: Alignment.center,
                                      children: [
                                        Positioned(
                                          left: 0,
                                          top: 0,
                                          child: Stack(
                                            alignment: Alignment.center,
                                            children: [
                                              Positioned(
                                                left: 0,
                                                top: 0,
                                                child: Stack(
                                                  alignment: Alignment.center,
                                                  children: [
                                                    Positioned(
                                                      left: 0,
                                                      top: 0,
                                                      child: Stack(
                                                        alignment: Alignment.center,
                                                        children: [
                                                          Positioned(
                                                            left: 0,
                                                            top: 0,
                                                            child: Stack(
                                                              alignment: Alignment.center,
                                                              children: [
                                                                Positioned(
                                                                  left: 0,
                                                                  top: 0,
                                                                  child: Stack(
                                                                    alignment: Alignment.center,
                                                                    children: [
                                                                      Positioned(
                                                                        left: 0,
                                                                        top: 0,
                                                                        child: Stack(
                                                                          alignment: Alignment.center,
                                                                          children: [
                                                                            Positioned(
                                                                              left: 0,
                                                                              top: 0,
                                                                              child: Stack(
                                                                                alignment: Alignment.center,
                                                                                children: [
                                                                                  Positioned(
                                                                                    left: 0,
                                                                                    top: 0,
                                                                                    child: Stack(
                                                                                      alignment: Alignment.center,
                                                                                      children: [
                                                                                        Positioned(
                                                                                          left: 0,
                                                                                          top: 37,
                                                                                          child: Opacity(
                                                                                            opacity: 1,
                                                                                            child: Text(
                                                                                              "07:00",
                                                                                              //alignment: TextAlign.left,
                                                                                              style: TextStyle(
                                                                                                color: Color.fromARGB(255, 255, 255, 255),
                                                                                                fontFamily: "Lato",
                                                                                                fontWeight: FontWeight.w300,
                                                                                                fontSize: 18,
                                                                                              ),
                                                                                            ),
                                                                                          ),
                                                                                        ),
                                                                                        Positioned(
                                                                                          left: 0,
                                                                                          top: 0,
                                                                                          child: Opacity(
                                                                                            opacity: 0.95622,
                                                                                            child: Text(
                                                                                              "Monday, 11 November",
                                                                                              //alignment: TextAlign.left,
                                                                                              style: TextStyle(
                                                                                                color: Color.fromARGB(255, 255, 255, 255),
                                                                                                fontFamily: "Lato",
                                                                                                fontWeight: FontWeight.w300,
                                                                                                fontSize: 18,
                                                                                              ),
                                                                                            ),
                                                                                          ),
                                                                                        ),
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                                  Positioned(
                                                                                    left: 75,
                                                                                    top: 49,
                                                                                    child: Opacity(
                                                                                      opacity: 1,
                                                                                      child: Text(
                                                                                        "n participants ",
                                                                                        //alignment: TextAlign.left,
                                                                                        style: TextStyle(
                                                                                          color: Color.fromARGB(255, 208, 222, 163),
                                                                                          fontFamily: "Lato",
                                                                                          fontWeight: FontWeight.w300,
                                                                                          //style: FontStyle.italic,
                                                                                          fontSize: 18,
                                                                                        ),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                            Positioned(
                                                                              left: 0,
                                                                              top: 66,
                                                                              child: Opacity(
                                                                                opacity: 1,
                                                                                child: Text(
                                                                                  "07:45",
                                                                                  //alignment: TextAlign.left,
                                                                                  style: TextStyle(
                                                                                    color: Color.fromARGB(255, 255, 255, 255),
                                                                                    fontFamily: "Lato",
                                                                                    fontWeight: FontWeight.w300,
                                                                                    fontSize: 18,
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                      Positioned(
                                                                        left: 75,
                                                                        top: 77,
                                                                        child: Opacity(
                                                                          opacity: 1,
                                                                          child: Text(
                                                                            "n participants ",
                                                                            //alignment: TextAlign.left,
                                                                            style: TextStyle(
                                                                              color: Color.fromARGB(255, 214, 226, 170),
                                                                              fontFamily: "Lato",
                                                                              fontWeight: FontWeight.w300,
                                                                              //style: FontStyle.italic,
                                                                              fontSize: 18,
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Positioned(
                                                                  left: 0,
                                                                  top: 95,
                                                                  child: Opacity(
                                                                    opacity: 1,
                                                                    child: Text(
                                                                      "12:30",
                                                                      // alignment: TextAlign.left,
                                                                      style: TextStyle(
                                                                        color: Color.fromARGB(255, 255, 255, 255),
                                                                        fontFamily: "Lato",
                                                                        fontWeight: FontWeight.w300,
                                                                        fontSize: 18,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          Positioned(
                                                            left: 75,
                                                            top: 106,
                                                            child: Opacity(
                                                              opacity: 1,
                                                              child: Text(
                                                                "n participants ",
                                                                //alignment: TextAlign.left,
                                                                style: TextStyle(
                                                                  color: Color.fromARGB(255, 219, 230, 177),
                                                                  fontFamily: "Lato",
                                                                  fontWeight: FontWeight.w300,
                                                                 // style: FontStyle.italic,
                                                                  fontSize: 18,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Positioned(
                                                      left: 0,
                                                      top: 123,
                                                      child: Opacity(
                                                        opacity: 1,
                                                        child: Text(
                                                          "13:15",
                                                          //alignment: TextAlign.left,
                                                          style: TextStyle(
                                                            color: Color.fromARGB(255, 255, 255, 255),
                                                            fontFamily: "Lato",
                                                            fontWeight: FontWeight.w300,
                                                            fontSize: 18,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Positioned(
                                                left: 75,
                                                top: 135,
                                                child: Opacity(
                                                  opacity: 1,
                                                  child: Text(
                                                    "n participants ",
                                                    //alignment: TextAlign.left,
                                                    style: TextStyle(
                                                      color: Color.fromARGB(255, 214, 226, 170),
                                                      fontFamily: "Lato",
                                                      fontWeight: FontWeight.w300,
                                                      //style: FontStyle.italic,
                                                      fontSize: 18,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Positioned(
                                          left: 0,
                                          top: 152,
                                          child: Opacity(
                                            opacity: 1,
                                            child: Text(
                                              "18:30",
                                              //alignment: TextAlign.left,
                                              style: TextStyle(
                                                color: Color.fromARGB(255, 255, 255, 255),
                                                fontFamily: "Lato",
                                                fontWeight: FontWeight.w300,
                                                fontSize: 18,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    left: 75,
                                    top: 165,
                                    child: Opacity(
                                      opacity: 1,
                                      child: Text(
                                        "n participants ",
                                        // alignment: TextAlign.left,
                                        style: TextStyle(
                                          color: Color.fromARGB(255, 214, 226, 170),
                                          fontFamily: "Lato",
                                          fontWeight: FontWeight.w300,
                                          // style: FontStyle.italic,
                                          fontSize: 18,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Positioned(
                              left: 0,
                              top: 182,
                              child: Opacity(
                                opacity: 1,
                                child: Text(
                                  "19:15",
                                  //alignment: TextAlign.left,
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    fontFamily: "Lato",
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Positioned(
                        left: 75,
                        top: 195,
                        child: Opacity(
                          opacity: 1,
                          child: Text(
                            "n participants ",
                            //alignment: TextAlign.left,
                            style: TextStyle(
                              color: Color.fromARGB(255, 214, 226, 170),
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w300,
                              //style: FontStyle.italic,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Spacer(),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 300,
                  height: 490,
                  margin: EdgeInsets.only(bottom: 192),
                  child: Opacity(
                    opacity: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Opacity(
                            opacity: 0.95622,
                            child: Text(
                              "WOD name: ",
                              //alignment: TextAlign.left,
                              style: TextStyle(
                                color: Color.fromARGB(255, 255, 255, 255),
                                fontFamily: "Lato",
                                fontWeight: FontWeight.w400,
                                fontSize: 24,
                              ),
                            ),
                          ),
                        ),
                        Opacity(
                          opacity: 1,
                          child: Container(
                            height: 1,
                            margin: EdgeInsets.only(top: 1),
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 216, 216, 216),
                              border: Border.all(
                                width: 0.5,
                                color: Color.fromARGB(255, 151, 151, 151),
                              ),
                            ),
                            child: Container(),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(top: 19),
                            child: Opacity(
                              opacity: 1,
                              child: Text(
                                "Insert WOD name",
                                //alignment: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 166, 187, 193),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(top: 33),
                            child: Opacity(
                              opacity: 0.95622,
                              child: Text(
                                "WOD type: ",
                                //alignment: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 255, 255, 255),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 24,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Opacity(
                          opacity: 1,
                          child: Container(
                            height: 1,
                            margin: EdgeInsets.only(top: 1),
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 216, 216, 216),
                              border: Border.all(
                                width: 0.5,
                                color: Color.fromARGB(255, 151, 151, 151),
                              ),
                            ),
                            child: Container(),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(top: 19),
                            child: Opacity(
                              opacity: 1,
                              child: Text(
                                "Insert WOD type",
                                //alignment: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 175, 195, 200),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Opacity(
                          opacity: 0.15,
                          child: Container(
                            height: 1,
                            margin: EdgeInsets.only(top: 58),
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 216, 216, 216),
                              border: Border.all(
                                width: 0.5,
                                color: Color.fromARGB(255, 151, 151, 151),
                              ),
                            ),
                            child: Container(),
                          ),
                        ),
                        Opacity(
                          opacity: 0.15,
                          child: Container(
                            height: 1,
                            margin: EdgeInsets.only(top: 49),
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 216, 216, 216),
                              border: Border.all(
                                width: 0.5,
                                color: Color.fromARGB(255, 151, 151, 151),
                              ),
                            ),
                            child: Container(),
                          ),
                        ),
                        Opacity(
                          opacity: 0.15,
                          child: Container(
                            height: 1,
                            margin: EdgeInsets.only(top: 49),
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 216, 216, 216),
                              border: Border.all(
                                width: 0.5,
                                color: Color.fromARGB(255, 151, 151, 151),
                              ),
                            ),
                            child: Container(),
                          ),
                        ),
                        Spacer(),
                        Opacity(
                          opacity: 0.15,
                          child: Container(
                            height: 1,
                            margin: EdgeInsets.only(bottom: 24),
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 216, 216, 216),
                              border: Border.all(
                                width: 0.5,
                                color: Color.fromARGB(255, 151, 151, 151),
                              ),
                            ),
                            child: Container(),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(bottom: 1),
                            child: Opacity(
                              opacity: 0.95622,
                              child: Text(
                                "WOD score: ",
                                //alignment: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 255, 255, 255),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 24,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Opacity(
                          opacity: 1,
                          child: Container(
                            height: 1,
                            margin: EdgeInsets.only(bottom: 19),
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 216, 216, 216),
                              border: Border.all(
                                width: 0.5,
                                color: Color.fromARGB(255, 151, 151, 151),
                              ),
                            ),
                            child: Container(),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 80, bottom: 8),
                          child: Opacity(
                            opacity: 1,
                            child: Text(
                              "Generated score description",
                              //alignment: TextAlign.left,
                              style: TextStyle(
                                color: Color.fromARGB(255, 198, 215, 219),
                                fontFamily: "Lato",
                                fontWeight: FontWeight.w300,
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 273,
                  height: 88,
                  margin: EdgeInsets.only(bottom: 91),
                  child: Opacity(
                    opacity: 1,
                    child: Row(
                      children: [
                        Container(
                          width: 88,
                          height: 88,
                          child: Opacity(
                            opacity: 1,
                            child: Image.asset(
                              "assets/images/key-4.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                        Spacer(),
                        Container(
                          margin: EdgeInsets.only(right: 2),
                          child: Opacity(
                            opacity: 1,
                            child: Text(
                              "mississipi",
                              //alignment: TextAlign.left,
                              style: TextStyle(
                                color: Color.fromARGB(255, 255, 255, 255),
                                fontFamily: "Lato",
                                fontWeight: FontWeight.w300,
                                fontSize: 36,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                height: 25,
                margin: EdgeInsets.only(left: 138, right: 170),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                        width: 17,
                        height: 25,
                        child: Opacity(
                          opacity: 1,
                          child: Image.asset(
                            "assets/images/icon-arrow-left-4-2.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                        width: 17,
                        height: 25,
                        child: Opacity(
                          opacity: 1,
                          child: Image.asset(
                            "assets/images/icon-arrow-left-4-2.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}