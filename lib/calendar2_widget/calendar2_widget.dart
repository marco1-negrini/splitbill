
import 'package:flutter/material.dart';
import 'package:project/calendar1_widget/calendar1_widget.dart';
import 'package:project/homepage_widget/homepage_widget.dart';
import 'package:project/values/values.dart';
import 'package:project/utilities.dart';
import 'package:project/SizeConfig.dart';



class Calendar2Widget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Opacity(
        opacity: 1,
        child: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            color: Color.fromARGB(217, 0, 0, 0),
          ),

          child: ListView(
            padding: EdgeInsets.only(
              left: SizeConfig.screenWidth * 0.04,
              right: SizeConfig.screenWidth * 0.04,
            ),

            children: <Widget>[


              Container(
                height: SizeConfig.screenHeight * 0.03,
                width: SizeConfig.screenWidth * 0.9,
              ),

              Container(
                height: SizeConfig.screenHeight * 0.12,
                child: Row(
                  children: <Widget>[

                    Container(

                      width: SizeConfig.screenWidth * 0.13,
                      child: ButtonTheme(
                        child: FlatButton(
                          child: Opacity(
                            opacity: 1,
                            child: Image.asset(
                              "assets/images/icon-arrow-left-4.png",
                              fit: BoxFit.fill,
                            ),
                          ),
                          onPressed: () {
                            Navigator.pushNamed(context, '/Calendar1');;
                            print("Pressing Back Button");
                            // Navigator.pushNamed(context, '/AddEvent1Widget');
                          },
                        ),
                      ),
                    ),


                    Container(

                      width: SizeConfig.screenWidth * 0.5,
                      margin: EdgeInsets.only(
                        //left: SizeConfig.screenWidth*0.24,
                        top: SizeConfig.screenHeight * 0.01,
                        bottom: SizeConfig.screenHeight * 0.01,

                      ),
                      child: Opacity(
                        opacity: 0.95622,
                        child: Text(
                          "Thursday, 7 November",
                          // alignment: TextAlign.left,
                          style: TextStyle(
                            color: AppColors.secondaryText,
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),

                    Container(

                        width: SizeConfig.screenWidth * 0.25,
                        //margin: EdgeInsets.only(
                        //  top: SizeConfig.screenHeight*0.03),
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: ButtonTheme(
                            child: FlatButton(
                              child: Opacity(
                                opacity: 1,
                                child: Image.asset(
                                  "assets/images/picture2-2.png",
                                  fit: BoxFit.fill,
                                ),
                              ),
                              onPressed: () {
                                Navigator.pushNamed(context, '/HomePage');
                                print("Pressing Back Home");
                                // Navigator.pushNamed(context, '/AddEvent1Widget');
                              },
                            ),
                          ),
                        )
                    ),

                  ],
                ),

              ),


              Container( // bordo bianco
                height: SizeConfig.screenHeight * 0.05,
                margin: EdgeInsets.only(
                    left: SizeConfig.screenWidth * 0.03,
                    right: SizeConfig.screenWidth * 0.03,
                    top: SizeConfig.screenHeight * 0.03), //70
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5, // 0.5
                    color: Color.fromARGB(255, 187, 206, 138),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(7)), //2
                ),

                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      child: Container(
                        margin: EdgeInsets.only(
                          //right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth * 0.04,
                          //top: SizeConfig.screenHeight*0.075,
                        ),
                        child: Text(
                          "WOD name: ",
                          //alignment: TextAlign.left,
                          style: TextStyle(
                            color: Color.fromARGB(255, 156, 178, 184),
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 22,
                          ),
                        ),
                      ),
                    ),
                    // Spacer(),
                    Align(
                      child: Container(
                        margin: EdgeInsets.only(
                          //right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth * 0.01,
                          // top: SizeConfig.screenHeight*0.075,
                        ),
                        child: Text(
                          "wod name",
                          style: TextStyle(
                            color: AppColors.accentText,
                            decorationColor: Color.fromARGB(150, 156, 178, 184),
                            decorationThickness: 0.0,
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              Container( // bordo bianco
                width: SizeConfig.screenWidth * 0.95,
                height: SizeConfig.screenHeight * 0.05,
                margin: EdgeInsets.only(
                  top: SizeConfig.screenHeight * 0.01,
                  left: SizeConfig.screenWidth * 0.03,
                  right: SizeConfig.screenWidth * 0.03,),
                //70
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5, // 0.5
                    color: Color.fromARGB(255, 187, 206, 138),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(7)), //2
                ),

                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      child: Container(
                        margin: EdgeInsets.only(
                          //right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth * 0.04,
                          //top: SizeConfig.screenHeight*0.075,
                        ),
                        child: Text(
                          "WOD type: ",
                          //alignment: TextAlign.left,
                          style: TextStyle(
                            color: Color.fromARGB(255, 156, 178, 184),
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 22,
                          ),
                        ),
                      ),
                    ),
                    // Spacer(),
                    Align(
                      child: Container(
                        width: SizeConfig.screenWidth * 0.5,
                        margin: EdgeInsets.only(
                          //right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth * 0.01,
                          // top: SizeConfig.screenHeight*0.075,
                        ),
                        child: Text(
                          "wod_type",
                          style: TextStyle(
                            color: AppColors.accentText,
                            decorationColor: Color.fromARGB(150, 156, 178, 184),
                            decorationThickness: 0.0,
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),


              Container(
                height: SizeConfig.screenHeight * 0.252,
                margin: EdgeInsets.only(
                  top: SizeConfig.screenHeight * 0.01,
                  left: SizeConfig.screenWidth * 0.03,
                  right: SizeConfig.screenWidth * 0.03,), //70
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5, // 0.5
                    color: Color.fromARGB(255, 187, 206, 138),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(7)), //2
                ),

                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [

                    Container(
                      height: SizeConfig.screenHeight * 0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight * 0.005,
                        left: SizeConfig.screenWidth * 0.02,
                        right: SizeConfig.screenWidth * 0.02,
                        bottom: SizeConfig.screenHeight * 0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.1,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth * 0.04,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "Ex 1: ",
                                //alignment: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 156, 178, 184),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.6,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth * 0.01,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "es1",
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(
                                      150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      height: SizeConfig.screenHeight * 0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight * 0.005,
                        left: SizeConfig.screenWidth * 0.02,
                        right: SizeConfig.screenWidth * 0.02,
                        bottom: SizeConfig.screenHeight * 0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.1,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth * 0.04,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "Ex 1: ",
                                //alignment: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 156, 178, 184),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.6,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth * 0.01,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "es1",
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(
                                      150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      height: SizeConfig.screenHeight * 0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight * 0.005,
                        left: SizeConfig.screenWidth * 0.02,
                        right: SizeConfig.screenWidth * 0.02,
                        bottom: SizeConfig.screenHeight * 0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.1,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth * 0.04,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "Ex 1: ",
                                //alignment: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 156, 178, 184),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.6,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth * 0.01,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "es1",
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(
                                      150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      height: SizeConfig.screenHeight * 0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight * 0.005,
                        left: SizeConfig.screenWidth * 0.02,
                        right: SizeConfig.screenWidth * 0.02,
                        bottom: SizeConfig.screenHeight * 0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.1,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth * 0.04,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "Ex 1: ",
                                //alignment: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 156, 178, 184),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.6,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth * 0.01,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "es1",
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(
                                      150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      height: SizeConfig.screenHeight * 0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight * 0.005,
                        left: SizeConfig.screenWidth * 0.02,
                        right: SizeConfig.screenWidth * 0.02,
                        bottom: SizeConfig.screenHeight * 0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.1,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth * 0.04,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "Ex 1: ",
                                //alignment: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 156, 178, 184),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.6,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth * 0.01,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "es1",
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(
                                      150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),

              Container( // bordo bianco
                height: SizeConfig.screenHeight * 0.05,
                margin: EdgeInsets.only(
                    left: SizeConfig.screenWidth * 0.03,
                    right: SizeConfig.screenWidth * 0.03,
                    top: SizeConfig.screenHeight * 0.01
                ), //70
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5, // 0.5
                    color: Color.fromARGB(255, 187, 206, 138),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(7)), //2
                ),

                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      child: Container(
                        margin: EdgeInsets.only(
                          //right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth * 0.04,
                          //top: SizeConfig.screenHeight*0.075,
                        ),
                        child: Text(
                          "WOD score: ",
                          //alignment: TextAlign.left,
                          style: TextStyle(
                            color: Color.fromARGB(255, 156, 178, 184),
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 22,
                          ),
                        ),
                      ),
                    ),
                    // Spacer(),
                    Align(
                      child: Container(
                        margin: EdgeInsets.only(
                          //right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth * 0.01,
                          // top: SizeConfig.screenHeight*0.075,
                        ),
                        child: Text(
                          "wod score",
                          style: TextStyle(
                            color: AppColors.accentText,
                            decorationColor: Color.fromARGB(150, 156, 178, 184),
                            decorationThickness: 0.0,
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              Container(
                height: SizeConfig.screenHeight * 0.03,

              ),


              Container(
                height: SizeConfig.screenHeight * 0.3,
                margin: EdgeInsets.only(
                  top: SizeConfig.screenHeight * 0.01,
                  left: SizeConfig.screenWidth * 0.03,
                  right: SizeConfig.screenWidth * 0.03,), //70
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(7)), //2
                ),

                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [

                    Container( // bordo bianco
                      height: SizeConfig.screenHeight * 0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight * 0.005,
                        bottom: SizeConfig.screenHeight * 0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.2,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                get_participationpertimeslot(0, 0),
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.15,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth * 0.05,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "m/n",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 187, 206, 138),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.3,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,

                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "participants",
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(
                                      150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      height: SizeConfig.screenHeight * 0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight * 0.005,
                        bottom: SizeConfig.screenHeight * 0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.2,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                get_participationpertimeslot(1, 0),
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.15,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth * 0.05,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "m/n",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 187, 206, 138),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.3,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,

                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "participants",
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(
                                      150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      height: SizeConfig.screenHeight * 0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight * 0.005,
                        bottom: SizeConfig.screenHeight * 0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.2,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                get_participationpertimeslot(2, 0),
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.15,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth * 0.05,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "m/n",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 187, 206, 138),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.3,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,

                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "participants",
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(
                                      150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      height: SizeConfig.screenHeight * 0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight * 0.005,
                        bottom: SizeConfig.screenHeight * 0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.2,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                get_participationpertimeslot(3, 0),
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.15,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth * 0.05,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "m/n",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 187, 206, 138),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.3,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,

                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "participants",
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(
                                      150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      height: SizeConfig.screenHeight * 0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight * 0.005,
                        bottom: SizeConfig.screenHeight * 0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.2,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                get_participationpertimeslot(4, 0),
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.15,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth * 0.05,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "m/n",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 187, 206, 138),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.3,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,

                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "participants",
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(
                                      150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),


                    Container( // bordo bianco
                      height: SizeConfig.screenHeight * 0.04,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight * 0.005,
                        bottom: SizeConfig.screenHeight * 0.005,
                      ), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.2,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                get_participationpertimeslot(5, 0),
                                style: TextStyle(
                                  color: AppColors.secondaryText,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.15,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth * 0.05,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "m/n",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 187, 206, 138),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),

                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth * 0.3,
                              margin: EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,

                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "participants",
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(
                                      150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),


                  ],
                ),
              ),
            ],

          ),
        ),
      ),
    );
  }
}
