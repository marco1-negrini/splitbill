
import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:project/SizeConfig.dart';
import 'package:project/values/values.dart';
import 'package:project/data.dart';
import 'package:project/utilities.dart';


class AddEvent3Widget extends StatefulWidget {

  @override
  _AddEvent3WidgetState createState() => _AddEvent3WidgetState();
}


class _AddEvent3WidgetState extends State<AddEvent3Widget> {
  // inizializzo i valori necessari
  // primo turno
  String orario1 = '07:00';
  // secondo turno
  String orario2 = '07:45';
  // terzo turno
  String orario3 = '12:30';
  // quarto turno
  String orario4 = '13:15';
  // quinto turno
  String orario5 = '18:30';
  // sesto turno
  String orario6 = '19:15';

  display_week_days() {
    Widget child;
    List <Widget> children = new List<Widget>();
    for (int i = 0; i < wod_week_days.length; i++) {
      child = Container(
        alignment: Alignment.center,
        width: SizeConfig.screenWidth * 0.45,
        child: Text(
          wod_week_days[i],
          //alignment: TextAlign.left,
          style: TextStyle(
            color: Color.fromARGB(255, 156, 178, 184),
            fontFamily: "Lato",
            fontWeight: FontWeight.w400,
            fontSize: 22,
          ),
        ),
      );
      children.add(child);
    }
    return children;
  }
  set_state(checkbox, value){
    //print(checkbox);
    //print(value);
    is_time_selected[checkbox] = value;

  }

    display_time(is_selected, time) {
      Widget child1, child2;
      List <Widget> children = new List<Widget>();
      for (int i = 0; i < wod_week_days.length; i++) {
        child1 = Container(
          alignment: Alignment.topLeft,
          width: SizeConfig.screenWidth * 0.10,
          child: Checkbox(
            value: is_time_selected[is_selected[i]],
            onChanged: (bool value) {
              setState(() {
                //print(is_time_selected[is_selected[i]]);
                //print('changing...');
                //print("value: " + value.toString());
                //is_time_selected[is_selected[i]] = value;
                set_state(is_selected[i], value);
                //print(is_time_selected[is_selected[i]]);
              });
              //printnotflaggeddays();
            },
            //
          ),
        );
        child2 =Container(
          alignment: Alignment.topLeft,
          width: SizeConfig.screenWidth * 0.20,
            child:Text(
              time,
          style: TextStyle(
            color: Colors.white,
            fontFamily: "Lato",
            fontWeight: FontWeight.w400,
            fontSize: 26,
          ),
        ),
        );
        children.add(child1);
        children.add(child2);
        if (i < wod_week_days.length - 1) {
          children.add(Spacer());
        }
      }
      return children;
    }

    Widget checkbox(String title, bool boolValue) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(title),
          Checkbox(
            value: boolValue,
            onChanged: (bool value) {},
          )
        ],
      );
    }


    @override
    Widget build(BuildContext context) {
      //print(wod_info);
      //print('week days:');
      //print(wod_week_days);
      return Scaffold(
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: false,
        body: Opacity(
          opacity: 1,
          child: Container(
            height: SizeConfig.screenHeight,
            width: SizeConfig.screenWidth,
            // constraints: BoxConstraints.expand(),
            decoration: BoxDecoration(
              color: Color.fromARGB(217, 0, 0, 0), //217
            ),
            child: ListView(
              padding: const EdgeInsets.all(8),
              children: <Widget>[

                // RIGA UNO: tstao home

                Container( // bordo bianco
                  width: SizeConfig.screenWidth * 0.95,
                  //300
                  height: SizeConfig.screenHeight * 0.1,
                  margin: EdgeInsets.only(top: SizeConfig.screenHeight * 0.03),
                  //40
                  child: Container(
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: ButtonTheme(
                          child: FlatButton(
                            child: Opacity(
                              opacity: 1,
                              child: Image.asset(
                                "assets/images/picture2-2.png",
                                fit: BoxFit.none,
                              ),
                            ),
                            onPressed: () {
                              Navigator.pushNamed(context, '/HomePage');
                              print("Pressing Back Home");
                              // Navigator.pushNamed(context, '/AddEvent1Widget');
                            },
                          ),
                        ),
                      )
                  ),
                ),

                // RIGA DUE: Contiene il giorno della settimana selezionato nel calendario

                Container(
                  margin: EdgeInsets.only(top: SizeConfig.screenHeight * 0.05,),
                  width: SizeConfig.screenWidth,
                  height: SizeConfig.screenHeight * 0.1,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: display_week_days(),
                  ),
                ),


                // TERZA ROW: Contiene il primo orario per entrambi i giorni


                Container(
                  margin: EdgeInsets.only(
                    right: SizeConfig.screenHeight * 0.05,),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: display_time(['d1_t1','d2_t1'],orario1)
                  ),
                ),

                // Quarta riga : Contiene il secondo orario per entrambi i giorni


                Container(
                  margin: EdgeInsets.only(
                    right: SizeConfig.screenHeight * 0.05,),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    //d1_t2 and d2_t2
                    children: display_time(['d1_t2','d2_t2'],orario2),
                  ),
                ),


// quinta riga : Contiene il terzo orario per entrambi i giorni

                Container(
                  margin: EdgeInsets.only(
                    right: SizeConfig.screenHeight * 0.05,),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    //d1_t3 and d2_t3
                    children: display_time(['d1_t3','d2_t3'],orario3),
                  ),
                ),

// sesta  riga : Contiene il quarto orario per entrambi i giorni

                Container(
                  margin: EdgeInsets.only(
                    right: SizeConfig.screenHeight * 0.05,),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: display_time(['d1_t4','d2_t4'],orario4),
                  ),
                ),


// settima  riga : Contiene il quinto orario per entrambi i giorni

                Container(
                  margin: EdgeInsets.only(
                    right: SizeConfig.screenHeight * 0.05,),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: display_time(['d1_t5','d2_t5'],orario5),
                  ),
                ),

                // ottava riga : Contiene il sesto orario per entrambi i giorni

                Container(
                  margin: EdgeInsets.only(
                    right: SizeConfig.screenHeight * 0.05,),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: display_time(['d1_t6','d2_t6'],orario6),
                  ),
                ),


                // Nona ROW: freccie avanti e indietro

                Container( // bordo bianco
                  width: SizeConfig.screenWidth * 0.65,
                  height: SizeConfig.screenHeight * 0.2,
                  margin: EdgeInsets.only(top: SizeConfig.screenHeight * 0.01),
                  //40

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          margin: EdgeInsets.only(left: SizeConfig
                              .screenHeight * 0.1), //40
                          child: ButtonTheme(
                            child: FlatButton(
                              child: Opacity(
                                opacity: 1,
                                child: Image.asset(
                                  "assets/images/icon-arrow-left-4.png",
                                  fit: BoxFit.none,
                                ),
                              ),
                              onPressed: () {
                                Navigator.pushNamed(context, '/Addevent2');
                                print("Pressing Back Button");
                                // Navigator.pushNamed(context, '/AddEvent1Widget');
                              },
                            ),
                          ),
                        ),
                      ),

                      Spacer(),

                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          margin: EdgeInsets.only(right: SizeConfig
                              .screenHeight * 0.1), //40
                          child: ButtonTheme(
                            child: FlatButton(
                              child: Opacity(
                                opacity: 1,
                                child: Image.asset(
                                  "assets/images/icon-arrow-right-4.png",
                                  fit: BoxFit.none,
                                ),
                              ),
                              onPressed: () {
                                Navigator.pushNamed(context, '/Addevent4');
                                print("Pressing Next Button");
                                // Navigator.pushNamed(context, '/AddEvent1Widget');
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),


              ],
            ),

          ),
        ),
      );
    }
  }


