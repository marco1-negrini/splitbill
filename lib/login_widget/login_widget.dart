import 'package:flutter/material.dart';

import '../SizeConfig.dart';
import '../utilities.dart';
import '../data.dart';
class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  TextEditingController userController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    if(!already_loaded){
    print("Loading data");
    load_Data();}
    SizeConfig().init(context);
    @override
    void dispose() {
      // Clean up the controller when the widget is disposed.
      userController.dispose();
      passwordController.dispose();
      super.dispose();
    }
    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      body:
      Container(
        height: SizeConfig.screenHeight*0.0005,
        width: SizeConfig.screenWidth*0.0005,
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(217, 0, 0, 0),
        ),

        child: Column(
          children: [
            Container(
              width: SizeConfig.screenWidth*0.8,
              height: SizeConfig.screenHeight*0.25,
              margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.07),
              child: Image.asset(
                "assets/images/group-76.png",
                fit: BoxFit.contain,
              ),
            ),
            Container(
              width: SizeConfig.screenWidth*0.7,
              height: SizeConfig.screenHeight*0.4,
              margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.07),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(0, 0, 0, 0),
                    offset: Offset(0, 2),
                    blurRadius: 4,
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    height: SizeConfig.screenHeight*0.08,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 0,
                          top: 0,
                          right: 0,
                          child: Container(
                            height: SizeConfig.screenHeight*0.07,
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 252, 252, 253),
                              border: Border.all(
                                color: Color.fromARGB(255, 236, 240, 243),
                                width: 0.587,
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(3.065)),
                            ),
                            child: TextField(
                              controller: userController,
                              decoration: InputDecoration(
                                filled: true,
                                hintText: 'Username',
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: SizeConfig.screenHeight*0.1,
                    margin: EdgeInsets.only(top: 0, right: 1),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 0,
                          top: 0,
                          right: 0,
                          child: Container(
                            height: SizeConfig.screenHeight*0.07,
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 245, 248, 249),
                              borderRadius: BorderRadius.all(Radius.circular(3.065)),
                            ),
                            child: TextField(
                              controller: passwordController,
                              decoration: InputDecoration(
                                filled: true,
                                hintText: 'Password',
                                border: InputBorder.none,
                              ),
                              obscureText: true,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Spacer(),
                  Container(
                    height: SizeConfig.screenHeight*0.07,
                    decoration: BoxDecoration(
                      color: Color.fromARGB(255, 187, 206, 138),
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                            ),
                    child:
                    ButtonTheme(
                      minWidth: 200.0,
                      height: 100.0,
                      child: FlatButton(
                        onPressed: () {
                          authenticate_user(context, userController.text, passwordController.text);
                          // Navigator.pushNamed(context, '/HomePage');
                        },
                        child: Text(
                            "LOGIN",
                            style: TextStyle(
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 24,
                          ),
                        ),
                      ),
                    )
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}