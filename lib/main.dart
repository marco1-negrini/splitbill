import 'package:flutter/material.dart';
import 'package:project/add_event2_widget/add_event2_widget.dart';
import 'package:project/add_event3_widget/add_event3_widget.dart';
import 'package:project/add_event4_widget/add_event4_widget.dart';
import 'package:project/calendar1_widget/calendar1_widget.dart';
import 'package:project/calendar2_widget/calendar2_widget.dart';
import 'package:project/calendar3_widget/calendar3_widget.dart';
import 'package:project/calendar4_copy2_widget/calendar4_copy2_widget.dart';
import 'package:project/calendar4_copy3_widget/calendar4_copy3_widget.dart';
import 'package:project/calendar4_copy4_widget/calendar4_copy4_widget.dart';
import 'package:project/calendar4_copy5_widget/calendar4_copy5_widget.dart';
import 'package:project/calendar4_copy_widget/calendar4_copy_widget.dart';
import 'package:project/calendar4_widget/calendar4_widget.dart';
import 'package:project/calendar5_widget/calendar5_widget.dart';
import 'package:project/gallery_widget/gallery_widget.dart';
import 'package:project/next_widget/next_widget.dart';
import 'package:project/partecipants2_widget/partecipants2_widget.dart';
import 'package:project/partecipants3_widget/partecipants3_widget.dart';
import 'package:project/partecipants4_widget/partecipants4_widget.dart';
import 'package:project/partecipants_widget/partecipants_widget.dart';
import 'package:project/previous_widget/previous_widget.dart';
import 'package:project/reports_widget/reports2.dart';

import 'add_event1_widget/add_event1_widget.dart';
import 'homepage_widget/homepage_widget.dart';
import 'login_widget/login_widget.dart';
import 'data.dart';

void main() => runApp(App());

class App extends StatelessWidget {

  @override
  Widget build(BuildContext context) {



    return MaterialApp(
      home: LoginPage(),
      routes: {

        '/Addevent1': (BuildContext context) => AddEvent1Widget(),
        '/Addevent2': (BuildContext context) => AddEvent2Widget(),
        '/Addevent3': (BuildContext context) => AddEvent3Widget(),
        '/Addevent4': (BuildContext context) => AddEvent4Widget(),

        '/Calendar1': (BuildContext context) => Calendar1Widget(),

        '/Calendar2': (BuildContext context) => Calendar2Widget(),

        '/Calendar3': (BuildContext context) => Calendar3Widget(),

        '/Calendar4': (BuildContext context) => Calendar4Widget(),
        '/Calendar4_ver0': (BuildContext context) => Calendar4CopyWidget(),
        '/Calendar4_ver1': (BuildContext context) => Calendar4Copy2Widget(),
        '/Calendar4_ver2': (BuildContext context) => Calendar4Copy3Widget(),
        '/Calendar4_ver3': (BuildContext context) => Calendar4Copy4Widget(),
        '/Calendar4_ver4': (BuildContext context) => Calendar4Copy5Widget(),

        '/Calendar5': (BuildContext context) => Calendar5Widget(),

        '/Gallery': (BuildContext context) => GalleryWidget(),

        '/HomePage': (BuildContext context) => HomepageWidget(),

        '/LoginPage': (BuildContext context) => LoginPage(),

        '/Next': (BuildContext context) => NextWidget(),

        '/Partecipants': (BuildContext context) => PartecipantsWidget(),

        '/Partecipants2': (BuildContext context) => Partecipants2Widget(),

        '/Partecipants3': (BuildContext context) => Partecipants3Widget(),

        '/Partecipants4': (BuildContext context) => Partecipants4Widget(),

        '/Previous': (BuildContext context) => PreviousWidget(),

        '/Reports2': (BuildContext context) => Reports2(),

      },
    );
  }
}
// class SecondRoute extends StatelessWidget {}
