import 'package:flutter/material.dart';
import 'package:project/SizeConfig.dart';
import 'package:project/data.dart';
import 'package:project/values/values.dart';
import 'package:project/utilities.dart';
import 'dart:collection';
import 'package:project/frequency_series.dart' show FrequencyData;
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:project/add_event2_widget/flutter_calendar.dart' as prefix0;
import 'package:csv/csv.dart';
import 'package:syncfusion_flutter_charts/charts.dart';




// import 'calendar1_widget.dart';


class Partecipants2Widget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {




    final String args = ModalRoute.of(context).settings.arguments;
    // print("the arg is: " + args);
    print("Loading partecipant: " + args);

    HashMap partecipantProfile = get_participantsProfiles(int.parse(args));

    //  int.parse(get_participantsFrequency(int.parse(args)));


    List <HashMap> participantResults = get_participantsReuslts(args);



    return Scaffold(
      body: Opacity(
        opacity: 1,
        child: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            color: Color.fromARGB(217, 0, 0, 0),
          ),

          child: ListView(
            padding: EdgeInsets.only(
              left: SizeConfig.screenWidth*0.05,
              right: SizeConfig.screenWidth*0.05,
            ),


            children: <Widget>[

              //NOME E COGNOME

              Container (
                margin: EdgeInsets.only(
                    top: SizeConfig.screenHeight*0.06,
                    bottom: SizeConfig.screenHeight*0.01
                ),
                child: Opacity(
                  opacity: 0.95622,
                  child: Text(
                    partecipantProfile['name'] + ' ' +partecipantProfile['lastname'],
                    // alignment: TextAlign.left,
                    style: TextStyle(
                      color: AppColors.secondaryText,
                      fontFamily: "Lato",
                      fontWeight: FontWeight.w700,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),

              Container(
                height: SizeConfig.screenWidth*0.005,
                width: SizeConfig.screenWidth,
                decoration: BoxDecoration(
                  color: AppColors.primaryElement,
                  border: Border.fromBorderSide(Borders.primaryBorder),
                ),

              ),


              //IMMAGINE


              Container (
                height: SizeConfig.screenHeight*0.14,
                margin: EdgeInsets.only(
                    top: SizeConfig.screenHeight*0.02,
                    bottom: SizeConfig.screenHeight*0.01,
                    left: SizeConfig.screenWidth*0.04,
                    right: SizeConfig.screenWidth*0.04
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        child: Opacity(
                          opacity: 1,
                          child: Image.asset(
                            partecipantProfile["Image"],
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Opacity(

                        opacity: 0.95622,
                        child: Text(


                          //DATI PERSONALI


                          "A g e :  "+ partecipantProfile["age"] +"\nH e i g h t :  "+ partecipantProfile['height']
                              +" cm\nW e i g h t :  "+  partecipantProfile["weight"]+" kg\nC i r c u m f. :  "+ partecipantProfile["circumference"]+ "\n\n",
                          //alignment: TextAlign.left,
                          style: TextStyle(
                            color: AppColors.secondaryText,
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),


              Container(
                margin: EdgeInsets.only(
                    bottom: SizeConfig.screenHeight*0.02
                ),
                child: Opacity(
                  opacity: 1,
                  child: Text(

                    //DESCRIZIONE
                    "\"" + partecipantProfile['descriptive_sentence'] + "\"",
                    //alignment: TextAlign.center,
                    style: TextStyle(
                      color: AppColors.secondaryText,
                      fontFamily: "Lato",
                      fontWeight: FontWeight.w300,
                      //style: FontStyle.italic,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),




              //CHART TEXT

              Align(
                alignment: Alignment.topCenter,
                child: Container (
                  margin: EdgeInsets.only(
                      top: SizeConfig.screenHeight*0.02,
                      bottom: SizeConfig.screenHeight*0.02
                  ),
                  child: Opacity(
                    opacity: 0.95622,
                    child: Text(
                      "Frequency Chart",
                      style: TextStyle(
                        color: AppColors.secondaryText,
                        fontFamily: "Lato",
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              ),

             //CHART

             Container( // bordo bianco
               height: SizeConfig.screenHeight*0.3,
               width: SizeConfig.screenWidth*0.5,//70
               margin: EdgeInsets.only(
                   bottom: SizeConfig.screenHeight*0.03
               ),
               decoration: BoxDecoration(
                 color: AppColors.secondaryText,

                 borderRadius: BorderRadius.all(Radius.circular(7)), //2
               ),
               child: SfCartesianChart(
                 primaryXAxis: CategoryAxis(),
                   //initialize line series
                   series: <ColumnSeries<FrequencyData, String>>[
                     ColumnSeries <FrequencyData,String>(
                         dataSource: [
                          FrequencyData(partecipantProfile["day1"], int.parse(partecipantProfile["freq1"])),
                          FrequencyData(partecipantProfile["day2"], int.parse(partecipantProfile["freq2"])),
                         ],
                      xValueMapper: (FrequencyData sales, _) => sales.dayHour,
                      yValueMapper: (FrequencyData sales, _) => sales.frequency,
                       color: Color.fromARGB(255, 187, 206, 138),

                     ),
                   ],
                  ),
                  ),


//participantResults[index]["score"],



              //TRAININGS RECAP AND CALENDAR

              Align(
                alignment: Alignment.topCenter,
                child: Container (
                  margin: EdgeInsets.only(
                    right: SizeConfig.screenWidth*0.07,
                      top: SizeConfig.screenHeight*0.04,

                  ),
                  child: Opacity(
                    opacity: 0.95622,
                    child: Text(
                      "Trainings recap         |              Calendar",
                      style: TextStyle(
                        color: AppColors.secondaryText,
                        fontFamily: "Lato",
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              ),




              //TRIS DI COLORI CON DESCRIZIONE

              Container(
                alignment: Alignment.center,
                width: SizeConfig.screenWidth*0.4,
                height: SizeConfig.screenHeight*0.04,
                margin: EdgeInsets.only(
                  top:SizeConfig.screenHeight*0.025,
                    bottom: SizeConfig.screenHeight*0.01,
                  //left: SizeConfig.screenWidth*0.09,
                  //right: SizeConfig.screenWidth*0.09
                ),
                child: Opacity(
                  opacity: 1,
                  child: Image.asset(
                    "assets/images/group-2.png",
                    fit: BoxFit.contain
                  ),
                ),
              ),

              Container(
                height: SizeConfig.screenHeight*0.05,
                width: SizeConfig.screenWidth*0.9,
                child: Row(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container (
                        margin: EdgeInsets.only(
                          bottom: SizeConfig.screenHeight*0.03,


                        ),
                        child: Opacity(
                          opacity: 0.95622,
                          child: Text(
                            "Attended",
                            style: TextStyle(
                              color: AppColors.secondaryText,
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w500,
                              fontSize: 11,
                            ),
                          ),
                        ),
                      ),
                    ),

                    Align(
                      alignment: Alignment.topCenter,
                      child: Container (
                        margin: EdgeInsets.only(
                          left: SizeConfig.screenWidth*0.18,
                          bottom: SizeConfig.screenHeight*0.03,

                        ),
                        child: Opacity(
                          opacity: 0.95622,
                          child: Text(
                            "Registered and skipped",
                            style: TextStyle(
                              color: AppColors.secondaryText,
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w500,
                              fontSize: 11,
                            ),
                          ),
                        ),
                      ),
                    ),

                    Align(
                      alignment: Alignment.topRight,
                      child: Container (
                        margin: EdgeInsets.only(
                          left: SizeConfig.screenWidth*0.1,
                          bottom: SizeConfig.screenHeight*0.03,

                        ),
                        child: Opacity(
                          opacity: 0.95622,
                          child: Text(
                            "Next registration",
                            style: TextStyle(
                              color: AppColors.secondaryText,
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w500,
                              fontSize: 11,
                            ),
                          ),
                        ),
                      ),
                    ),


                  ],

                ),
              ),


              // SECONDO BOX BIANCO

              Container( // bordo bianco
                height: SizeConfig.screenHeight*0.7, //70
                margin: EdgeInsets.only(
                    bottom: SizeConfig.screenHeight*0.03,
                ),
                decoration: BoxDecoration(
                  color: AppColors.secondaryText,
                  borderRadius: BorderRadius.all(Radius.circular(7)), //2
                ),



                //HORIZONTAL LISTVIEW

                child: PageView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[



                    //TABELLA


                    Container(
                      child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [

                      Container (
                        width: SizeConfig.screenWidth*0.2,
                        height: SizeConfig.screenHeight*0.05,
                      ),

                        Row(
                          children: <Widget>[
                            Container (
                              width: SizeConfig.screenWidth*0.2,
                              height: SizeConfig.screenHeight*0.05,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  width: 4, // 0.5
                                  color: Color.fromARGB(255, 187, 206, 138),
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(7)), //2
                              ),
                              child: Container(
                                margin:EdgeInsets.only(
                                  left: SizeConfig.screenWidth*0.06,
                                  top: SizeConfig.screenHeight*0.01,
                                ),
                                child: Text(
                                  "DAY",
                                  style: TextStyle(
                                    color: Color.fromARGB(217, 0, 0, 0),
                                    fontFamily: "Lato",
                                    fontWeight: FontWeight.w800,
                                    fontSize: 13,
                                  ),
                                ),
                              ),
                            ),

                            Container (
                              width: SizeConfig.screenWidth*0.125,
                              height: SizeConfig.screenHeight*0.05,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  width: 4, // 0.5
                                  color: Color.fromARGB(255, 187, 206, 138),
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(7)), //2
                              ),
                              child: Container(
                                margin:EdgeInsets.only(
                                  //right: SizeConfig.screenWidth*0.07,
                                  left: SizeConfig.screenWidth*0.007,
                                  top: SizeConfig.screenHeight*0.011,
                                ),
                                child: Text(
                                  "HOUR",
                                  //alignment: TextAlign.left,
                                  style: TextStyle(
                                    color: Color.fromARGB(217, 0, 0, 0),
                                    fontFamily: "Lato",
                                    fontWeight: FontWeight.w700,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),


                            Container (
                              width: SizeConfig.screenWidth*0.125,
                              height: SizeConfig.screenHeight*0.05,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  width: 4, // 0.5
                                  color: Color.fromARGB(255, 187, 206, 138),
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(7)), //2
                              ),
                              child: Container(
                                margin:EdgeInsets.only(
                                  //right: SizeConfig.screenWidth*0.07,
                                  left: SizeConfig.screenWidth*0.002,
                                  top: SizeConfig.screenHeight*0.011,
                                ),
                                child: Text(
                                  "SCORE",
                                  //alignment: TextAlign.left,
                                  style: TextStyle(
                                    color: Color.fromARGB(217, 0, 0, 0),
                                    fontFamily: "Lato",
                                    fontWeight: FontWeight.w700,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),


                            Container (
                              width: SizeConfig.screenWidth*0.45,
                              height: SizeConfig.screenHeight*0.05,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  width: 4, // 0.5
                                  color: Color.fromARGB(255, 187, 206, 138),
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(7)), //2
                              ),
                              child: Container(
                                margin:EdgeInsets.only(
                                  //right: SizeConfig.screenWidth*0.07,
                                  left: SizeConfig.screenWidth*0.065,
                                  top: SizeConfig.screenHeight*0.011,
                                ),
                                child: Text(
                                  "PERSONAL NOTES",
                                  //alignment: TextAlign.left,
                                  style: TextStyle(
                                    color: Color.fromARGB(217, 0, 0, 0),
                                    fontFamily: "Lato",
                                    fontWeight: FontWeight.w700,
                                    fontSize: 13,
                                  ),
                                ),
                              ),
                            ),





                          ],
                        ),



                        Expanded(
                          child: ListView.separated(
                            itemCount: (participantResults.length),
                            //itemCount: 9,
                            itemBuilder: (context, int index) {
                              print("index:");
                              print(index);

                              return Container(


                                //height: 50,
                                child:
                                Row(
                                  children: <Widget> [

                                    Container (
                                      width: SizeConfig.screenWidth*0.2,
                                      height: SizeConfig.screenHeight*0.05,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(7)),
                                        border: Border.all(
                                          width: 0.5, // 0.5
                                          color: Color.fromARGB(217, 0, 0, 0),
                                        ), //2
                                      ),
                                      child: Container(
                                        margin:EdgeInsets.only(
                                          top: SizeConfig.screenHeight*0.02,
                                        ),
                                        child: Text(
                                          participantResults[index]["date"],
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Color.fromARGB(217, 0, 0, 0),
                                            fontFamily: "Lato",
                                            fontWeight: FontWeight.w400,
                                            fontSize: 10,
                                          ),
                                        ),
                                      ),
                                    ),

                                    Container (
                                      width: SizeConfig.screenWidth*0.125,
                                      height: SizeConfig.screenHeight*0.05,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(7)),
                                        border: Border.all(
                                          width: 0.5, // 0.5
                                          color: Color.fromARGB(217, 0, 0, 0),
                                        ), //2
                                      ),
                                      child: Container(
                                        margin:EdgeInsets.only(
                                          top: SizeConfig.screenHeight*0.02,
                                        ),
                                        child: Text(
                                            participantResults[index]["time"],
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Color.fromARGB(217, 0, 0, 0),
                                            fontFamily: "Lato",
                                            fontWeight: FontWeight.w400,
                                            fontSize: 10,
                                          ),
                                        ),
                                      ),
                                    ),


                                    Container (
                                      width: SizeConfig.screenWidth*0.125,
                                      height: SizeConfig.screenHeight*0.05,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(7)),
                                        border: Border.all(
                                          width: 0.5, // 0.5
                                          color: Color.fromARGB(217, 0, 0, 0),
                                        ), //2
                                      ),
                                      child: Container(
                                        margin:EdgeInsets.only(
                                          top: SizeConfig.screenHeight*0.02,
                                        ),
                                        child: Text(
                                            participantResults[index]["score"],
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Color.fromARGB(217, 0, 0, 0),
                                            fontFamily: "Lato",
                                            fontWeight: FontWeight.w400,
                                            fontSize: 10,
                                          ),
                                        ),
                                      ),
                                    ),


                                    Container (
                                      width: SizeConfig.screenWidth*0.45,
                                      height: SizeConfig.screenHeight*0.05,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(7)),
                                        border: Border.all(
                                          width: 0.5, // 0.5
                                          color: Color.fromARGB(217, 0, 0, 0),
                                        ), //2
                                      ),
                                      child: Container(
                                        margin:EdgeInsets.only(
                                          //right: SizeConfig.screenWidth*0.07,
                                          left: SizeConfig.screenWidth*0.008,
                                          right: SizeConfig.screenWidth*0.008,
                                          //top: SizeConfig.screenHeight*0.001,
                                          //bottom: SizeConfig.screenHeight*0.01,
                                        ),
                                        child: Text(
                                          participantResults[index]["notes"],
                                          //maxLength: 60,
                                          //maxLength: 3,
                                          //alignment: TextAlign.left,
                                          style: TextStyle(
                                            color: Color.fromARGB(217, 0, 0, 0),
                                            fontFamily: "Lato",
                                            fontWeight: FontWeight.w400,
                                            fontSize: 10,
                                          ),
                                        ),
                                      ),
                                    ),

                                  ],

                                ),
                              );
                            },
                            separatorBuilder: (context, int index) => const Divider(),

                          ),
                        ),


                        Container (
                          margin: EdgeInsets.only(
                            left: SizeConfig.screenWidth*0.24,
                            top: SizeConfig.screenHeight*0.05,
                            bottom: SizeConfig.screenHeight*0.05,

                          ),
                          child: Opacity(
                            opacity: 0.95622,
                            child: Text(
                              "Swipe to Calendar  >>",
                              style: TextStyle(
                                color: Color.fromARGB(217, 0, 0, 0),
                                fontFamily: "Lato",
                                fontWeight: FontWeight.w700,
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ),
                        ],
                      ),
                    ),


                    //CALENDARIO

                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          new prefix0.Calendar(
                            onSelectedRangeChange: (range) =>
                                print("Range is ${range.item1}, ${range.item2}"),
                            isExpandable: true,
                          ),



                        ],
                      ),
                    ),



                  ],
                ),
              ),








              Container (
                margin: EdgeInsets.only(
                    top: SizeConfig.screenHeight*0.06,
                    bottom: SizeConfig.screenHeight*0.01
                ),
                child: Opacity(
                  opacity: 0.95622,
                  child: Text(
                    "A v a i l a b i l i t i e s",
                    // alignment: TextAlign.left,
                    style: TextStyle(
                      color: AppColors.secondaryText,
                      fontFamily: "Lato",
                      fontWeight: FontWeight.w700,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),

              Container(
                height: SizeConfig.screenWidth*0.005,
                width: SizeConfig.screenWidth,

                decoration: BoxDecoration(
                  color: AppColors.primaryElement,
                  border: Border.fromBorderSide(Borders.primaryBorder),
                ),

              ),


              Container (
                height: SizeConfig.screenHeight*0.15,
                margin: EdgeInsets.only(
                    top: SizeConfig.screenHeight*0.02,
                    bottom: SizeConfig.screenHeight*0.02
                ),
                child: Opacity(
                  opacity: 0.95622,
                  child: Text(
                    "Mon:		7:45	-	19:15\nTue:		7:45	-	19:15\nThu:		18:30  -	19:15\nFri:			18:30  -	19:15",
                    //alignment: TextAlign.left,
                    style: TextStyle(
                      color: AppColors.secondaryText,
                      fontFamily: "Lato",
                      fontWeight: FontWeight.w300,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),


              Container(
                width: SizeConfig.screenWidth*0.1,
                height: SizeConfig.screenHeight*0.2,
                /*decoration: BoxDecoration(
                  color: AppColors.primaryBackground,
                ),*/
                child: Column(
                  children: [
                    // container calendar image
                    Container(
                      height: SizeConfig.screenHeight*0.1,
                      margin: EdgeInsets.only(
                        bottom: SizeConfig.screenHeight*0.02,
                        top: SizeConfig.screenHeight*0.02
                      ),
                      child: ButtonTheme(
                        child: FlatButton(
                          child: Opacity(
                            opacity: 1,
                            child: Image.asset(
                              "assets/images/icon-arrow-left-4.png",
                              fit: BoxFit.none,
                            ),
                          ),
                          onPressed: () {
                            Navigator.pushNamed(context, '/Partecipants');
                            print("Pressing Back Button");
                            // Navigator.pushNamed(context, '/AddEvent1Widget');
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),


            ],
          ),
        ),



      ),
    );
  }
}