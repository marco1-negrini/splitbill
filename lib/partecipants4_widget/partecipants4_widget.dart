
import 'package:flutter/material.dart';
import 'package:project/values/values.dart';


class Partecipants4Widget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      body: Opacity(
        opacity: 1,
        child: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            color: Color.fromARGB(217, 0, 0, 0),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                height: 67,
                margin: EdgeInsets.only(left: 53, top: 50, right: 25),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(top: 45),
                        child: Opacity(
                          opacity: 0.95622,
                          child: Text(
                            "Chiara Di Vece (V1)",
                            // alignment: TextAlign.left,
                            style: TextStyle(
                              color: AppColors.secondaryText,
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w700,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: 50,
                        height: 49,
                        child: Opacity(
                          opacity: 1,
                          child: Image.asset(
                            "assets/images/picture2.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Opacity(
                  opacity: 1,
                  child: Container(
                    width: 200,
                    height: 1,
                    margin: EdgeInsets.only(left: 53),
                    decoration: BoxDecoration(
                      color: AppColors.primaryElement,
                      border: Border.fromBorderSide(Borders.primaryBorder),
                    ),
                    child: Container(),
                  ),
                ),
              ),
              Container(
                height: 132,
                margin: EdgeInsets.only(left: 61, top: 52, right: 62),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: 93,
                        height: 90,
                        child: Opacity(
                          opacity: 1,
                          child: Image.asset(
                            "assets/images/immagine1.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Opacity(
                        opacity: 0.95622,
                        child: Text(
                          "Age: 24\nHeight: 170 cm\nWeight: 65 kg\nCircumferences: -\n\n",
                          // alignment: TextAlign.left,
                          style: TextStyle(
                            color: AppColors.secondaryText,
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 301,
                  margin: EdgeInsets.only(top: 10),
                  child: Opacity(
                    opacity: 1,
                    child: Text(
                      "I would like to go beyond my limits and improve my physical condition, adding a training routine to my university career.",
                     //  alignment: TextAlign.center,
                      style: TextStyle(
                        color: AppColors.secondaryText,
                        fontFamily: "Lato",
                        fontWeight: FontWeight.w300,
                        // style: FontStyle.italic,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              ),
              Spacer(),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 215,
                  height: 200,
                  margin: EdgeInsets.only(bottom: 26),
                  child: Opacity(
                    opacity: 1,
                    child: Image.asset(
                      "assets/images/slot-frequency-chiara.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Container(
                height: 204,
                margin: EdgeInsets.only(left: 19, right: 18, bottom: 52),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      bottom: 138,
                      child: Opacity(
                        opacity: 0.95622,
                        child: Text(
                          "Monday, 4 November - 7:00 a.m.\n",
                          // alignment: TextAlign.left,
                          style: TextStyle(
                            color: AppColors.secondaryText,
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w300,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 0,
                      right: 0,
                      bottom: 0,
                      child: Opacity(
                        opacity: 1,
                        child: Image.asset(
                          "assets/images/single-session-png-2.png",
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 25,
                margin: EdgeInsets.only(left: 138, right: 170),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                        width: 17,
                        height: 25,
                        child: Opacity(
                          opacity: 1,
                          child: Image.asset(
                            "assets/images/icon-arrow-left-4-2.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                        width: 17,
                        height: 25,
                        child: Opacity(
                          opacity: 1,
                          child: Image.asset(
                            "assets/images/icon-arrow-left-4-2.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}