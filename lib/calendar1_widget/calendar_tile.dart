import 'package:flutter/material.dart';
import 'package:date_utils/date_utils.dart';



class CalendarTile extends StatelessWidget {
  final VoidCallback onDateSelected;
  final DateTime date;
  final String dayOfWeek;
  final bool isDayOfWeek;
  final bool isSelected;
  final TextStyle dayOfWeekStyles;
  final TextStyle dateStyles;
  final Widget child;
  final List<DateTime> listOfTrainings;

  CalendarTile({
    this.onDateSelected,
    this.date,
    this.child,
    this.dateStyles,
    this.dayOfWeek,
    this.dayOfWeekStyles,
    this.isDayOfWeek: false,
    this.isSelected: false,
    this.listOfTrainings,
  });


  // mette i giorni della settimana nella prima riga e i numeri del mese nelle altre
  Widget renderDateOrDayOfWeek(BuildContext context) {

    if (isDayOfWeek) {
      return new InkWell(
        child: new Container(
          alignment: Alignment.center,
          child: new Text(
            dayOfWeek,
            style: dayOfWeekStyles, // è quello che srive i giorni della settimana
          ),
        ),
      );
    } else {
      return new InkWell(
        onTap: onDateSelected,
        child: new Container(
          decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  color: getColor(context, isSelected,date),
                ),

          alignment: Alignment.center,
          child: new Text(
            Utils.formatDay(date).toString(),
            style: isSelected ? Theme.of(context).primaryTextTheme.body1 : dateStyles,
            textAlign: TextAlign.center, //mette i numeri dei giorni tutto sotto
          ),
        ),
      );
    }
  }

  Color getColor(context, bool isSelected, DateTime selector) {
    if(listOfTrainings.contains(selector.toUtc())){
      return Color.fromARGB(255, 187, 206, 138);

    }
    if (isSelected) {
      return Theme.of(context).primaryColor;

    }

    else {
      return Colors.transparent;

    }
  }

  @override
  Widget build(BuildContext context) {
    if (child != null) {
      return new InkWell(
        child: child,
        onTap: onDateSelected,
      );
    }
    return new Container(
      child: renderDateOrDayOfWeek(context),
    );
  }
}
