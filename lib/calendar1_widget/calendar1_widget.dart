
import 'package:flutter/material.dart';
import 'package:project/calendar1_widget/flutter_calendar.dart';
// import 'package:flutter_calendar/flutter_calendar.dart';
import 'package:project/values/values.dart';

import '../SizeConfig.dart';
// import 'flutter_calendar.dart';


class Calendar1Widget extends StatelessWidget {
  void handleNewDate(date) {
    //print("handleNewDate ${date}");
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: new ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.purple,
      ),
      home: new Scaffold(
        body: new Container(
          width: SizeConfig.screenWidth, //300
          height: SizeConfig.screenHeight,
          margin: new EdgeInsets.symmetric(
            horizontal: 5.0,
            vertical: 10.0,
          ),
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.03),
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: ButtonTheme(
                      child: FlatButton(
                        child: Opacity(
                          opacity: 1,
                          child: Image.asset(
                            "assets/images/picture2-2.png",
                            fit: BoxFit.none,
                          ),
                        ),
                        onPressed: () {
                          Navigator.pushNamed(context, '/HomePage');
                          print("Pressing Back Home");
                          // Navigator.pushNamed(context, '/AddEvent1Widget');
                        },
                      ),
                    ),
                  )
              ),
              Container(
                height: SizeConfig.screenHeight*0.05, // il container serve per spaziare sotto la home
              ),
              new Calendar(
                //onSelectedRangeChange: (range) =>
                    //print("Range is ${range.item1}, ${range.item2}"),
                isExpandable: true,

              ),
              /*new Divider(
                height: 50.0,
              ),*/
            ],
          ),
        ),
      ),

    );
  }
}