

import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:project/SizeConfig.dart';
import 'package:project/utilities.dart' as utilities;
import 'package:project/data.dart';
import 'package:project/values/values.dart';


class AddEvent1Widget extends StatefulWidget {

  @override
  _AddEvent1WidgetState createState() => _AddEvent1WidgetState();
}


class _AddEvent1WidgetState extends State<AddEvent1Widget> {
  TextEditingController wodNme_controller = new TextEditingController();
  TextEditingController wodType_controller = new TextEditingController();
  TextEditingController exe1_controller = new TextEditingController();
  TextEditingController exe2_controller = new TextEditingController();
  TextEditingController exe3_controller = new TextEditingController();
  TextEditingController exe4_controller = new TextEditingController();
  TextEditingController scoreDescription_controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    wodNme_controller.text = wod_name;
    wodType_controller.text = wod_type;
    exe1_controller.text = ex1;
    exe2_controller.text = ex2;
    exe3_controller.text = ex3;
    exe4_controller.text = ex4;
    scoreDescription_controller.text = scoreDescription;
  }



// serve per inserire l'immagine dalla galleria



  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery); // .camera per accedere alla camera
    // .gallery per accedere alla galleria
    setState(() {
      wod_image = image;
    });
  }


  @override
  Widget build(BuildContext context) {
    //HashMap<String, String> wod_info = new HashMap();
    //utilities.display_addEvent_data();

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      body: Opacity(
        opacity: 1,
        child: Container(
          height: SizeConfig.screenHeight,
          width: SizeConfig.screenWidth,
          // constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            color: Color.fromARGB(217, 0, 0, 0), //217
          ),
          child: ListView(
            // alignment: Alignment.center,
            //children: [
            padding: const EdgeInsets.all(8),
            children: <Widget>[

              // ZERO ROW: contiene la casella home

              Container( // bordo bianco
                width: SizeConfig.screenWidth*0.95, //300
                height: SizeConfig.screenHeight*0.1,
                margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.03), //40
                child:Container(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: ButtonTheme(
                        child: FlatButton(
                          child: Opacity(
                            opacity: 1,
                            child: Image.asset(
                              "assets/images/picture2-2.png",
                              fit: BoxFit.none,
                            ),
                          ),
                          onPressed: () {
                            Navigator.pushNamed(context, '/HomePage');
                            print("Pressing Back Home");
                            // Navigator.pushNamed(context, '/AddEvent1Widget');
                          },
                        ),
                      ),
                    )
                ),
              ),
              Container( // bordo bianco
                width: SizeConfig.screenWidth*0.95,
                height: SizeConfig.screenHeight*0.085,
                margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.02), //70
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5, // 0.5
                    color: Color.fromARGB(255, 187, 206, 138),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(7)), //2
                ),

                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      child: Container(
                        margin:EdgeInsets.only(
                          //right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth*0.04,
                          //top: SizeConfig.screenHeight*0.075,
                        ),
                        child: Text(
                          "WOD name: ",
                          //alignment: TextAlign.left,
                          style: TextStyle(
                            color: Color.fromARGB(255, 156, 178, 184),
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 22,
                          ),
                        ),
                      ),
                    ),
                    // Spacer(),
                    Align(
                      child: Container(
                        width: SizeConfig.screenWidth*0.5,
                        margin:EdgeInsets.only(
                          //right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth*0.01,
                          // top: SizeConfig.screenHeight*0.075,
                        ),
                        child: TextField(
                          controller: wodNme_controller,
                          style: TextStyle(
                            color: AppColors.secondaryText,
                            decorationColor: Color.fromARGB(150, 156, 178, 184),
                            decorationThickness: 0.0,
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 20,
                          ),
                          decoration: InputDecoration(
                            filled: false,
                            hintText: 'Insert WOD name',
                            hintStyle: TextStyle(color: Color.fromARGB(150, 156, 178, 184)),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              // TERZA ROW: WOD TYPE

              Container( // bordo verde
                  width: SizeConfig.screenWidth*0.95, //300
                  height: SizeConfig.screenHeight*0.085,
                  margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.01), //40
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 0.5, // 0.5
                      color: Color.fromARGB(255, 187, 206,138),
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(7)), //2
                  ),

                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Align(
                        child: Container(
                          margin:EdgeInsets.only(
                            //right: SizeConfig.screenWidth*0.07,
                            left: SizeConfig.screenWidth*0.04,
                            //top: SizeConfig.screenHeight*0.075,
                          ),
                          child: Text(
                            "WOD type: ",
                            //alignment: TextAlign.left,
                            style: TextStyle(
                              color: Color.fromARGB(255, 156, 178, 184),
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w400,
                              fontSize: 22,
                            ),
                          ),
                        ),
                      ),
                      // Spacer(),
                      Align(
                        child: Container(
                          width: SizeConfig.screenWidth*0.5,
                          margin:EdgeInsets.only(
                            //right: SizeConfig.screenWidth*0.07,
                            left: SizeConfig.screenWidth*0.01,
                            // top: SizeConfig.screenHeight*0.075,
                          ),
                          child: TextField(
                            onChanged: (text) {
                              //print( watchWord_controller.text);
                              if (utilities.check_parameters_createEvent(context,wodNme_controller.text,
                                  wodType_controller.text,
                                  exe1_controller.text,
                                  exe2_controller.text,
                                  exe3_controller.text,
                                  exe4_controller.text, send_alert:false) && is_description_editable) {
                                scoreDescription = utilities.generate_score_description(
                                    wodNme_controller.text,
                                    wodType_controller.text,
                                    exe1_controller.text,
                                    exe2_controller.text,
                                    exe3_controller.text,
                                    exe4_controller.text,
                                    context);
                                scoreDescription_controller.text =
                                    scoreDescription;
                              }} ,
                            controller: wodType_controller,
                            style: TextStyle(
                              color: AppColors.secondaryText,
                              decorationColor: Color.fromARGB(150, 156, 178, 184),
                              decorationThickness: 0.0,
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w400,
                              fontSize: 20,
                            ),
                            decoration: InputDecoration(
                              filled: false,
                              hintText: 'Insert WOD type',
                              hintStyle: TextStyle(color: Color.fromARGB(150, 156, 178, 184)),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )

              ),

              // QUARTA ROW: contiene i nomi degli esercizi

              Container( // bordo bianco
                  width: SizeConfig.screenWidth*0.95, //300
                  height: SizeConfig.screenHeight*0.4,
                  margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.1), //40
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 0.5, // 0.5
                      color: Color.fromARGB(255, 187, 206, 138),
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(7)), //2
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Align(
                        child: Container(
                          width: SizeConfig.screenWidth*0.75,
                          margin:EdgeInsets.only(
                            //right: SizeConfig.screenWidth*0.07,
                            left: SizeConfig.screenWidth*0.01,
                            //top: SizeConfig.screenHeight*0.075,
                          ),
                          child: TextField(
                            onChanged: (text) {
                              //print( watchWord_controller.text);
                      if (utilities.check_parameters_createEvent(context,wodNme_controller.text,
                        wodType_controller.text,
                        exe1_controller.text,
                        exe2_controller.text,
                        exe3_controller.text,
                        exe4_controller.text, send_alert:false) && is_description_editable) {
                            scoreDescription = utilities.generate_score_description(
                            wodNme_controller.text,
                            wodType_controller.text,
                            exe1_controller.text,
                            exe2_controller.text,
                            exe3_controller.text,
                            exe4_controller.text,
                            context);
                            scoreDescription_controller.text =
                            scoreDescription;
                            }} ,
                            controller: exe1_controller,
                            style: TextStyle(
                              color: AppColors.secondaryText,
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w400,
                              fontSize: 20,
                            ),
                            decoration: InputDecoration(
                              filled: false,
                              hintText: 'Exe 1: Insert exercise name',
                              hintStyle: TextStyle(color: Color.fromARGB(150, 156, 178, 184)),
                            ),
                          ),
                        ),
                      ),
                      // Spacer(),
                      Align(
                        child: Container(
                          width: SizeConfig.screenWidth*0.75,
                          margin:EdgeInsets.only(
                            //right: SizeConfig.screenWidth*0.07,
                            left: SizeConfig.screenWidth*0.01,
                            //top: SizeConfig.screenHeight*0.075,
                          ),
                          child: TextField(
                            onChanged: (text) {
                              //print( watchWord_controller.text);
                              if (utilities.check_parameters_createEvent(context,wodNme_controller.text,
                                  wodType_controller.text,
                                  exe1_controller.text,
                                  exe2_controller.text,
                                  exe3_controller.text,
                                  exe4_controller.text, send_alert:false) && is_description_editable) {
                                scoreDescription = utilities.generate_score_description(
                                    wodNme_controller.text,
                                    wodType_controller.text,
                                    exe1_controller.text,
                                    exe2_controller.text,
                                    exe3_controller.text,
                                    exe4_controller.text,
                                    context);
                                scoreDescription_controller.text =
                                    scoreDescription;
                              }} ,
                            controller: exe2_controller,
                            style: TextStyle(
                              color: AppColors.secondaryText,
                              decorationColor: Color.fromARGB(150, 156, 178, 184),
                              decorationThickness: 0.0,
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w400,
                              fontSize: 20,
                            ),
                            decoration: InputDecoration(
                              filled: false,
                              hintText: 'Exe 2: Insert exercise name',
                              hintStyle: TextStyle(color: Color.fromARGB(150, 156, 178, 184)),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        child: Container(
                          width: SizeConfig.screenWidth*0.75,
                          margin:EdgeInsets.only(
                            //right: SizeConfig.screenWidth*0.07,
                            left: SizeConfig.screenWidth*0.01,
                            //top: SizeConfig.screenHeight*0.075,
                          ),
                          child: TextField(
                            onChanged: (text) {
                              //print( watchWord_controller.text);
                              if (utilities.check_parameters_createEvent(context,wodNme_controller.text,
                                  wodType_controller.text,
                                  exe1_controller.text,
                                  exe2_controller.text,
                                  exe3_controller.text,
                                  exe4_controller.text, send_alert:false) && is_description_editable) {
                                scoreDescription = utilities.generate_score_description(
                                    wodNme_controller.text,
                                    wodType_controller.text,
                                    exe1_controller.text,
                                    exe2_controller.text,
                                    exe3_controller.text,
                                    exe4_controller.text,
                                    context);
                                scoreDescription_controller.text =
                                    scoreDescription;
                              }} ,
                            controller: exe3_controller,
                            style: TextStyle(
                              color: AppColors.secondaryText,
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w400,
                              fontSize: 20,
                            ),
                            decoration: InputDecoration(
                              filled: false,
                              hintText: 'Exe 3: Insert exercise name',
                              hintStyle: TextStyle(color: Color.fromARGB(150, 156, 178, 184)),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        child: Container(
                          width: SizeConfig.screenWidth*0.75,
                          margin:EdgeInsets.only(
                            //right: SizeConfig.screenWidth*0.07,
                            left: SizeConfig.screenWidth*0.01,
                            //top: SizeConfig.screenHeight*0.075,
                          ),
                          child: TextField(
                            onChanged: (text) {
                              //print( watchWord_controller.text);
                              if (utilities.check_parameters_createEvent(context,wodNme_controller.text,
                                  wodType_controller.text,
                                  exe1_controller.text,
                                  exe2_controller.text,
                                  exe3_controller.text,
                                  exe4_controller.text, send_alert:false) && is_description_editable) {
                                scoreDescription = utilities.generate_score_description(
                                    wodNme_controller.text,
                                    wodType_controller.text,
                                    exe1_controller.text,
                                    exe2_controller.text,
                                    exe3_controller.text,
                                    exe4_controller.text,
                                    context);
                                scoreDescription_controller.text =
                                    scoreDescription;
                              }} ,
                            controller: exe4_controller,
                            style: TextStyle(
                              color: AppColors.secondaryText,
                              decorationColor: Color.fromARGB(150, 156, 178, 184),
                              decorationThickness: 0.0,
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w400,
                              fontSize: 20,
                            ),
                            decoration: InputDecoration(
                              filled: false,
                              hintText: 'Exe 4: Insert exercise name',
                              hintStyle: TextStyle(color: Color.fromARGB(150, 156, 178, 184)),
                            ),
                          ),
                        ),
                      ),


                    ],
                  )
              ),


              // QUINTA ROW: WOD SCORE

              Container( // bordo verde
                width: SizeConfig.screenWidth*0.95, //300
                height: SizeConfig.screenHeight*0.35,
                margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.01), //40
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5, // 0.5
                    color: Color.fromARGB(255, 187, 206, 138),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(7)), //2
                ),

                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Align(
                        alignment: Alignment.topLeft,
                        child: Row(
                    children: [
                      Container(
                          width: SizeConfig.screenWidth*0.65, //300
                          margin: EdgeInsets.only(left: SizeConfig.screenHeight*0.04), // bottom:11
                          child: Opacity(
                            opacity: 0.95622,
                            child: Text(
                              "WOD score description: ",
                              // alignment: TextAlign.center,
                              style: TextStyle(
                                color: Color.fromARGB(255, 156, 178, 184),
                                fontFamily: "Lato",
                                fontWeight: FontWeight.w400,
                                fontSize: 22,
                              ),
                            ),
                          ),
                        ),
                      Align(
                        child: Container(
                          width: SizeConfig.screenWidth*0.2, //300
                          margin: EdgeInsets.only(left: SizeConfig.screenWidth*0.01, top:SizeConfig.screenHeight*0.015),
                          child: ButtonTheme(
                            child: FlatButton(
                              child: Opacity(
                                opacity: 1,
                                child: Image.asset(
                                  "assets/images/update-arrow-2.png",
                                  fit: BoxFit.fill,
                                ),
                              ),
                              onPressed: () {
                                is_description_editable = true;
                                print("Pressing Generate score description");
                                if (utilities.check_parameters_createEvent(context,wodNme_controller.text,
                                    wodType_controller.text,
                                    exe1_controller.text,
                                    exe2_controller.text,
                                    exe3_controller.text,
                                    exe4_controller.text)) {
                                  scoreDescription = utilities.generate_score_description(
                                      wodNme_controller.text,
                                      wodType_controller.text,
                                      exe1_controller.text,
                                      exe2_controller.text,
                                      exe3_controller.text,
                                      exe4_controller.text,
                                  context);
                                  scoreDescription_controller.text =
                                      scoreDescription;
                                }

                              },
                            ),
                          ),
                        ),
                      ),
          ]
          )
                      ),
                      Align(
                        alignment: Alignment.topLeft,
                          child: Opacity(
                              opacity: 0.95622,
                              child: Container(
                                  width: SizeConfig.screenWidth*0.9,
                                  height: SizeConfig.screenHeight*0.2,
                                  margin:EdgeInsets.only(
                                    //right: SizeConfig.screenWidth*0.07,
                                    left: SizeConfig.screenWidth*0.05,
                                    right: SizeConfig.screenWidth*0.05,

                                    //top: SizeConfig.screenHeight*0.075,
                                  ),
                                  child: TextField(
                                    onChanged: (text) {is_description_editable = false; },
                                    controller: scoreDescription_controller,
                                    maxLines: 13,
                                    style: TextStyle(
                                      color: Colors.white,
                                      decorationColor: Color.fromARGB(150, 156, 178, 184),
                                      decorationThickness: 0.0,
                                      fontFamily: "Lato",
                                      fontWeight: FontWeight.w400,
                                      fontSize: 20,

                                    ),
                                    decoration: InputDecoration(
                                      filled: false,
                                      hintText: 'Press the button to generate score description.',
                                      hintStyle: TextStyle(color: Color.fromARGB(150, 156, 178, 184)),
                                    ),
                                  )
                              )
                          ),

                      ),

                    ]

                ),

              ),



// SESTA ROW: Add photo




              //  SETTIMA ROW  : container immagine

              Container( // bordo bianco
                width: SizeConfig.screenWidth*0.95, //300
                height: SizeConfig.screenHeight*0.5,
                margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.01), //40
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5, // 0.5
                    color: Color.fromARGB(255, 187, 206, 138),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(50)), //2
                ),
                child: Center(
                  child: wod_image == null
                      ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          // alignment: Alignment.centerLeft,
                          child: Container(
                            margin: EdgeInsets.symmetric(),
                            child: Opacity(
                              opacity: 1,
                              child: Text(
                                "Add photo:",
                                // alignment: TextAlign.center,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 183, 202, 207),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 30,
                                ),
                              ),
                            ),
                          ),
                        ),

                        //Spacer(),

                        Align(
                          // alignment: Alignment.topRight,
                          child: Container(
                            width: SizeConfig.screenWidth*0.4, // dimensioni tasto add photo
                            height: SizeConfig.screenHeight*0.4,
                            margin: EdgeInsets.symmetric(),
                            child: FloatingActionButton(
                              onPressed: getImage,
                              child: Image.asset(
                                "assets/images/photo-2.png",
                              ),
                              backgroundColor: Color.fromARGB(0, 0, 0, 0),
                              // child: Icon(Icons.add_a_photo),
                            ),
                          ),
                        ),
                      ]
                  )
                      :Column(
                  children: [

                    Container(
                      height: SizeConfig.screenHeight*0.01,
                    ),

                    Image.file(
                      wod_image,
                      fit: BoxFit.contain,
                      height: SizeConfig.screenHeight*0.4,
                      width: SizeConfig.screenWidth*0.8,
                    ),

                    Container(
                      height: SizeConfig.screenHeight*0.015,
                    ),

                    RaisedButton(
                      onPressed: getImage,
                      color: Color.fromARGB(255, 187, 206, 138),
                      elevation: 10.0,
                      child: Text(
                        "Change Image",
                        style: TextStyle(
                          fontFamily: "Lato",
                          fontWeight: FontWeight.w400,
                          fontSize: 24,
                          color: Color.fromARGB(217, 0, 0, 0),
                        ),
                      ),
                    ),


                  ]
                  )
                ),
              ),



              // OTTAVA ROW: freccie avanti e indietro

              Container( // bordo bianco
                width: SizeConfig.screenWidth*0.65,
                height: SizeConfig.screenHeight*0.2,
                margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.01), //40

                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        margin: EdgeInsets.only(right: SizeConfig.screenHeight*0.1), //40
                        child: ButtonTheme(
                          child: FlatButton(
                            child: Opacity(
                              opacity: 1,
                              child: Image.asset(
                                "assets/images/icon-arrow-right-4.png",
                                fit: BoxFit.none,
                              ),
                            ),
                            onPressed: () {
                              print("Pressing Next Button");
                              scoreDescription = scoreDescription_controller.text;
                              utilities.save_addEvent_data( wodNme_controller.text,
                                  wodType_controller.text, exe1_controller.text,
                                  exe2_controller.text, exe3_controller.text,
                                  exe4_controller.text, scoreDescription, wod_image);
                              if (scoreDescription != '') {
                                //wod_info['name'] = wod_name;
                                //wod_info['type'] = wod_type;
                                //wod_info['ex1'] = ex1;
                                //wod_info['ex2'] = ex2;
                                //wod_info['ex3'] = ex3;
                                //wod_info['ex4'] = ex4;
                                //wod_info['description'] = scoreDescription;
                                //wod_info["image"] = wod_image == null
                                    //? "none" : wod_image.path;
                                // check if image is been selected
                                if (wod_image == null) {
                                  //notify user that no image has been selected
                                  utilities.showAlert2(context, "No image has "
                                      "been selected, do you want to continue anyway?");

                                }
                                else{

                                  Navigator.pushNamed(context, '/Addevent2');
                                }

                              }
                              else{
                                utilities.showAlert(context, "Score description cannot be empty.");
                              }

                              // Navigator.pushNamed(context, '/AddEvent1Widget');
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),

    );
  }
}

