
import 'package:flutter/material.dart';
import 'package:project/values/values.dart';


class GalleryWidget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      body: Opacity(
        opacity: 1,
        child: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            color: Color.fromARGB(217, 0, 0, 0),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                height: 96,
                child: Opacity(
                  opacity: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        height: 20,
                        margin: EdgeInsets.only(left: 15, top: 9, right: 15),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Opacity(
                                opacity: 1,
                                child: Text(
                                  "PHOTO STREAM",
                                  // alignment: TextAlign.left,
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 212, 136, 0),
                                    fontFamily: "",
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                            ),
                            Spacer(),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                width: 16,
                                height: 4,
                                margin: EdgeInsets.only(top: 9),
                                child: Opacity(
                                  opacity: 1,
                                  child: Image.asset(
                                    "assets/images/ic-manage.png",
                                    fit: BoxFit.none,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 45,
                        margin: EdgeInsets.only(left: 15, top: 11),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Positioned(
                              left: 0,
                              top: 0,
                              right: 0,
                              child: Opacity(
                                opacity: 1,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Container(
                                      width: 44,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.stretch,
                                        children: [
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: Opacity(
                                              opacity: 1,
                                              child: Text(
                                                "Feed",
                                                // alignment: TextAlign.left,
                                                style: TextStyle(
                                                  color: AppColors.primaryText,
                                                  fontFamily: "",
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 20,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Spacer(),
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: Container(
                                              width: 24,
                                              height: 4,
                                              margin: EdgeInsets.only(left: 13),
                                              child: Opacity(
                                                opacity: 1,
                                                child: Image.asset(
                                                  "assets/images/current.png",
                                                  fit: BoxFit.none,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(left: 25),
                                        child: Opacity(
                                          opacity: 1,
                                          child: Text(
                                            "Places",
                                            // alignment: TextAlign.left,
                                            style: TextStyle(
                                              color: Color.fromARGB(255, 78, 78, 81),
                                              fontFamily: "",
                                              fontWeight: FontWeight.w400,
                                              fontSize: 20,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(right: 27),
                                        child: Opacity(
                                          opacity: 1,
                                          child: Text(
                                            "People",
                                            // alignment: TextAlign.left,
                                            style: TextStyle(
                                              color: Color.fromARGB(255, 78, 78, 81),
                                              fontFamily: "",
                                              fontWeight: FontWeight.w400,
                                              fontSize: 20,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(right: 15),
                                        child: Opacity(
                                          opacity: 1,
                                          child: Text(
                                            "Landscapes",
                                            // alignment: TextAlign.left,
                                            style: TextStyle(
                                              color: Color.fromARGB(255, 78, 78, 81),
                                              fontFamily: "",
                                              fontWeight: FontWeight.w400,
                                              fontSize: 20,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Positioned(
                              top: 5,
                              right: 2,
                              child: Opacity(
                                opacity: 1,
                                child: Container(
                                  width: 20,
                                  height: 40,
                                  decoration: BoxDecoration(
                                    gradient: Gradients.primaryGradient,
                                  ),
                                  child: Container(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                height: 540,
                margin: EdgeInsets.only(top: 31),
                child: Opacity(
                  opacity: 1,
                  child: Image.asset(
                    "assets/images/items.png",
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}