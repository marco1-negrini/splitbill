
import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:project/add_event2_widget/calendar_for_addEvent.dart' as prefix0;
import 'package:project/data.dart';
import 'package:project/utilities.dart';

import 'package:project/values/values.dart';

import '../SizeConfig.dart';
// import 'flutter_calendar.dart';


class AddEvent2Widget extends StatelessWidget {
  void handleNewDate(date) {
    print("handleNewDate ${date}");
  }

  @override
  Widget build(BuildContext context) {
    //HashMap<String, Object> wod_info = ModalRoute.of(context).settings.arguments;
    //print("wod_info:");
    //print(wod_info);
    //display_addEvent_data();
    return new MaterialApp(
      theme: new ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.purple,
      ),
      home: new Scaffold(
        /*appBar: new AppBar(
          title: new Text('Calendar'),
        ),*/
        body: new Container(
          margin: new EdgeInsets.symmetric(
            horizontal: 5.0,
            vertical: 10.0,
          ),
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              new prefix0.Calendar(
                onSelectedRangeChange: (range) =>
                    print("Range is ${range.item1}, ${range.item2}"),
                isExpandable: true,

              ),
              /*new Divider(
                height: 50.0,
              ),*/
              Container( // bordo bianco
                width: SizeConfig.screenWidth*0.65,
                height: SizeConfig.screenHeight*0.2,
                margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.01), //40

                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        margin: EdgeInsets.only(left: SizeConfig.screenHeight*0.1), //40
                        child: ButtonTheme(
                          child: FlatButton(
                            child: Opacity(
                              opacity: 1,
                              child: Image.asset(
                                "assets/images/icon-arrow-left-4.png",
                                fit: BoxFit.none,
                              ),
                            ),
                            onPressed: () {
                              Navigator.pushNamed(context, '/Addevent1');
                              print("Pressing Back Button");
                              // Navigator.pushNamed(context, '/AddEvent1Widget');
                            },
                          ),
                        ),
                      ),
                    ),

                    Spacer(),

                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        margin: EdgeInsets.only(right: SizeConfig.screenHeight*0.1), //40
                        child: ButtonTheme(
                          child: FlatButton(
                            child: Opacity(
                              opacity: 1,
                              child: Image.asset(
                                "assets/images/icon-arrow-right-4.png",
                                fit: BoxFit.none,
                              ),
                            ),
                            onPressed: () {
                              Navigator.pushNamed(context, '/Addevent3');
                              print("Pressing Next Button");
                              // Navigator.pushNamed(context, '/AddEvent1Widget');
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),

    );
  }
}