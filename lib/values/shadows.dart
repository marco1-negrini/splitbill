
part of values;


class Shadows {
  static const BoxShadow primaryShadow = BoxShadow(
    color: Color.fromARGB(128, 0, 0, 0),
    offset: Offset(0, 2),
    blurRadius: 2,
  );
  static const BoxShadow secondaryShadow = BoxShadow(
    color: Color.fromARGB(26, 0, 0, 0),
    offset: Offset(0, 50),
    blurRadius: 80,
  );
}