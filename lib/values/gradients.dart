
part of values;


class Gradients {
  static const Gradient primaryGradient = LinearGradient(
    begin: Alignment(0.01196, 0.5),
    end: Alignment(1, 0.5),
    stops: [
      0,
      1,
    ],
    colors: [
      Color.fromARGB(11, 15, 15, 19),
      Color.fromARGB(255, 15, 15, 19),
    ],
  );
}