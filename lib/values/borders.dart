
part of values;


class Borders {
  static const BorderSide primaryBorder = BorderSide(
    color: Color.fromARGB(255, 151, 151, 151),
    width: 0.5,
    style: BorderStyle.solid,
  );
  static const BorderSide secondaryBorder = BorderSide(
    color: Color.fromARGB(255, 27, 99, 220),
    width: 0.97656,
    style: BorderStyle.solid,
  );
}