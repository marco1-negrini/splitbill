
part of values;


class AppColors {
  static const Color primaryBackground = Color.fromARGB(255, 187, 206, 138);
  static const Color secondaryBackground = Color.fromARGB(255, 255, 255, 255);
  static const Color primaryElement = Color.fromARGB(255, 216, 216, 216);
  static const Color secondaryElement = Color.fromARGB(255, 255, 255, 255);
  static const Color accentElement = Color.fromARGB(159, 106, 106, 106);
  static const Color primaryText = Color.fromARGB(255, 255, 255, 255);
  static const Color secondaryText = Color.fromARGB(255, 255, 255, 255);
  static const Color accentText = Color.fromARGB(255, 146, 146, 146);
}