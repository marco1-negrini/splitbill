
import 'package:flutter/material.dart';
import 'package:project/values/values.dart';


class Partecipants3Widget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      body: Opacity(
        opacity: 1,
        child: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            color: Color.fromARGB(217, 0, 0, 0),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                height: 67,
                margin: EdgeInsets.only(left: 53, top: 50, right: 25),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(top: 45),
                        child: Opacity(
                          opacity: 0.95622,
                          child: Text(
                            "Chiara Di Vece (V1)",
                            // alignment: TextAlign.left,
                            style: TextStyle(
                              color: AppColors.secondaryText,
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w700,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: 50,
                        height: 49,
                        child: Opacity(
                          opacity: 1,
                          child: Image.asset(
                            "assets/images/picture2.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Opacity(
                  opacity: 1,
                  child: Container(
                    width: 200,
                    height: 1,
                    margin: EdgeInsets.only(left: 53),
                    decoration: BoxDecoration(
                      color: AppColors.primaryElement,
                      border: Border.fromBorderSide(Borders.primaryBorder),
                    ),
                    child: Container(),
                  ),
                ),
              ),
              Container(
                height: 132,
                margin: EdgeInsets.only(left: 61, top: 52, right: 62),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: 93,
                        height: 90,
                        child: Opacity(
                          opacity: 1,
                          child: Image.asset(
                            "assets/images/immagine1.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Opacity(
                        opacity: 0.95622,
                        child: Text(
                          "Age: 24\nHeight: 170 cm\nWeight: 65 kg\nCircumferences: -\n\n",
                          // alignment: TextAlign.left,
                          style: TextStyle(
                            color: AppColors.secondaryText,
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 301,
                  margin: EdgeInsets.only(top: 10),
                  child: Opacity(
                    opacity: 1,
                    child: Text(
                      "I would like to go beyond my limits and improve my physical condition, adding a training routine to my university career.",
                      // alignment: TextAlign.center,
                      style: TextStyle(
                        color: AppColors.secondaryText,
                        fontFamily: "Lato",
                        fontWeight: FontWeight.w300,
                        // style: FontStyle.italic,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              ),
              Spacer(),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 215,
                  height: 200,
                  margin: EdgeInsets.only(bottom: 22),
                  child: Opacity(
                    opacity: 1,
                    child: Image.asset(
                      "assets/images/slot-frequency-chiara.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 118,
                  height: 39,
                  margin: EdgeInsets.only(left: 129, bottom: 112),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Positioned(
                        bottom: 0,
                        child: Opacity(
                          opacity: 1,
                          child: Container(
                            width: 118,
                            height: 39,
                            decoration: BoxDecoration(
                              color: AppColors.secondaryElement,
                              border: Border.all(
                                width: 0.5,
                                color: Color.fromARGB(255, 217, 217, 217),
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(19.47953)),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Opacity(
                                  opacity: 1,
                                  child: Container(
                                    width: 37,
                                    height: 35,
                                    margin: EdgeInsets.only(right: 5),
                                    decoration: BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color.fromARGB(128, 0, 0, 0),
                                          offset: Offset(0, 1),
                                          blurRadius: 2,
                                        ),
                                      ],
                                    ),
                                    child: Image.asset(
                                      "assets/images/button.png",
                                      fit: BoxFit.none,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        left: 10,
                        bottom: 5,
                        child: Opacity(
                          opacity: 1,
                          child: Image.asset(
                            "assets/images/calendar.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 325,
                  height: 152,
                  margin: EdgeInsets.only(bottom: 52),
                  child: Opacity(
                    opacity: 1,
                    child: Image.asset(
                      "assets/images/immagine1-3.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 162,
                  height: 46,
                  margin: EdgeInsets.only(bottom: 54),
                  child: Opacity(
                    opacity: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Container(
                          height: 18,
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              Positioned(
                                left: 0,
                                top: 0,
                                right: 0,
                                child: Opacity(
                                  opacity: 1,
                                  child: Container(
                                    height: 18,
                                    decoration: BoxDecoration(
                                      color: Color.fromARGB(255, 187, 206, 138),
                                      border: Border.fromBorderSide(Borders.primaryBorder),
                                      borderRadius: BorderRadius.all(Radius.circular(2)),
                                    ),
                                    child: Container(),
                                  ),
                                ),
                              ),
                              Positioned(
                                left: 8,
                                top: 2,
                                right: 30,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        width: 16,
                                        height: 14,
                                        child: Opacity(
                                          opacity: 1,
                                          child: Image.asset(
                                            "assets/images/download-arrow-2.png",
                                            fit: BoxFit.none,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 1),
                                        child: Opacity(
                                          opacity: 0.95622,
                                          child: Text(
                                            "DOWNLOAD csv",
                                            // alignment: TextAlign.center,
                                            style: TextStyle(
                                              color: Color.fromARGB(255, 64, 80, 85),
                                              fontFamily: "Lato",
                                              fontWeight: FontWeight.w700,
                                              fontSize: 11,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Spacer(),
                        Container(
                          height: 18,
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              Positioned(
                                left: 0,
                                right: 0,
                                bottom: 0,
                                child: Opacity(
                                  opacity: 1,
                                  child: Container(
                                    height: 18,
                                    decoration: BoxDecoration(
                                      color: Color.fromARGB(255, 187, 206, 138),
                                      border: Border.fromBorderSide(Borders.primaryBorder),
                                      borderRadius: BorderRadius.all(Radius.circular(2)),
                                    ),
                                    child: Container(),
                                  ),
                                ),
                              ),
                              Positioned(
                                left: 8,
                                right: 29,
                                bottom: 2,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Align(
                                      alignment: Alignment.bottomLeft,
                                      child: Container(
                                        width: 16,
                                        height: 14,
                                        child: Opacity(
                                          opacity: 1,
                                          child: Image.asset(
                                            "assets/images/download-arrow-2.png",
                                            fit: BoxFit.none,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    Align(
                                      alignment: Alignment.bottomLeft,
                                      child: Opacity(
                                        opacity: 0.95622,
                                        child: Text(
                                          "DOWNLOAD pdf",
                                          // alignment: TextAlign.center,
                                          style: TextStyle(
                                            color: Color.fromARGB(255, 64, 80, 85),
                                            fontFamily: "Lato",
                                            fontWeight: FontWeight.w700,
                                            fontSize: 11,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 215,
                  height: 120,
                  margin: EdgeInsets.only(left: 50, bottom: 52),
                  child: Opacity(
                    opacity: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Opacity(
                            opacity: 0.95622,
                            child: Text(
                              "Availabilities",
                              // alignment: TextAlign.left,
                              style: TextStyle(
                                color: AppColors.secondaryText,
                                fontFamily: "Lato",
                                fontWeight: FontWeight.w300,
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ),
                        Opacity(
                          opacity: 1,
                          child: Container(
                            height: 1,
                            margin: EdgeInsets.only(right: 15),
                            decoration: BoxDecoration(
                              color: AppColors.primaryElement,
                              border: Border.fromBorderSide(Borders.primaryBorder),
                            ),
                            child: Container(),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 9),
                          child: Opacity(
                            opacity: 0.95622,
                            child: Text(
                              "Mon:		7:45	-	19:15\nTue:		7:45	-	19:15\nThu:		18:30  -	19:15\nFri:			18:30  -	19:15",
                             //  alignment: TextAlign.left,
                              style: TextStyle(
                                color: AppColors.secondaryText,
                                fontFamily: "Lato",
                                fontWeight: FontWeight.w300,
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                height: 25,
                margin: EdgeInsets.only(left: 138, right: 170),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                        width: 17,
                        height: 25,
                        child: Opacity(
                          opacity: 1,
                          child: Image.asset(
                            "assets/images/icon-arrow-left-4-2.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                        width: 17,
                        height: 25,
                        child: Opacity(
                          opacity: 1,
                          child: Image.asset(
                            "assets/images/icon-arrow-left-4-2.png",
                            fit: BoxFit.none,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}