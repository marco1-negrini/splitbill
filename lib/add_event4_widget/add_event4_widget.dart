
import 'package:flutter/material.dart';
import 'package:project/data.dart';
import 'package:project/utilities.dart';
import 'package:project/values/values.dart';

import '../SizeConfig.dart';


class AddEvent4Widget extends StatelessWidget {
  TextEditingController watchWord_controller = new TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    watchWord_controller.text = watchWord;

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,

      body: Opacity(
        opacity: 1,
        child: Container( // container di tutto
          width: SizeConfig.screenWidth,
          height: SizeConfig.screenHeight,
          decoration: BoxDecoration(
            color: Color.fromARGB(217, 0, 0, 0), // 217
          ),


          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [

              //here
              Align(
                // alignment: Alignment.topCenter,
                child: Container(
                  width: SizeConfig.screenWidth*0.65,
                  height: SizeConfig.screenHeight*0.1,
                  margin: EdgeInsets.only(
                      left: SizeConfig.screenWidth*0.175,
                      right: SizeConfig.screenWidth*0.175, top: SizeConfig.screenHeight*0.05
                  ),
                  alignment: Alignment.topCenter,
                  child: Opacity(
                    //alignment: Alignment.topRight,
                    opacity: 1,
                    child: Row(
                      children: [
                        Opacity(
                          opacity: 1,
                          child: Image.asset(
                            "assets/images/key.png",
                            fit: BoxFit.none,
                          ),
                        ),
                        Opacity(
                          opacity: 1,
                          child: Text(
                            "“Dimmi che ci sei”\n     watchword",
                            style: TextStyle(
                              color: Color.fromARGB(255, 211, 225, 228),
                              fontFamily: "Lato",
                              fontWeight: FontWeight.w300,
                              // style: FontStyle.italic,
                              fontSize: 24,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),

              // Contiene il box per scrivere la watchword

              Container(
                alignment: Alignment.center,
                width: SizeConfig.screenWidth,
                height: SizeConfig.screenHeight*0.1,
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 245, 248, 249),
                  border: Border.fromBorderSide(Borders.primaryBorder),
                  borderRadius: BorderRadius.all(Radius.circular(10)), //1.14946
                ),
                margin: EdgeInsets.only(
                    left: SizeConfig.screenWidth*0.175,
                    top:SizeConfig.screenHeight*0.01 ,
                    right: SizeConfig.screenWidth*0.175
                ),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Opacity(
                      opacity: 1,
                      child: TextField(
                        onChanged: (text) {
                          //print( watchWord_controller.text);
                          watchWord = watchWord_controller.text;},
                        controller: watchWord_controller,
                        decoration: InputDecoration(
                          filled: true,
                          hintText: 'Insert watchword',
                        ),
                        style: TextStyle(
                          // color: Color.fromARGB(255, 156, 178, 184),
                          fontFamily: "Lato",
                          fontWeight: FontWeight.w400,
                          fontSize: 20,
                        ),
                      ),
                    ),
                  ],
                ),
              ),

           // contiene la frase Classes and WOD created
              /*Align(
                child: Container(
                  width: SizeConfig.screenWidth*0.95,
                  height: SizeConfig.screenHeight*0.07,
                  alignment: Alignment.topCenter,
                  margin: EdgeInsets.only(
                      top: SizeConfig.screenHeight*0.05
                  ),
                  child: Opacity(
                    opacity: 0.95622,
                    child: Text(
                      "Classes and WOD created!",
                      // alignment: TextAlign.center,
                      style: TextStyle(
                        color: AppColors.secondaryText,
                        fontFamily: "Lato",
                        fontWeight: FontWeight.w400,
                        fontSize: 27,
                      ),
                    ),
                  ),
                ),
              ),*/
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  width: SizeConfig.screenWidth*0.7,
                  height: SizeConfig.screenHeight*0.05,
                  margin: EdgeInsets.only(
                    top:SizeConfig.screenHeight*0.07,
                    left: SizeConfig.screenWidth*0.04,
                  ),
                  child: Opacity(
                    opacity: 1,
                    child: Text(
                      "Training Recap:",
                      // alignment: TextAlign.center,
                      style: TextStyle(
                        color: Color.fromARGB(255, 211, 225, 228),
                        fontFamily: "Lato",
                        fontWeight: FontWeight.w400,
                        // style: FontStyle.italic,
                        fontSize: 24,
                      ),
                    ),
                  ),
                ),
              ),

              Container( // bordo bianco
                width: SizeConfig.screenWidth*0.95,
                height: SizeConfig.screenHeight*0.05,
                margin: EdgeInsets.only(

                  left: SizeConfig.screenWidth*0.03,
                  right: SizeConfig.screenWidth*0.03, top: SizeConfig.screenHeight*0.03), //70
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5, // 0.5
                    color: Color.fromARGB(255, 187, 206, 138),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(7)), //2
                ),

                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      child: Container(
                        margin:EdgeInsets.only(
                          //right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth*0.04,
                          //top: SizeConfig.screenHeight*0.075,
                        ),
                        child: Text(
                          "WOD name: ",
                          //alignment: TextAlign.left,
                          style: TextStyle(
                            color: Color.fromARGB(255, 156, 178, 184),
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 22,
                          ),
                        ),
                      ),
                    ),
                    // Spacer(),
                    Align(
                      child: Container(
                        width: SizeConfig.screenWidth*0.5,
                        margin:EdgeInsets.only(
                          //right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth*0.01,
                          // top: SizeConfig.screenHeight*0.075,
                        ),
                        child: Text(
                          wod_name,
                          style: TextStyle(
                            color: AppColors.accentText,
                            decorationColor: Color.fromARGB(150, 156, 178, 184),
                            decorationThickness: 0.0,
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              Container( // bordo bianco
                width: SizeConfig.screenWidth*0.95,
                height: SizeConfig.screenHeight*0.05,
                margin: EdgeInsets.only(
                  top: SizeConfig.screenHeight*0.005,
                  left: SizeConfig.screenWidth*0.03,
                  right: SizeConfig.screenWidth*0.03,), //70
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5, // 0.5
                    color: Color.fromARGB(255, 187, 206, 138),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(7)), //2
                ),

                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      child: Container(
                        margin:EdgeInsets.only(
                          //right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth*0.04,
                          //top: SizeConfig.screenHeight*0.075,
                        ),
                        child: Text(
                          "WOD type: ",
                          //alignment: TextAlign.left,
                          style: TextStyle(
                            color: Color.fromARGB(255, 156, 178, 184),
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 22,
                          ),
                        ),
                      ),
                    ),
                    // Spacer(),
                    Align(
                      child: Container(
                        width: SizeConfig.screenWidth*0.5,
                        margin:EdgeInsets.only(
                          //right: SizeConfig.screenWidth*0.07,
                          left: SizeConfig.screenWidth*0.01,
                          // top: SizeConfig.screenHeight*0.075,
                        ),
                        child: Text(
                          wod_type,
                          style: TextStyle(
                            color: AppColors.accentText,
                            decorationColor: Color.fromARGB(150, 156, 178, 184),
                            decorationThickness: 0.0,
                            fontFamily: "Lato",
                            fontWeight: FontWeight.w400,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),



              Container( // bordo bianco
                width: SizeConfig.screenWidth*0.95,
                height: SizeConfig.screenHeight*0.252,
                margin: EdgeInsets.only(
                  top: SizeConfig.screenHeight*0.01,
                  left: SizeConfig.screenWidth*0.03,
                  right: SizeConfig.screenWidth*0.03,), //70
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5, // 0.5
                    color: Color.fromARGB(255, 187, 206, 138),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(7)), //2
                ),

                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [

                    Container( // bordo bianco
                      width: SizeConfig.screenWidth*0.95,
                      height: SizeConfig.screenHeight*0.05,
                      margin: EdgeInsets.only(
                        top: SizeConfig.screenHeight*0.005,
                        left: SizeConfig.screenWidth*0.02,
                        right: SizeConfig.screenWidth*0.02,
                        bottom: SizeConfig.screenHeight*0.005,), //70


                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            child: Container(
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.04,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "Ex 1: ",
                                //alignment: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 156, 178, 184),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.7,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.01,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                ex1,
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      width: SizeConfig.screenWidth*0.95,
                      height: SizeConfig.screenHeight*0.05,
                      margin: EdgeInsets.only(
                        left: SizeConfig.screenWidth*0.02,
                        right: SizeConfig.screenWidth*0.02,
                        top: SizeConfig.screenHeight*0.005,
                        bottom: SizeConfig.screenHeight*0.005,), //70
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            child: Container(
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.04,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "Ex 2: ",
                                //alignment: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 156, 178, 184),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.7,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.01,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                ex2,
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      width: SizeConfig.screenWidth*0.95,
                      height: SizeConfig.screenHeight*0.05,
                      margin: EdgeInsets.only(
                        left: SizeConfig.screenWidth*0.02,
                        right: SizeConfig.screenWidth*0.02,
                        bottom: SizeConfig.screenHeight*0.005,), //70


                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            child: Container(
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.04,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "Ex 3: ",
                                //alignment: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 156, 178, 184),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.7,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.01,
                                top: SizeConfig.screenHeight*0.005,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                ex3,
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container( // bordo bianco
                      width: SizeConfig.screenWidth*0.95,
                      height: SizeConfig.screenHeight*0.05,
                      margin: EdgeInsets.only(
                        left: SizeConfig.screenWidth*0.02,
                        right: SizeConfig.screenWidth*0.02,
                        top: SizeConfig.screenHeight*0.005,
                        bottom: SizeConfig.screenHeight*0.005,), //70


                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            child: Container(
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.04,
                                //top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                "Ex 4: ",
                                //alignment: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 156, 178, 184),
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          // Spacer(),
                          Align(
                            child: Container(
                              width: SizeConfig.screenWidth*0.7,
                              margin:EdgeInsets.only(
                                //right: SizeConfig.screenWidth*0.07,
                                left: SizeConfig.screenWidth*0.01,
                                // top: SizeConfig.screenHeight*0.075,
                              ),
                              child: Text(
                                ex4,
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  decorationColor: Color.fromARGB(150, 156, 178, 184),
                                  decorationThickness: 0.0,
                                  fontFamily: "Lato",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),






              // contiene la chiave e la frase dimmi che ci sei...
              //start


            // contiene l'immagine high five per andare alla home


             /*
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: SizeConfig.screenWidth*0.4,
                  height: SizeConfig.screenWidth*0.3,
                  margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.01),
                  child: ButtonTheme(
                    child: FlatButton(
                      child: Opacity(
                        opacity: 1,
                        child: Image.asset(
                          "assets/images/high-five.png",
                          fit: BoxFit.none,
                        ),
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, '/HomePage');
                        print("Pressing high five button, back home and saving the watchword");
                        // Navigator.pushNamed(context, '/AddEvent1Widget');
                      },
                    ),
                  ),
                ),
              ),

              */

             //TODO:











              Container( // bordo bianco
                width: SizeConfig.screenWidth*0.65,
                height: SizeConfig.screenHeight*0.18,
                margin: EdgeInsets.only(top: SizeConfig.screenHeight*0.01), //40

                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        margin: EdgeInsets.only(left: SizeConfig.screenHeight*0.1), //40
                        child: ButtonTheme(
                          child: FlatButton(
                            child: Opacity(
                              opacity: 1,
                              child: Image.asset(
                                "assets/images/icon-arrow-left-4.png",
                                fit: BoxFit.none,
                              ),
                            ),
                            onPressed: () {
                              Navigator.pushNamed(context, '/Addevent3');
                              print("Pressing Back Button");
                              // Navigator.pushNamed(context, '/AddEvent1Widget');
                            },
                          ),
                        ),
                      ),
                    ),

                    Spacer(),

                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        margin: EdgeInsets.only(right: SizeConfig.screenHeight*0.1), //40
                        child: ButtonTheme(
                          child: FlatButton(
                            child: Opacity(
                              opacity: 1,
                              child: Image.asset(
                                "assets/images/high-five.png",
                                fit: BoxFit.none,
                              ),
                            ),
                            onPressed: () {
                              post_event(watchWord_controller.text);
                              Navigator.pushNamed(context, '/HomePage');
                              print("Pressing high five Button");
                              // Navigator.pushNamed(context, '/AddEvent1Widget');
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),


            ],
          ),



        ),
      ),
    );
  }
}