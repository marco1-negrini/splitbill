import 'dart:collection';
import 'dart:io';

import 'package:flutter/material.dart';

HashMap registered_users;
HashMap imagesMapping;
List<HashMap> participantPreviews;
List<HashMap> participantProfiles;

HashMap participantResults;

List<List> participantPresences_oct;
List<String> presences1_oct;
List<String> presences2_oct;
List<String> presences3_oct;
List<String> presences4_oct;
List<String> presences5_oct;

List<List> participantPresences;
List<String> presences1;
List<String> presences2;
List<String> presences3;
List<String> presences4;
List<String> presences5;


List<List> participationperday_oct;
List<String> firstday_oct;
List<String> secondday_oct;
List<String> thirdday_oct;
List<String> fourthday_oct;

List<List> participationperday;
List<String> firstday;
List<String> secondday;
List<String> thirdday;
List<String> fourthday;

List<List> participationpertimeslot_oct;
List<String> firstslot_oct;
List<String> secondslot_oct;
List<String> thirdslot_oct;
List<String> fourthslot_oct;
List<String> fifthslot_oct;
List<String> sixthslot_oct;

List<List> participationpertimeslot;
List<String> firstslot;
List<String> secondslot;
List<String> thirdslot;
List<String> fourthslot;
List<String> fifthslot;
List<String> sixthslot;

HashMap reportsdata;
List<List> october2019;
List<List>   november2019;

List<List> zero;
List<List>   participantPresences_zero;
List<String>  presences1_zero;
List<String>  presences2_zero;
List<String>  presences3_zero;
List<String>  presences4_zero;
List<String>  presences5_zero;

List<List> participationperday_zero;
List<String> firstday_zero;
List<String> secondday_zero;
List<String> thirdday_zero;
List<String> fourthday_zero;

List<List> participationpertimeslot_zero;
List<String> firstslot_zero;
List<String> secondslot_zero;
List<String> thirdslot_zero;
List<String> fourthslot_zero;
List<String> fifthslot_zero;
List<String> sixthslot_zero;

bool already_loaded = false;
String scoreDescription;
String wod_name;
String wod_type;
String ex1, ex2, ex3, ex4, watchWord;
bool is_description_editable = true;
File wod_image;
List <String> listOfSelectedDays = new List();
List<String> wod_days;
List<String> wod_week_days;
HashMap<String,bool> is_time_selected = new HashMap();
List<String> suggested_watchWords = new List();
HashMap<String, HashMap> wods=new HashMap();
String selected_wod = "20191213";


load_Data()
{
  /* Method to load data */
  if (already_loaded){
    return;}


  already_loaded = true;


  //data relative to registered coaches
  // username -> password
  registered_users = new HashMap<String, String>();
  registered_users["andal"] = "andal123";
  registered_users["melissa"] = "melissa123";

  // Data relative to coaches' profile images
  imagesMapping = new HashMap<String, String>();

  imagesMapping["andal"] = "assets/images/andal.png";
  imagesMapping["melissa"] = "assets/images/melissa.png";

  // Data relative to participants' previews (profile images and IDs)

  participantPreviews = new List<HashMap>();
  HashMap participantPreview1 = new HashMap<String, String>();
  HashMap participantPreview2 = new HashMap<String, String>();
  HashMap participantPreview3 = new HashMap<String, String>();
  HashMap participantPreview4 = new HashMap<String, String>();
  HashMap participantPreview5 = new HashMap<String, String>();

  participantPreview1["ID"] = "1";
  participantPreview1["Image"] = "assets/images/lore.png";

  participantPreview2["ID"] = "2";
  participantPreview2["Image"] = "assets/images/marco.png";

  participantPreview3["ID"] = "3";
  participantPreview3["Image"] = "assets/images/salvatore.png";

  participantPreview4["ID"] = "4";
  participantPreview4["Image"] = "assets/images/chiara.png";

  participantPreview5["ID"] = "5";
  participantPreview5["Image"] = "assets/images/piervi.png";

  participantPreviews.add(participantPreview1);
  participantPreviews.add(participantPreview2);
  participantPreviews.add(participantPreview3);
  participantPreviews.add(participantPreview4);
  participantPreviews.add(participantPreview5);

  // Data relative to participantProfiles
  // {"ID": string, "Image": String, "name": string, "lastname": string, "age":String,
  // "height" = String, "weight" : String,
  // "circumference": string, "descriptive_sentence": String}


  participantProfiles = new List<HashMap>();


  HashMap participantProfile1 = new HashMap<String, String>();
  HashMap participantProfile2 = new HashMap<String, String>();
  HashMap participantProfile3 = new HashMap<String, String>();
  HashMap participantProfile4 = new HashMap<String, String>();
  HashMap participantProfile5 = new HashMap<String, String>();

  participantProfile1["ID"] = "1";
  participantProfile1["Image"] = "assets/images/lore.png";
  participantProfile1["name"] = "Lore";
  participantProfile1["lastname"] = "Rezza";
  participantProfile1["age"] = "22";
  participantProfile1["height"] = "1.90";
  participantProfile1["weight"] = "98";
  participantProfile1["circumference"] = "135";
  participantProfile1["descriptive_sentence"] = "I should stop eating too much " +
      "Nutella and start attending CrossFit lessons...";
  participantProfile1["presences"] = "20";
  participantProfile1["day1"] = "Martedì 18:45";
  participantProfile1["day2"] = "Venerdì 7:45";
  participantProfile1["freq1"] = "70";
  participantProfile1["freq2"] = "80";

  participantProfile2["ID"] = "2";
  participantProfile2["Image"] = "assets/images/marco.png";
  participantProfile2["name"] = "Marco";
  participantProfile2["lastname"] = "ByeBye";
  participantProfile2["age"] = "23";
  participantProfile2["height"] = "1.77";
  participantProfile2["weight"] = "63";
  participantProfile2["circumference"] = "70";
  participantProfile2["descriptive_sentence"] = "I love fitness and watching " +
      "Netflix all night long.";
  participantProfile2["presences"] = "4";
  participantProfile2["day1"] = "Lunedi 18:45";
  participantProfile2["day2"] = "Giovedi 7:45";
  participantProfile2["freq1"] = "60";
  participantProfile2["freq2"] = "70";

  participantProfile3["ID"] = "3";
  participantProfile3["Image"] = "assets/images/salvatore.png";
  participantProfile3["name"] = "Salva";
  participantProfile3["lastname"] = "Tore";
  participantProfile3["age"] = "21";
  participantProfile3["height"] = "1.87";
  participantProfile3["weight"] = "85";
  participantProfile3["circumference"] = "90";
  participantProfile3["descriptive_sentence"] = "No pain no Gain -" +
      " Pensaci un minuto - Dimmi che ci sei!";
  participantProfile3["presences"] = "1";
  participantProfile3["day1"] = "Martedi 18:45";
  participantProfile3["day2"] = "Venerdì 7:45";
  participantProfile3["freq1"] = "65";
  participantProfile3["freq2"] = "85";


  participantProfile4["ID"] = "4";
  participantProfile4["Image"] = "assets/images/chiara.png";
  participantProfile4["name"] = "Chiara";
  participantProfile4["lastname"] = "Scura";
  participantProfile4["age"] = "24";
  participantProfile4["height"] = "1.70";
  participantProfile4["weight"] = "64";
  participantProfile4["circumference"] = "";
  participantProfile4["descriptive_sentence"] = "Shoot for the moon. " +
      " Even if you miss you'll land among the stars!";
  participantProfile4["presences"] = "13";
  participantProfile4["day1"] = "Lunedi 18:45";
  participantProfile4["day2"] = "Giovedi 7:45";
  participantProfile4["freq1"] = "99";
  participantProfile4["freq2"] = "99";

  participantProfile5["ID"] = "5";
  participantProfile5["Image"] = "assets/images/piervi.png";
  participantProfile5["name"] = "Piervi";
  participantProfile5["lastname"] = "Hola";
  participantProfile5["age"] = "24";
  participantProfile5["height"] = "1.85";
  participantProfile5["weight"] = "75";
  participantProfile5["circumference"] = "";
  participantProfile5["descriptive_sentence"] = "Hola amigos. ";
  participantProfile5["presences"] = "5";
  participantProfile5["day1"] = "Martedi 18:45";
  participantProfile5["day2"] = "Venerdì 7:45";
  participantProfile5["freq1"] = "70";
  participantProfile5["freq2"] = "80";


  participantProfiles.add(participantProfile1);
  participantProfiles.add(participantProfile2);
  participantProfiles.add(participantProfile3);
  participantProfiles.add(participantProfile4);
  participantProfiles.add(participantProfile5);

//  for (int i = 0; i<participantPreviews.length; i++)
//    {
//      print(participantPreviews[i]);
//    }

  List <HashMap> participantWodResults1 = new List<HashMap>();

  HashMap participantWodResult_11 = new HashMap<String, String>();
  HashMap participantWodResult_12 = new HashMap<String, String>();
  HashMap participantWodResult_13 = new HashMap<String, String>();
  HashMap participantWodResult_14 = new HashMap<String, String>();
  HashMap participantWodResult_15 = new HashMap<String, String>();

  participantWodResult_11["ID_wOD"] = "1";
  participantWodResult_11["time"] = "7:45";
  participantWodResult_11["date"] = "2019-10-12";
  participantWodResult_11["score"] = "225";
  participantWodResult_11["notes"] = "Felt hungry";

  participantWodResult_12["ID_wOD"] = "2";
  participantWodResult_12["time"] = "18:45";
  participantWodResult_12["date"] = "2019-10-17";
  participantWodResult_12["score"] = "300";
  participantWodResult_12["notes"] = "Felt so hungry";

  participantWodResult_13["ID_wOD"] = "3";
  participantWodResult_13["time"] = "7:45";
  participantWodResult_13["date"] = "2019-10-20";
  participantWodResult_13["score"] = "350";
  participantWodResult_13["notes"] = "Felt so so hungry";

  participantWodResult_14["ID_wOD"] = "4";
  participantWodResult_14["time"] = "18:45";
  participantWodResult_14["date"] = "2019-10-23";
  participantWodResult_14["score"] = "400";
  participantWodResult_14["notes"] = "Felt so so so hungry";

  participantWodResult_15["ID_wOD"] = "5";
  participantWodResult_15["time"] = "7:45";
  participantWodResult_15["date"] = "2019-10-27";
  participantWodResult_15["score"] = "450";
  participantWodResult_15["notes"] = "Felt so so so so hungry";


  participantWodResults1.add(participantWodResult_11);
  participantWodResults1.add(participantWodResult_12);
  participantWodResults1.add(participantWodResult_13);
  participantWodResults1.add(participantWodResult_14);
  participantWodResults1.add(participantWodResult_15);






  List <HashMap> participantWodResults2 = new List<HashMap>();

  HashMap participantWodResult_21 = new HashMap<String, String>();
  HashMap participantWodResult_22 = new HashMap<String, String>();
  HashMap participantWodResult_23 = new HashMap<String, String>();
  HashMap participantWodResult_24 = new HashMap<String, String>();
  HashMap participantWodResult_25 = new HashMap<String, String>();

  participantWodResult_21["ID_wOD"] = "1";
  participantWodResult_21["time"] = "7:45";
  participantWodResult_21["date"] = "2019-10-12";
  participantWodResult_21["score"] = "225";
  participantWodResult_21["notes"] = "Felt hungry";

  participantWodResult_22["ID_wOD"] = "2";
  participantWodResult_22["time"] = "18:45";
  participantWodResult_22["date"] = "2019-10-17";
  participantWodResult_22["score"] = "300";
  participantWodResult_22["notes"] = "Felt so hungry";

  participantWodResult_23["ID_wOD"] = "3";
  participantWodResult_23["time"] = "7:45";
  participantWodResult_23["date"] = "2019-10-20";
  participantWodResult_23["score"] = "350";
  participantWodResult_23["notes"] = "Felt so so hungry";

  participantWodResult_24["ID_wOD"] = "4";
  participantWodResult_24["time"] = "18:45";
  participantWodResult_24["date"] = "2019-10-23";
  participantWodResult_24["score"] = "400";
  participantWodResult_24["notes"] = "Felt so so so hungry";

  participantWodResult_25["ID_wOD"] = "5";
  participantWodResult_25["time"] = "7:45";
  participantWodResult_25["date"] = "2019-10-27";
  participantWodResult_25["score"] = "450";
  participantWodResult_25["notes"] = "Felt so so so so hungry";

  participantWodResults2.add(participantWodResult_21);
  participantWodResults2.add(participantWodResult_22);
  participantWodResults2.add(participantWodResult_23);
  participantWodResults2.add(participantWodResult_24);
  participantWodResults2.add(participantWodResult_25);





  List <HashMap> participantWodResults3 = new List<HashMap>();

  HashMap participantWodResult_31 = new HashMap<String, String>();
  HashMap participantWodResult_32 = new HashMap<String, String>();
  HashMap participantWodResult_33 = new HashMap<String, String>();
  HashMap participantWodResult_34 = new HashMap<String, String>();
  HashMap participantWodResult_35 = new HashMap<String, String>();

  participantWodResult_31["ID_wOD"] = "1";
  participantWodResult_31["time"] = "7:45";
  participantWodResult_31["date"] = "2019-10-12";
  participantWodResult_31["score"] = "225";
  participantWodResult_31["notes"] = "Felt hungry";

  participantWodResult_32["ID_wOD"] = "2";
  participantWodResult_32["time"] = "18:45";
  participantWodResult_32["date"] = "2019-10-17";
  participantWodResult_32["score"] = "300";
  participantWodResult_32["notes"] = "Felt so hungry";

  participantWodResult_33["ID_wOD"] = "3";
  participantWodResult_33["time"] = "7:45";
  participantWodResult_33["date"] = "2019-10-20";
  participantWodResult_33["score"] = "350";
  participantWodResult_33["notes"] = "Felt so so hungry";

  participantWodResult_34["ID_wOD"] = "4";
  participantWodResult_34["time"] = "18:45";
  participantWodResult_34["date"] = "2019-10-23";
  participantWodResult_34["score"] = "400";
  participantWodResult_34["notes"] = "Felt so so so hungry";

  participantWodResult_35["ID_wOD"] = "5";
  participantWodResult_35["time"] = "7:45";
  participantWodResult_35["date"] = "2019-10-27";
  participantWodResult_35["score"] = "450";
  participantWodResult_35["notes"] = "Felt so so so so hungry";

  participantWodResults3.add(participantWodResult_31);
  participantWodResults3.add(participantWodResult_32);
  participantWodResults3.add(participantWodResult_33);
  participantWodResults3.add(participantWodResult_34);
  participantWodResults3.add(participantWodResult_35);




  List <HashMap> participantWodResults4 = new List<HashMap>();

  HashMap participantWodResult_41 = new HashMap<String, String>();
  HashMap participantWodResult_42 = new HashMap<String, String>();
  HashMap participantWodResult_43 = new HashMap<String, String>();
  HashMap participantWodResult_44 = new HashMap<String, String>();
  HashMap participantWodResult_45 = new HashMap<String, String>();

  participantWodResult_41["ID_wOD"] = "1";
  participantWodResult_41["time"] = "7:45";
  participantWodResult_41["date"] = "2019-10-12";
  participantWodResult_41["score"] = "225";
  participantWodResult_41["notes"] = "Felt hungry";

  participantWodResult_42["ID_wOD"] = "2";
  participantWodResult_42["time"] = "18:45";
  participantWodResult_42["date"] = "2019-10-17";
  participantWodResult_42["score"] = "300";
  participantWodResult_42["notes"] = "Felt so hungry";

  participantWodResult_43["ID_wOD"] = "3";
  participantWodResult_43["time"] = "7:45";
  participantWodResult_43["date"] = "2019-10-20";
  participantWodResult_43["score"] = "350";
  participantWodResult_43["notes"] = "Felt so so hungry";

  participantWodResult_44["ID_wOD"] = "4";
  participantWodResult_44["time"] = "18:45";
  participantWodResult_44["date"] = "2019-10-23";
  participantWodResult_44["score"] = "400";
  participantWodResult_44["notes"] = "Felt so so so hungry";

  participantWodResult_45["ID_wOD"] = "5";
  participantWodResult_45["time"] = "7:45";
  participantWodResult_45["date"] = "2019-10-27";
  participantWodResult_45["score"] = "450";
  participantWodResult_45["notes"] = "Felt so so so so hungry";

  participantWodResults4.add(participantWodResult_41);
  participantWodResults4.add(participantWodResult_42);
  participantWodResults4.add(participantWodResult_43);
  participantWodResults4.add(participantWodResult_44);
  participantWodResults4.add(participantWodResult_45);


  List <HashMap> participantWodResults5 = new List<HashMap>();

  HashMap participantWodResult_51 = new HashMap<String, String>();
  HashMap participantWodResult_52 = new HashMap<String, String>();
  HashMap participantWodResult_53 = new HashMap<String, String>();
  HashMap participantWodResult_54 = new HashMap<String, String>();
  HashMap participantWodResult_55 = new HashMap<String, String>();

  participantWodResult_51["ID_wOD"] = "1";
  participantWodResult_51["time"] = "7:45";
  participantWodResult_51["date"] = "2019-10-12";
  participantWodResult_51["score"] = "225";
  participantWodResult_51["notes"] = "Felt hungry";

  participantWodResult_52["ID_wOD"] = "2";
  participantWodResult_52["time"] = "18:45";
  participantWodResult_52["date"] = "2019-10-17";
  participantWodResult_52["score"] = "300";
  participantWodResult_52["notes"] = "Felt so hungry";

  participantWodResult_53["ID_wOD"] = "3";
  participantWodResult_53["time"] = "7:45";
  participantWodResult_53["date"] = "2019-10-20";
  participantWodResult_53["score"] = "350";
  participantWodResult_53["notes"] = "Felt so so hungry";

  participantWodResult_54["ID_wOD"] = "4";
  participantWodResult_54["time"] = "18:45";
  participantWodResult_54["date"] = "2019-10-23";
  participantWodResult_54["score"] = "400";
  participantWodResult_54["notes"] = "Felt so so so hungry";

  participantWodResult_55["ID_wOD"] = "5";
  participantWodResult_55["time"] = "7:45";
  participantWodResult_55["date"] = "2019-10-27";
  participantWodResult_55["score"] = "450";
  participantWodResult_55["notes"] = "Felt so so so so hungry";

  participantWodResults5.add(participantWodResult_51);
  participantWodResults5.add(participantWodResult_52);
  participantWodResults5.add(participantWodResult_53);
  participantWodResults5.add(participantWodResult_54);
  participantWodResults5.add(participantWodResult_55);





  participantResults = new HashMap<String, List>();

  participantResults["1"] = participantWodResults1;
  participantResults["2"] = participantWodResults2;
  participantResults["3"] = participantWodResults3;
  participantResults["4"] = participantWodResults4;
  participantResults["5"] = participantWodResults5;






// REPORTS PLOT

  reportsdata = new HashMap<String,List>();



  october2019 = new List<List>();

  // consecutive presences of participatnts, we need to know the five participants with most presences

  participantPresences_oct = new List<List>();

  presences1_oct = new List<String>();
  presences1_oct.add("Lore");
  presences1_oct.add("15");

  presences2_oct = new List<String>();
  presences2_oct.add("Marco");
  presences2_oct.add("12");

  presences3_oct = new List<String>();
  presences3_oct.add("Salvo");
  presences3_oct.add("10");


  presences4_oct = new List<String>();
  presences4_oct.add("Piervi");
  presences4_oct.add("9");


  presences5_oct = new List<String>();
  presences5_oct.add("Chiara");
  presences5_oct.add("5");

  participantPresences_oct.add(presences1_oct);
  participantPresences_oct.add(presences2_oct);
  participantPresences_oct.add(presences3_oct);
  participantPresences_oct.add(presences4_oct);
  participantPresences_oct.add(presences5_oct);
  // participation per day

  participationperday_oct = new List<List>();

  firstday_oct = new List<String>();
  firstday_oct.add("Monday");
  firstday_oct.add("120");

  secondday_oct = new List<String>();
  secondday_oct.add("Tuesday");
  secondday_oct.add("130");

  thirdday_oct = new List<String>();
  thirdday_oct.add("Thursday");
  thirdday_oct.add("140");

  fourthday_oct = new List<String>();
  fourthday_oct.add("Friday");
  fourthday_oct.add("130");

  participationperday_oct.add(firstday_oct);
  participationperday_oct.add(secondday_oct);
  participationperday_oct.add(thirdday_oct);
  participationperday_oct.add(fourthday_oct);

  // participation per time slot

  participationpertimeslot_oct = new List<List>();

  firstslot_oct = new List<String>();
  firstslot_oct.add("07:00");
  firstslot_oct.add("103");

  secondslot_oct = new List<String>();
  secondslot_oct.add("07:45");
  secondslot_oct.add("94");

  thirdslot_oct = new List<String>();
  thirdslot_oct.add("12:30");
  thirdslot_oct.add("50");

  fourthslot_oct = new List<String>();
  fourthslot_oct.add("13:15");
  fourthslot_oct.add("89");

  fifthslot_oct = new List<String>();
  fifthslot_oct.add("18:30");
  fifthslot_oct.add("40");

  sixthslot_oct = new List<String>();
  sixthslot_oct.add("19:15");
  sixthslot_oct.add("120");

  participationpertimeslot_oct.add(firstslot_oct);
  participationpertimeslot_oct.add(secondslot_oct);
  participationpertimeslot_oct.add(thirdslot_oct);
  participationpertimeslot_oct.add(fourthslot_oct);
  participationpertimeslot_oct.add(fifthslot_oct);
  participationpertimeslot_oct.add(sixthslot_oct);


  october2019.add(participantPresences_oct);
  october2019.add(participationperday_oct);
  october2019.add(participationpertimeslot_oct);



  november2019 = new List<List>();

  // consecutive presences of participatnts, we need to know the five participants with most presences

  participantPresences = new List<List>();

  presences1 = new List<String>();
  presences1.add("Lore");
  presences1.add("20");

  presences2 = new List<String>();
  presences2.add("Marco");
  presences2.add("17");

  presences3 = new List<String>();
  presences3.add("Salvo");
  presences3.add("14");


  presences4 = new List<String>();
  presences4.add("Piervi");
  presences4.add("12");


  presences5 = new List<String>();
  presences5.add("Chiara");
  presences5.add("7");

  participantPresences.add(presences1);
  participantPresences.add(presences2);
  participantPresences.add(presences3);
  participantPresences.add(presences4);
  participantPresences.add(presences5);


// participation per day

  participationperday = new List<List>();

  firstday = new List<String>();
  firstday.add("Monday");
  firstday.add("170");

  secondday = new List<String>();
  secondday.add("Tuesday");
  secondday.add("180");

  thirdday = new List<String>();
  thirdday.add("Thursday");
  thirdday.add("175");

  fourthday = new List<String>();
  fourthday.add("Friday");
  fourthday.add("150");

  participationperday.add(firstday);
  participationperday.add(secondday);
  participationperday.add(thirdday);
  participationperday.add(fourthday);



// participation per time slot

  participationpertimeslot = new List<List>();

  firstslot = new List<String>();
  firstslot.add("07:00");
  firstslot.add("100");
  firstslot.add("15");


  secondslot = new List<String>();
  secondslot.add("07:45");
  secondslot.add("90");
  secondslot.add("13");


  thirdslot = new List<String>();
  thirdslot.add("12:30");
  thirdslot.add("70");
  thirdslot.add("11");


  fourthslot = new List<String>();
  fourthslot.add("13:15");
  fourthslot.add("60");
  fourthslot.add("8");


  fifthslot = new List<String>();
  fifthslot.add("18:30");
  fifthslot.add("80");
  fifthslot.add("12");


  sixthslot = new List<String>();
  sixthslot.add("19:15");
  sixthslot.add("50");
  sixthslot.add("7");


  participationpertimeslot.add(firstslot);
  participationpertimeslot.add(secondslot);
  participationpertimeslot.add(thirdslot);
  participationpertimeslot.add(fourthslot);
  participationpertimeslot.add(fifthslot);
  participationpertimeslot.add(sixthslot);


  november2019.add(participantPresences);
  november2019.add(participationperday);
  november2019.add(participationpertimeslot);

  zero = new List<List>();

  // consecutive presences of participatnts, we need to know the five participants with most presences

  participantPresences_zero = new List<List>();

  presences1_zero = new List<String>();
  presences1_zero.add("Lore");
  presences1_zero.add("0");

  presences2_zero = new List<String>();
  presences2_zero.add("Marco");
  presences2_zero.add("0");

  presences3_zero = new List<String>();
  presences3_zero.add("Salvo");
  presences3_zero.add("0");


  presences4_zero = new List<String>();
  presences4_zero.add("Piervi");
  presences4_zero.add("0");


  presences5_zero = new List<String>();
  presences5_zero.add("Chiara");
  presences5_zero.add("0");

  participantPresences_zero.add(presences1_zero);
  participantPresences_zero.add(presences2_zero);
  participantPresences_zero.add(presences3_zero);
  participantPresences_zero.add(presences4_zero);
  participantPresences_zero.add(presences5_zero);


// participation per day

  participationperday_zero = new List<List>();

  firstday_zero = new List<String>();
  firstday_zero.add("Monday");
  firstday_zero.add("0");

  secondday_zero = new List<String>();
  secondday_zero.add("Tuesday");
  secondday_zero.add("0");

  thirdday_zero = new List<String>();
  thirdday_zero.add("Thursday");
  thirdday_zero.add("0");

  fourthday_zero = new List<String>();
  fourthday_zero.add("Friday");
  fourthday_zero.add("0");

  participationperday_zero.add(firstday_zero);
  participationperday_zero.add(secondday_zero);
  participationperday_zero.add(thirdday_zero);
  participationperday_zero.add(fourthday_zero);



// participation per time slot

  participationpertimeslot_zero = new List<List>();

  firstslot_zero = new List<String>();
  firstslot_zero.add("07:00");
  firstslot_zero.add("0");

  secondslot_zero = new List<String>();
  secondslot_zero.add("07:45");
  secondslot_zero.add("0");

  thirdslot_zero = new List<String>();
  thirdslot_zero.add("12:30");
  thirdslot_zero.add("0");

  fourthslot_zero = new List<String>();
  fourthslot_zero.add("13:15");
  fourthslot_zero.add("0");

  fifthslot_zero = new List<String>();
  fifthslot_zero.add("18:30");
  fifthslot_zero.add("0");

  sixthslot_zero = new List<String>();
  sixthslot_zero.add("19:15");
  sixthslot_zero.add("0");

  participationpertimeslot_zero.add(firstslot_zero);
  participationpertimeslot_zero.add(secondslot_zero);
  participationpertimeslot_zero.add(thirdslot_zero);
  participationpertimeslot_zero.add(fourthslot_zero);
  participationpertimeslot_zero.add(fifthslot_zero);
  participationpertimeslot_zero.add(sixthslot_zero);


  zero.add(participantPresences_zero);
  zero.add(participationperday_zero);
  zero.add(participationpertimeslot_zero);


  reportsdata["January 2019"] = zero;
  reportsdata["February 2019"] = zero;
  reportsdata["March 2019"] = zero;
  reportsdata["April 2019"] = zero;
  reportsdata["May 2019"] = zero;
  reportsdata["June 2019"] = zero;
  reportsdata["July 2019"] = zero;
  reportsdata["August 2019"] = zero;
  reportsdata["September 2019"] = zero;
  reportsdata["October 2019"]=   october2019;
  reportsdata["November 2019"]=   november2019;
  reportsdata["December 2019"] = october2019;
  reportsdata["January 2020"] = zero;
  reportsdata["February 2020"] = zero;
  reportsdata["March 2020"] = zero;
  reportsdata["April 2020"] = zero;
  reportsdata["May 2020"] = zero;
  reportsdata["June 2020"] = zero;
  reportsdata["July 2020"] = zero;
  reportsdata["August 2020"] = zero;
  reportsdata["September 2020"] = zero;
  reportsdata["October 2020"]=   zero;
  reportsdata["November 2020"]=   zero;
  reportsdata["December 2020"] = zero;




  is_time_selected['d1_t1'] = true;
  is_time_selected['d2_t1'] = true;
  is_time_selected['d1_t2'] = true;
  is_time_selected['d2_t2'] = true;
  is_time_selected['d1_t3'] = true;
  is_time_selected['d2_t3'] = true;
  is_time_selected['d1_t4'] = true;
  is_time_selected['d2_t4'] = true;
  is_time_selected['d1_t5'] = true;
  is_time_selected['d2_t5'] = true;
  is_time_selected['d1_t6'] = true;
  is_time_selected['d2_t6'] = true;

  suggested_watchWords = ["missisipi", "limone", 'usa', 'burbero', 'rabarbaro',
    'chicago', 'mosca', 'polimi', 'berlino', 'tokyo', 'singapore', 'nutella'];

  // CREO WODS

  // creo time slots

  HashMap<String,Object> time_slot1=new HashMap();
  time_slot1["hour"]="7:00";
  time_slot1["participant_number"]=20;
  time_slot1["subscribed_number"]=23;

  HashMap<String,Object> time_slot2=new HashMap();
  time_slot2["hour"]="7:45";
  time_slot2["participant_number"]=24;
  time_slot2["subscribed_number"]=27;

  HashMap<String,Object> time_slot3=new HashMap();
  time_slot3["hour"]="12:30";
  time_slot3["participant_number"]=22;
  time_slot3["subscribed_number"]=28;

  HashMap<String,Object> time_slot4=new HashMap();
  time_slot4["hour"]="13:15";
  time_slot4["participant_number"]=21;
  time_slot4["subscribed_number"]=27;

  HashMap<String,Object> time_slot5=new HashMap();
  time_slot5["hour"]="18:30";
  time_slot5["participant_number"]=19;
  time_slot5["subscribed_number"]=21;

  HashMap<String,Object> time_slot6=new HashMap();
  time_slot6["hour"]="19:15";
  time_slot6["participant_number"]=15;
  time_slot6["subscribed_number"]=18;

  List<HashMap> time_slots=new List();
  time_slots.add(time_slot1);
  time_slots.add(time_slot2);
  time_slots.add(time_slot3);
  time_slots.add(time_slot4);
  time_slots.add(time_slot5);
  time_slots.add(time_slot6);

// creo wod19_12_13
  HashMap<String,Object> wod19_12_13=new HashMap();
  wod19_12_13["ID"]="2019_12_13";
  wod19_12_13["day"]=DateTime.utc(2019, 12, 13);
  wod19_12_13["name"]="OTTV019";
  wod19_12_13["type"]="13min AMRAP";
  wod19_12_13["ex1"]="12 KTB face burpee";
  wod19_12_13["ex2"]="20 air squat";
  wod19_12_13["ex3"]="200m run";
  wod19_12_13["description"]="Numero di ripetizioni totali+peso";
  wod19_12_13["time_slots"]=time_slots;

// creo wod19_12_12
  HashMap<String,Object> wod19_12_12=new HashMap();
  wod19_12_12["ID"]="2019_12_12";
  wod19_12_12["day"]=DateTime.utc(2019, 12, 12);
  wod19_12_12["name"]="OTTV019";
  wod19_12_12["type"]="13min AMRAP";
  wod19_12_12["ex1"]="12 KTB face burpee";
  wod19_12_12["ex2"]="20 air squat";
  wod19_12_12["ex3"]="200m run";
  wod19_12_12["description"]="Numero di ripetizioni totali+peso";
  wod19_12_12["time_slots"]=time_slots;

  // creo wod19_12_10
  HashMap<String,Object> wod19_12_10=new HashMap();
  wod19_12_10["ID"]="2019_12_10";
  wod19_12_10["day"]=DateTime.utc(2019, 12, 10);
  wod19_12_10["name"]="Tourbillon";
  wod19_12_10["type"]="5 rnds: 1.5min ON 1.5min OFF";
  wod19_12_10["ex1"]="10 KTBs squat";
  wod19_12_10["ex2"]="12 push up";
  wod19_12_10["ex3"]="ME 10m shuttle run";
  wod19_12_10["description"]="Numero di shuttle run per ognuno dei rnds + peso";
  wod19_12_10["time_slots"]=time_slots;

    // creo wod19_12_09
  HashMap<String,Object> wod19_12_09=new HashMap();
  wod19_12_09["ID"]="2019_12_09";
  wod19_12_09["day"]=DateTime.utc(2019, 12, 09);
  wod19_12_09["name"]="Tourbillon";
  wod19_12_09["type"]="5 rnds: 1.5min ON 1.5min OFF";
  wod19_12_09["ex1"]="10 KTBs squat";
  wod19_12_09["ex2"]="12 push up";
  wod19_12_09["ex3"]="ME 10m shuttle run";
  wod19_12_09["description"]="Numero di shuttle run per ognuno dei rnds + peso";
  wod19_12_09["time_slots"]=time_slots;

  // creo wod19_12_06
  HashMap<String,Object> wod19_12_06=new HashMap();
  wod19_12_06["ID"]="2019_12_06";
  wod19_12_06["day"]=DateTime.utc(2019, 12, 06);
  wod19_12_06["name"]="saluto d'inverno";
  wod19_12_06["type"]="5 rnds: 1.5ON 1.5OFF";
  wod19_12_06["ex1"]="16 hang KTB snatch (8sn+8ds)";
  wod19_12_06["ex2"]="16 mountain climb ( 8 coppie sn e ds)";
  wod19_12_06["ex3"]="KTBs deadlift";
  wod19_12_06["description"]="Numero di deadlift per ognuno dei rnds + peso";
  wod19_12_06["time_slots"]=time_slots;

  // creo wod19_12_05
  HashMap<String,Object> wod19_12_05=new HashMap();
  wod19_12_05["ID"]="2019_12_05";
  wod19_12_05["day"]=DateTime.utc(2019, 12, 05);
  wod19_12_05["name"]="saluto d'inverno";
  wod19_12_05["type"]="5 rnds: 1.5ON 1.5OFF";
  wod19_12_05["ex1"]="16 hang KTB snatch (8sn+8ds)";
  wod19_12_05["ex2"]="16 mountain climb ( 8 coppie sn e ds)";
  wod19_12_05["ex3"]="KTBs deadlift";
  wod19_12_05["description"]="Numero di deadlift per ognuno dei rnds + peso";
  wod19_12_05["time_slots"]=time_slots;

  // creo wod19_12_03
  HashMap<String,Object> wod19_12_03=new HashMap();
  wod19_12_03["ID"]="2019_12_03";
  wod19_12_03["day"]=DateTime.utc(2019, 12, 03);
  wod19_12_03["name"]="Sven";
  wod19_12_03["type"]="5 rnds: 1.5ON 1.5OFF";
  wod19_12_03["ex1"]="8 KTBs power clean";
  wod19_12_03["ex2"]="8 KTBs squat";
  wod19_12_03["ex3"]="8 mountain climb (sn+ds=1)";
  wod19_12_03["ex4"]="ME 10m shuttle run";
  wod19_12_03["description"]="Numero degli shuttle run per ognuno dei rnds + peso";
  wod19_12_03["time_slots"]=time_slots;

  // creo wod19_12_02
  HashMap<String,Object> wod19_12_02=new HashMap();
  wod19_12_02["ID"]="2019_12_02";
  wod19_12_02["day"]=DateTime.utc(2019, 12, 02);
  wod19_12_02["name"]="Sven";
  wod19_12_02["type"]="5 rnds: 1.5ON 1.5OFF";
  wod19_12_02["ex1"]="8 KTBs power clean";
  wod19_12_02["ex2"]="8 KTBs squat";
  wod19_12_02["ex3"]="8 mountain climb (sn+ds=1)";
  wod19_12_02["ex4"]="ME 10m shuttle run";
  wod19_12_02["description"]="Numero degli shuttle run per ognuno dei rnds + peso";
  wod19_12_02["time_slots"]=time_slots;

  // aggiungo wods
  wods["20191213"] = wod19_12_13;
  wods["20191212"] = wod19_12_12;
  wods["20191210"] = wod19_12_10;
  wods["20191209"] = wod19_12_09;
  wods["20191206"] = wod19_12_06;
  wods["20191205"] = wod19_12_05;
  wods["20191203"] = wod19_12_03;
  wods["20191202"] = wod19_12_02;


}



