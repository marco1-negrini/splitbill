import 'package:flutter/material.dart';
import 'calendar4_widget/calendar4_widget.dart';
import 'data.dart';
import 'dart:collection';
import 'dart:math';




String USER = " ";

authenticate_user(context, user, password)
{
  /* Method to authenticate the user */

  user = user.trim();

  String REQUEST_NAME = "Authentication";

  if (user.length == 0 || password.length == 0)
  {
    print("No username or password inserted.");
    showAlert(context, "Empty username or password");
  }
  else
  {
    print("Sending request to server: " + REQUEST_NAME + "\nParams: {username: " + user + ", password: " + password + "}");

    // Simulating server answer to authentication
    bool answer = check_user(context,user,password);

    if (answer)
    {
      // answer ok
      print(REQUEST_NAME + " response: 200");
      USER = user;
      Navigator.pushNamed(context, '/HomePage');
    }
    else
    {
      // answer ko
      print(REQUEST_NAME + " response: 404");
    }
  }
}

get_imageProfile(user)
{
  /* Method to retrieve image profile */

  String REQUEST_NAME = "Profile image retrieval";

  String path;

  print("Sending request to server: " + REQUEST_NAME + "\nParams: {username: " + user + "}");

  if (imagesMapping.containsKey(user))
  {
    path = imagesMapping[user];
  }
  else
  {
    print( "Could not find image profile, default image has been loaded.");
    path = "assets/images/defaultuser.png";
  }

  print(REQUEST_NAME + " response: 200");
  return path;
}

logout(context)
{
  String REQUEST_NAME = "Logout";

  USER = "";

  print("Sending request to server: " + REQUEST_NAME + "\nParams: { }");
  print(REQUEST_NAME + " response: 200");

  Navigator.pushNamed(context, '/LoginPage');
}


get_participantsProfiles(partecipant_id)
{
  /* Method to retrieve partecipant profile info by partecipant_id */

  String REQUEST_NAME = "Get Participant Profiles";

  print("Sending request to server: " + REQUEST_NAME + "\nParams: {partcipant_id: " + partecipant_id.toString() + "}");

  HashMap answer = participantProfiles[partecipant_id-1];
  print(REQUEST_NAME + " response: 200");
  print("ansewer:");
  print(answer);
  return answer;
}

get_participantsReuslts(partecipant_id){

  String REQUEST_NAME = "Get Participant Results";

  print("Sending request to server: " + REQUEST_NAME + "\nParams: {partecipant_id: " + partecipant_id.toString() + "}");

  List <HashMap> answer = participantResults[partecipant_id];

  print(REQUEST_NAME + "response: 200");
  print(answer);
  return answer;

}


get_participationpertimeslot(int a, int b){

  String REQUEST_NAME = "Get participation per time slot";

  print("Sending request to server: " + REQUEST_NAME + "\nParams: none");
  String answer = participationpertimeslot[a][b];

  print(REQUEST_NAME + "response: 200");
  print(answer);
  return answer;

}



get_participantsPreviewa()
{
  /* Method to retrieve participants list (ID + Image) */

  String REQUEST_NAME = "Get Participants list";

  print("Sending request to server: " + REQUEST_NAME + "\nParams: none");
  List<HashMap> answer = participantPreviews;
  print(REQUEST_NAME + " response: 200");
  print("ansewer:");
  print(answer);
  return answer;
}

check_user(context, username, password)
{
  /* Method to check if username and password are corrected */
  if (registered_users.containsKey(username))
  {
    //check if password is correct
    if (registered_users[username] == password)
    {
      //password correct
      return true;
    }
    else
    {
      //send alert "wrong password"
      print("Incorrect password");
      showAlert(context, "Incorrect password");
      return false;
    }
  }
  else
  {
    //send alert "user does not exist"
    print("User does not exist");
    showAlert(context, "User does not exist");
    return false;
  }
}

// user defined function
void showAlert(context, message) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text("Alert!"),
        content: new Text(message),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

showAlert2(context, message) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text("Alert!"),
        content: new Text(message),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("Continue"),
            onPressed: () {
              Navigator.of(context).pop();
              Navigator.pushNamed(context, '/Addevent2');
              return true;
            },
          ),
          new FlatButton(
            child: new Text("Cancel"),
            onPressed: () {
              Navigator.of(context).pop();
              return false;
            },
          ),
        ],
      );
    },
  );
}

generate_score_description(wodName, wodType, ex1, ex2, ex3, ex4, context){
  print("Generationg score description for WOD session:");
  String peso =  '';
  int flag=0; // azzero la variabile flag
  // cerco se gli esercizi contengono la parola ktb per inserire "+ peso" o no
  if (ex1.toLowerCase().contains('ktb') || ex2.toLowerCase().contains('ktb') ||
      ex3.toLowerCase().contains('ktb') || ex4.toLowerCase().contains('ktb')){
    peso = ' + weight of the used ktb';
  };

// crea wod score
  if (wodType.toLowerCase().contains('rnds')) {
    String exscore;
    flag=1;
    if (ex4 == '') {
      //print('null ex4');
      if (ex3 == '') {
        if (ex2 == '') {
          exscore = ex1;
        }
        else{
          exscore = ex2;
          //return ('Numero di ' + exscore + ' per ognuno dei ' + wodType + peso);
        }
      }
      else{
        exscore = ex3;
        //return ('Numero di ' + exscore + ' per ognuno dei ' + wodType + peso);
      }
    }
    else {
      exscore = ex4;
    }
    return ('Insert the number of \'' + exscore.toUpperCase() + '\' for each round ' + peso + '.');
  }

  if (wodType.toLowerCase().contains('amrap')) {
    flag=1;
    return('Insert the total number of reps computed as the sum of the reps for each ex in each round '  +peso +'.');

  }
  // check che il wod type sia corretto
  if(flag==0) {
    //showAlert(context, 'Invalid WOD type, could not generate a description.');
    return '';
  }
}

clean_add_event_data(){
  print("Cleaning data for addEvent.");
  scoreDescription='';
  wod_name = '';
  wod_type = '';
  ex1 = '';
  ex2 = '';
  ex3 = '';
  ex4 = '';
  wod_image = null;
  is_time_selected.forEach((k,v) => is_time_selected[k] = true);
  listOfSelectedDays = [];
  watchWord = suggest_watchWord();
  is_description_editable = true;
  selected_wod = "20191213";
}

check_parameters_createEvent(context, wodNme, wodType, exe1, exe2, exe3, exe4, {send_alert=true}){
  if (wodNme == '' || wodType == '') {
    if(send_alert) {
      showAlert(context, 'WOD name and WOD type cannot be empy!');
    }
    return false;
  }
  if (exe1 == '') {
    if(send_alert) {
      showAlert(context, 'ex1 cannot be empty!');
    }
    return false;
  }
  wod_name = wodNme;
  wod_type = wodType;
  ex1 = exe1;
  ex2 = exe2;
  ex3 = exe3;
  ex4 = exe4;
  return true;
}



getreportsdata(){
  String REQUEST_NAME = "Get reports data";


  /*print("Sending request to server: " + REQUEST_NAME + "\nParams:"+month);
  List answer = reportsdata[month];
  List participantPresences = answer[0];
  List participationperday = answer[1];
  List participationpertimeslot = answer[2]; */
  print("Sending request to server: " + REQUEST_NAME + "\nParams: none");
  HashMap answer = reportsdata;
  print(REQUEST_NAME + " response: 200");
  print("answer:");
  print(answer);
  return answer;
}

printnotflaggeddays() {
  print("d1_t1: " + is_time_selected['d1_t1'].toString());
  print("d2_t1: " + is_time_selected['d2_t1'].toString());
  print("d1_t2: " + is_time_selected['d1_t2'].toString());
  print("d2_t2: " + is_time_selected['d2_t2'].toString());
  print("d1_t3: " + is_time_selected['d1_t3'].toString());
  print("d2_t3: " + is_time_selected['d2_t3'].toString());
  print("d1_t4: " + is_time_selected['d1_t4'].toString());
  print("d2_t4: " + is_time_selected['d2_t4'].toString());
  print("d1_t5: " + is_time_selected['d1_t5'].toString());
  print("d2_t5: " + is_time_selected['d2_t5'].toString());
  print("d1_t6: " + is_time_selected['d1_t6'].toString());
  print("d2_t6: " + is_time_selected['d2_t6'].toString());
}


suggest_watchWord(){
  //print("suggesting watchword...");
  var rng = new Random();
  return suggested_watchWords[rng.nextInt(suggested_watchWords.length-1)];
}

post_event(secret){
  watchWord = secret;
  print('creating next call with the following information:');
  String REQUEST_NAME = "Post event";

  print("Sending request to server: " + REQUEST_NAME + "\nParams:");
  display_addEvent_data();
  print(REQUEST_NAME + " response: 200");

  return true;
}

void display_addEvent_data(){
  print("");
  print("wod_name: " + wod_name);
  print("wod_type: " + wod_type);
  print("ex1: " + ex1);
  print("ex2: " + ex2);
  print("ex3: " + ex3);
  print("ex4: " + ex4);
  print("score_description: " + scoreDescription);
  print("selected days:");
  print(listOfSelectedDays);
  print("sessions:");
  is_time_selected.forEach((k,v) => print('${k}: ${v}'));
  print("watchword: " + watchWord);
}

void save_addEvent_data(wodNme, wodType, exe1, exe2, exe3, exe4, description, image){
  wod_name = wodNme;
  wod_type = wodType;
  ex1 = exe1;
  ex2 = exe2;
  ex3 = exe3;
  ex4 = exe4;
  scoreDescription = description;
  wod_image = image;
  //display_addEvent_data();

}

get_wods_dates(){
List<DateTime> listOfTrainings= new List();
//List<DateTime> listOfTrainings= [date1,date2];

//print('aaaaaaaa');
wods.forEach((k,v) => listOfTrainings.add(DateTime.parse(k).toUtc()));
//print(date2.isUtc);
print(listOfTrainings);
//print(wods.containsKey("2019_12_12"));
//print(wods.keys);
return listOfTrainings;
}

checkTraining(date, context){
  print("aaaaaa");
  print(date);
  //check if there is a wod session for the given date
  String _stringdate = date.toString().substring(0,10).replaceAll('-', '');
  print(_stringdate);
  print(there_is_wod_session(_stringdate));
  if(there_is_wod_session(_stringdate)){
    //check if is a past or futher wod session
    if(date.isBefore(DateTime.now())){
      //load the page for the past wod session
      print(context);
      //Navigator.pushNamed(context, '/HomePage');
      selected_wod = _stringdate;
      Navigator.push(context, new MaterialPageRoute(
          builder: (context) =>
              Calendar4Widget()), );
    }
    else{
      //load the page for the future wod session
    }
  }
}

there_is_wod_session(date){
  return wods.containsKey(date);
}

get_wod(wod_id){
  return wods[wod_id];
}
